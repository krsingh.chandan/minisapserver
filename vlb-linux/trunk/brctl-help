brctl-vlb : configuration utility for a bridge with the vlanbridge patch

help summary :
~~~~~~~~~~~~~~

commands:
        addbr           <bridge>                add bridge
        addif           <bridge> <device>       add interface to bridge
        delbr           <bridge>                delete bridge
        delif           <bridge> <device>       delete interface from bridge
        show                                    show a list of bridges
        showmacs        <bridge>                show a list of mac addrs
        showstp         <bridge>                show bridge stp info

        setageing       <bridge> <time>         set ageing time
        setbridgeprio   <bridge> <prio>         set bridge priority
        setfd           <bridge> <time>         set bridge forward delay
        setgcint        <bridge> <time>         set garbage collection interval
        sethello        <bridge> <time>         set hello time
        setmaxage       <bridge> <time>         set max message age
        setpathcost     <bridge> <port> <cost>  set path cost
        setportprio     <bridge> <port> <prio>  set port priority
        stp             <bridge> <state>        turn stp on/off

VLANBridge specific commands :
        setsubnet       <bridge> <subnet>/<netmask bits> <router mac>
        getsubnet       <bridge>
        getdenylist     <bridge>                Shows the list of denied macs
        flushdenylist   <bridge>                Flushes the deny list
        adddeny         <bridge> <mac> [<flags>] Add a mac to the deny list
        deldeny         <bridge> <mac>          Remove a mac from the deny list
        settimeoutstate <bridge> enable|disable Enable timeout in the proxy database
        gettimeoutstate <bridge>                Get the current timeout state
        getproxydb      <bridge>
        setloglevel     <bridge> <loglevel>     Set the desired log level (0=none..7=debug)
        getloglevel     <bridge>


For more information about the generic bridge commands see : 
    http://bridge.sourceforge.net


*
* VLANbridge-specific commands
*

* setsubnet :
~~~~~~~~~~~~~
* changes the subnet in which the bridge works
For each the IPs in this subnet, a memory buffer is allocated to store
the corresponding MAC address.
* You need to specify the router MAC address, so the VLANbridge will know
where to send packet aimed to go outside the subnet.
* VLANbridge doesn't work if it has not been set
eg : brctl setsubnet br0 138.195.136.0/20 08:00:12:AF:E3:10


* getsubnet :
~~~~~~~~~~~~~
prints out the subnet configuration


* getdenylist :
~~~~~~~~~~~~~~~
* prints out the list of MAC addresses that are blocked by the vlanbridge
and whether the unicast is still allowed or not


* flushdenylist :
~~~~~~~~~~~~~~~~~
* removes all the MAC addresses from the deny list


* adddeny :  
~~~~~~~~~~~
* adds a MAC address to the denylist
* you can specify the allow_unicast option, if you want unicast (and ARP
        requests) to be still forwarded
eg: brctl adddeny 00:01:02:DC:04:66 allow_unicast


* deldeny :
~~~~~~~~~~~
* removes the specified MAC address from the denylist


* settimeoutstate:
~~~~~~~~~~~~~~~~~~
* enable or disables timeout in the proxydatabase


* gettimeoutstate :
~~~~~~~~~~~~~~~~~~~
* prints out whether there is a timeout for the prosydatabase or not


* getproxydb :
~~~~~~~~~~~~~~
* prints out the list of learned MAC addresses and their corresponding
IP in the database


* setloglevel :
~~~~~~~~~~~~~~~
* sets the desired kernel log level for the VLANbridge module :
    0 = none
    7 = debug

    
* getloglevel :
~~~~~~~~~~~~~~~
* prints the current log level

