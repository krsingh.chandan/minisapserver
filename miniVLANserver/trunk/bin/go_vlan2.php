#!/home/diff/bin/php -q
<?
/*****************************************************************************
 * go_vlan2.php: Place ALL machines in VLAN 2
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000 VideoLAN
 * $Id: go_vlan2.php,v 1.1 2002/01/27 22:00:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

if( $argv[1] == "-h" || $argv[1] == "--help" )
{
    echo "Usage : go_vlan2.php\n";
    exit( -1 );
}

include( "database.inc" );
include( "snmp.inc" );

openlog( "VLCS/PHP", LOG_PERROR, LOG_DAEMON );

base_Init();

$ports = base_AllPorts( 2 );

if( !$ports )
{
    base_Close();
    closelog();
    exit( 1 );
}

foreach ($ports as $query)
{
    if( $query["port_protection"] )
    {
        continue;
    }

    /* Do SNMP request */
    if( !snmp_set_vlan( $query["switch_ip"], $query["port_internal_id"],
                        $query["vlan2_id"], $query["community_string"] ) )
    {
        syslog( LOG_CRIT, "SNMP failed (".$query["switch_ip"].":"
                .$query["port_internal_id"].")\n" );
        continue;
    }

    base_Change( $query["port_id"], 2 );
}

base_Close();
closelog();
exit( 0 );

?>
