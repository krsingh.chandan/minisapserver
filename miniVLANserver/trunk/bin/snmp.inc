<?
// correspondance type-switch, nom mod�le
$typeSw[1]="3C1000";
$typeSw[2]="3CDESK";
$typeSw[3]="3C1100";
$typeSw[4]="3C3300";
$typeSw[5]="3C4400";
$typeSw[6]="HP2524";

function snmp_walk_switch_ids( $switch_ip, $switch_unit, $switch_typen, $community)
{
    global $typeSw;

    $switch_type=$typeSw[$switch_typen];
    $vlan_port=snmpwalkoid( $switch_ip, $community, ".1.3.6.1.2.1.2.2.1.2");

    foreach($vlan_port as $oid => $value)
    {
        $type=0;
        if (ereg("RMON:V2 Port.*Unit",$value))
        {
            list($j1, $j2, $port_num, $j4, $j5, $unit)=
                    explode(" ",$value);
            if ($unit==$switch_unit)
            {
                list($j1,$j2,$j3,$j4,$port_id)=explode(".",$oid);
                $type=1;
            }
        }
        else if(ereg("RMON:10/100 Port.*Unit",$value))
        {
            list($j1,$j2,$port_num,$j4,$j5,$unit)=explode(" ",$value);
            if ($unit==$switch_unit)
            {
            list($j1, $j2, $j3, $j4,$port_id)=explode(".",$oid);
            $type=1;
            }
        }
        else if (ereg("RMON:V2 Port",$value))
        {
            list($j1, $j2, $port_num)=explode(" ",$value);
            list($j1, $j2, $j3, $j4,$port_id)=explode(".",$oid);
            $type=1;
        }
        else if(ereg("RMON:10/100 Port",$value))
        {
            list($j1,$j2,$port_num,$j4,$j5)=explode(" ",$value);
            list($j1, $j2, $j3, $j4,$port_id)=explode(".",$oid);
            $type=1;
        } 
        else if ($switch_type=="3C1000" || $switch_type=="3CDESK")
        {
            if (ereg("RMON:VLAN",$value))
            {
                list($j1, $vlan_tmp, $j3)=explode(":",$value);
                list($j1, $vlan_num) = explode(" ",$vlan_tmp);
                list($j1, $j2, $j3, $j4,$vlan_id)=explode(".",$oid);
                $type=2;
            }
        }
        else if ($switch_type=="3C1100" || $switch_type=="3C3300")
        {
            if (ereg("RMON VLAN",$value))
            {
                list($j1, $j2, $vlan_num ) = explode(" ",$value);
                list($j1, $j2, $j3, $j4, $vlan_id) = explode(".",$oid);
                $type=2;
            }
        }
        else if ($switch_type=="HP2524")
        {
            if (ereg("VLAN",$value))
            {
                $vlan_num = substr($value, 4);
                list($j1, $j2, $j3, $j4, $vlan_id) = explode(".",$oid);
                $type=2;
            }
            else
            {
                $port_num=$value;
                list($j1, $j2, $j3, $j4,$port_id) = explode(".",$oid);
                if ($port_num>0)
                {
                    $type=1;    
                }
            }
        }
        if ($type==1)
        {
            $result["Port $port_num"]=$port_id;
        }
        else if ($type==2)
        {
            $result["Vlan $vlan_num"]=$vlan_id;
        }
    }
    return $result;
}

function snmp_set_vlan( $switch_ip, $port_internal_id, $vlan_id, $community)
{
    syslog( LOG_INFO, "SNMP request : $switch_ip: ".
              "31.1.2.1.3.$vlan_id.$port_internal_id\n");
    
    if ($switch_ip=="138.195.128.58")
    { // kludge immonde pour le switch hp, � enlever au plus vite
        echo "executing system\n";
        system("./hp_change_vlan.sh ".$port_internal_id." ".($vlan_id-28));
        return true;
//        return false;
    }
    else
    {
    return is_array(snmpset("$switch_ip", "$community", "31.1.2.1.3.$vlan_id.$port_internal_id", "i", "4"));
    }
}

function snmp_get_vlan( $switch_ip, $port_internal_id, $community)
{
    $vlantab=snmpwalkoid("$switch_ip", "$community", "31.1.2.1.3");

    foreach($vlantab as $oid => $value)
    {
        $liste=explode(".",$oid);
        $vlanid=$liste[5];
        $portid=$liste[6];
        $resultat["$portid"]=$vlanid;
    }
    return $resultat;
}

function snmp_get_sdb( $switch_ip, $switch_unit, $community, $switch_typen)
{
    global $typeSw;
    
    $switch_type=$typeSw[$switch_typen];
    echo "$switch_type\n";
    
    if ($switch_type=="HP2524")
    {
        $sdb=snmpwalkoid( $switch_ip, $community,
                    "17.7.1.2.2.1.2.1");
        foreach($sdb as $oid => $value)
        {
            $liste=explode(".", $oid);
            $m1=dechex($liste[8]);
            $m2=dechex($liste[9]);
            $m3=dechex($liste[10]);
            $m4=dechex($liste[11]);
            $m5=dechex($liste[12]);
            $m6=dechex($liste[13]);
            $mac=$m1.":".$m2.":".$m3.":".$m4.":".$m5.":".$m6;
            $port_num=$value;
            $result[$mac]=$port_num;
        }
    }
    else
    {
        $sdb=snmpwalkoid( $switch_ip, $community, 
                ".1.3.6.1.4.1.43.10.22.2.1.3.$switch_unit");
    
        foreach($sdb as $oid => $value)
        {
            $liste=explode(".", $oid);
            $port_num=$liste[8];
            list($j1, $j2, $m1, $m2, $m3, $m4, $m5, $m6) = explode(" ",$value);
            $mac=$m1.":".$m2.":".$m3.":".$m4.":".$m5.":".$m6;
            $result[$mac]=$port_num;
        }
    }
    return $result;
}

?>
