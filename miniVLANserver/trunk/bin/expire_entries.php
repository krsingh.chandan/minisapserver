#!/home/diff/bin/php -q
<?
/*****************************************************************************
 * expire_entries.php: Expires old machines
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000 VideoLAN
 * $Id: expire_entries.php,v 1.1 2002/01/27 22:00:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

if( $argv[1] == "-h" || $argv[1] == "--help" )
{
    echo "Usage : expire_entries.php\n";
    exit( -1 );
}

include( "database.inc" );

openlog( "VLCS/PHP", LOG_PERROR, LOG_DAEMON );

base_Init();

base_Expire();

base_Close();
closelog();
exit( 0 );

?>
