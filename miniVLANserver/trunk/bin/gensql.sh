#!/bin/bash

cat ../conf/network.conf | grep -v '^#' | grep -v '^ *$' |
{
    switch_num=1;
    port_id=1;
    while read nom ip unit type community nports liste
    do
        echo "INSERT INTO switch VALUES ($switch_num, '$nom', '$ip', $unit, $type, '$community',";
        echo "            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);"
        for port_num in `seq 1 $nports`
        do
            if echo $liste | grep "\<$port_num\>" > /dev/null 
            then
                echo "INSERT INTO port VALUES ( $port_id, $switch_num, $port_num, -1, 2, 2, -1);"
            elif echo $liste | grep "\<r$port_num\>" > /dev/null 
            then
                echo "INSERT INTO port VALUES ( $port_id, $switch_num, $port_num, -1, 1, 2, -1);"
            else
                echo "INSERT INTO port VALUES ( $port_id, $switch_num, $port_num, -1, 0, 2, -1);"
            fi
            port_id=$(($port_id+1))
        done

        switch_num=$(($switch_num+1))
        echo
        echo
    done

    port_id=$(($port_id+1))
    switch_num=$(($switch_num+1))
    echo "SELECT setval('switch_seq',$switch_num);";
    echo "SELECT setval('port_seq',$port_id);";
        
} > ../conf/network.sql
