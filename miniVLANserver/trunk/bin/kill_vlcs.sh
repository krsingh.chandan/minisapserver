#!/bin/bash
# usage : kill_vlcs.sh signal
# stop the previous instance of vlcs
killall $1 vlcs
