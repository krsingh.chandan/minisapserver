<?

/* Configuration */
$pg_host = "localhost";
$pg_port = 5432;
$pg_db = "vlcs";

/*
 * Database management functions
 */
function base_Init( )
{
    global $pg_handle, $pg_host, $pg_port, $pg_db;

    $pg_handle = pg_connect($pg_host, $pg_port, $pg_db);
    if( !$pg_handle )
    {
        syslog( LOG_ALERT, "Error : Cannot open database !\n" );
        exit(-1);
    }
}

function base_Close( )
{
    global $pg_handle;

    pg_close( $pg_handle );
}

function base_Check( $mac_ad, $vlan )
{
    global $pg_handle;

    $request = pg_exec( $pg_handle, "select port.port_id, port.port_internal_id, port.port_protection, switch.switch_ip, switch.community_string, switch.vlan".$vlan."_id from machine, port, switch where machine.mac_ad='$mac_ad' and machine.port_id=port.port_id and port.switch_id=switch.switch_id" );

    if( !$request )
    {
        syslog( LOG_ALERT, "Error : PostgreSQL database has gone fishing !\n" );
        return;
    }

    $num = pg_numrows( $request );

    if( $num == 0 )
    {
        syslog( LOG_NOTICE, "Warning : $mac_ad doesn't exist in the database\n" );
        return;
    }
    elseif( $num > 1 )
    {
        syslog( LOG_NOTICE, "Warning : duplicate entries for $mac_ad\n" );
    }

    return pg_fetch_array( $request, 0 );
}

function base_AllPorts( $channel )
{
    global $pg_handle;

    $request = pg_exec( $pg_handle, "select port.port_id, port.port_internal_id, port.port_protection, switch.switch_ip, switch.community_string, switch.vlan".$channel."_id from port, switch where port.switch_id=switch.switch_id" );

    if( !$request )
    {
        syslog( LOG_ALERT, "Error : PostgreSQL database has gone fishing !\n" );
        return;
    }

    $num = pg_numrows( $request );
    $result = array();

    for( $i = 0; $i < $num; $i++ )
    {
        $result[] = pg_fetch_array( $request, $i );
    }

    return( $result );
}

function base_Change( $port_id, $channel )
{
    /* No error checks because it's been done in base_Check() */
    global $pg_handle;

    $request = pg_exec($pg_handle, "update port set vlan_num=$channel where port_id=$port_id" );

    if( !$request )
    {
        syslog( LOG_ALERT, "Error : PostgreSQL database has gone fishing !\n");
    }

    if( ($num = pg_cmdtuples( $request )) != 1 )
    {
        syslog( LOG_CRIT, "Error : Update made weird things (cmdtuples=$num)");
    }
}

function base_Expire()
{
    global $pg_handle;

    // 172800=2*24*3600secondes=48heures de aging time
    $request = pg_exec($pg_handle, 
                    "delete from machine where age(now(), expires)>172800");
    if( !$request )
    {
        echo "Error : PostgreSQL database has gone fishing !\n";
        return;
    }

    syslog( LOG_NOTICE, "Maintenance : Expired ".pg_cmdtuples($request)." machine entries\n" );
}

function base_IncreaseUnseen()
{
    global $pg_handle;

//    pg_exec($pg_handle, "update machine, port set port.unseen_count = port.unseen_count + 1 where machine.port_id = port.port_id");
}


// returns all macs that have not been seen for a while (4 times for eg.)
function base_UnseenLimit()
{
    global $pg_handle;

  /*  $request=pg_exec($pg_handle, "select switch.switch_ip, port.port_internal_id, switch.vlan2_id, switch.community_string from machine, port, switch where unseen_count>4 and port.port_id=machine.port_id and port.vlan_num>2");

    if( !$request )
    {
        syslog( LOG_ALERT, "Error : PostgreSQL database has gone fishing !\n" );
        return;
    }

    $num = pg_numrows( $request );
    $result = array();

    for( $i = 0; $i < $num; $i++ )
    {
        $row = pg_fetch_array( $request, $i );
        $result[]=$row["mac_ad"];
    }

    return( $result );
    */
}

// insert machine in db or update info if machine already exists
// return 0 if everything is OK
function set_Machine( $port_id, $mac_addr )
{
    global $pg_handle;

    $request = pg_exec($pg_handle, "select machine_id from machine where mac_ad='$mac_addr';" );
    if( !$request )
    {
        syslog(LOG_ERR,"Error : PostgreSQL database has gone fishing !\n");
        return;
    }

    $num = pg_numrows( $request );

    if( $num == 0 )
    {
        $request=pg_exec($pg_handle, "select nextval('machine_seq');");
        $row=pg_fetch_row($request, 0);
        $machine_id=$row[0];

        $timestamp=time()+3600*24*48;
        $res=pg_exec($pg_handle, "insert into machine values ($machine_id, $port_id, '$mac_addr', now());");
        if ($res==FALSE)
        {
            syslog(LOG_ERR, "Insert machine query could not be executed\n");
            return 1;
        }
        return 0;
    }
    else if ($num == 1)
    {
        $row=pg_fetch_array($request, 0);
        $machine_id=$row[0];
        $res=pg_exec($pg_handle, "update machine set port_id=$port_id, mac_ad='$mac_addr',expires=now() where machine_id=$machine_id"); 
        if ($res==FALSE)
        {
            syslog(LOG_ERR, "Update machine query could not be executed\n");
            return 1;
        }
        return 0;
    }
    else 
    {
        syslog(LOG_WARNING, "Warning machine $mac_addr is more than once in DB");
        return 2;
    }
        
}

function update_Vlan( $switch_id, $port_number, $vlan_num )
{
    global $pg_handle;

    $request = pg_exec($pg_handle, "update port set vlan_num=$vlan_num where switch_id=$switch_id and port_number=$port_number" );

    if( !$request )
    {
        syslog( LOG_ALERT, "Error : PostgreSQL database has gone fishing !\n");
    }

    if( ($num = pg_cmdtuples( $request )) != 1 )
    {
        syslog( LOG_CRIT, "Error : Update made weird things (cmdtuples=$num)");
    }
}

function set_PortInternalId( $switch_id, $port_number, $port_internal_id )
{
    global $pg_handle;

    $request = pg_exec($pg_handle, "select port_id from port where port_number=$port_number and switch_id=$switch_id" );
    if( !$request )
    {
        echo "Error : PostgreSQL database has gone fishing !\n";
        return 1;
    }

    $num = pg_numrows( $request );

    if( $num == 0 )
    {
        syslog(LOG_ERR, "Error : port number $port_number not found on switch n�$switch_id");
    }
    else
    {
        $row=pg_fetch_row($request, 0);
        $port_id=$row[0];
        $res=pg_exec($pg_handle, "update port set port_internal_id=$port_internal_id where port_id=$port_id and port_protection=0;");
        if ($res==FALSE)
        {
            syslog(LOG_ERR, "Update port query could not be executed\n");
            return 1;
        }
    }
}

function set_vlanId($switch_id, $number, $id)
{
    global $pg_handle;

    $res=pg_exec($pg_handle, "update switch set vlan".$number."_id=$id where switch_id=$switch_id;");
    if ($res==FALSE)
    {
        syslog(LOG_ERR, "Update vlan_id query could not be executed\n");
        return 1;
    }
    return 0;
}

function Channel2VLAN( $channel )
{
    if( $channel == 0 )
    {
        return( 2 );
    }
    else
    {
        return( $channel + 3 );
    }
}

?>
