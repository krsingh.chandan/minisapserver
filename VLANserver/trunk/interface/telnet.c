/*****************************************************************************
 * telnet.c
 * Provide a telnet interface
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: telnet.c,v 1.25 2001/11/12 16:38:49 marcari Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>                                                /* sprintf */
#include <stdlib.h>                                          /* free, malloc */
#include <strings.h>                                               /* memset */
#include <string.h>                                               /* strncmp */
#include <unistd.h>                                    /* close, read, write */
#include <sys/socket.h>                                 /* socket, bind, ... */
#include <arpa/inet.h>                                            /* types.h */
#include <arpa/telnet.h>                                   /* WILL, IAC, ... */
#include <pthread.h>                              /* pthread_mutex_init, ... */
#include <errno.h>

#include "../types.h"
#include "../logger.h"
#include "telnet.h"
#include "interface.h"

#define PORT 7891
#define MAXCONNECTIONS 2 // how many connections are queued

/* TELNET Interface */
#define WELCOME    "\nVideoLAN Channel Server Administration System\n"
#define LOGIN      "Login: "
#define PASSWD     "Password: "
#define PROMPT     "@CHANNELServer "
#define PROMPT_END " # "

#define KEY_UP          65         //
#define KEY_DOWN        66         /* Various codes sent after the 0x1b byte */
#define KEY_LEFT        67         //
#define KEY_RIGHT       68         //


/* Protoypes of static functions further defined */

static ERR_CODE IF_connection_handle(struct IF_connection* connection);

static void IF_telnet_init(int isocket);
static char* IF_telnet_login(int isocket);
static void IF_telnet_commands(struct IF_connection* connection);

static void Menu_send(int iSocket, struct IF_Menu* mMenu);

static char* Catch_Word(int iSocket, struct IF_Menu* mMenu);
static char* Catch_Passwd(int iSocket);

static void Request_send(int iSocket, char bAction, char bOption);
static void Message_send(int iSocket, char* sMessage, size_t size);
static void Prompt_send(int iSocket, char* sUser, struct IF_Menu* mMenu);
  

/*****************************************************************************
 * IF_telnet_main() Main function of the telnet interface
 *****************************************************************************/
void* IF_telnet_main(void* p_void)
{
  signed int sock;
  struct sockaddr_in addr;
  struct sockaddr incoming;
  signed int size;
  signed int rc;
  signed int iOpt;
  pthread_attr_t thread_attr;
  pthread_t thread;
  fd_set readfds;
  struct IF_module* module;
  struct IF_connection* connection;
  struct timeval select_timeout;
  

  /*
   * Need to cast telnet
   */
  module=(struct IF_module*)p_void;

  size=sizeof(struct sockaddr_in);
  iOpt=1;
  
  /* Initialization */
  sock=socket(AF_INET,SOCK_STREAM,0);
  if(sock==-1)
  {
    VS_log(LOGERROR,TELNET,"Error creating socket !");
    return NULL;
  }

  
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);
  addr.sin_addr.s_addr = INADDR_ANY;
  memset(&(addr.sin_zero),0,sizeof(addr.sin_zero));
  
  setsockopt(sock,SOL_SOCKET, SO_REUSEADDR, &iOpt,sizeof(iOpt));
  rc=bind(sock,(struct sockaddr *)&addr,sizeof(struct sockaddr));
  if(rc==-1)
  {
    VS_log(LOGERROR,TELNET,"Unable to bind socket");
    close(sock);
    return NULL;
  }
  
  
  rc=listen(sock,MAXCONNECTIONS);
  if(rc==-1)
  {
    VS_log(LOGERROR,TELNET,"Unable to listen to the socket");
    close(sock);
    return NULL;
  }
  
  
  /*
   * New connection
   */
  while(!module->uDie)
  {
    FD_ZERO(&readfds);
    FD_SET(sock, &readfds);
    select_timeout.tv_sec=1;
    select_timeout.tv_usec=0;
    rc=select(sock+1, &readfds, NULL, NULL, &select_timeout);
    if(rc == -1)
    {
      VS_log(LOGINFO,TELNET,"Error in select statement : %s",strerror(errno));
      if (errno==EINTR) /* ctrl+C has probably been pressed */
	break;
      else
        continue;
    }
    else if(rc == 1)
    {
      connection=malloc(sizeof(struct IF_connection));
      if(connection == NULL)
      {
        VS_log(LOGERROR,TELNET,"Unable to allocate memory");
        continue;
      }
  
      connection->module=module;
  
      connection->iSocket=accept(sock,&incoming,&size);
  
      pthread_attr_init (&thread_attr);
      pthread_attr_setdetachstate (&thread_attr, PTHREAD_CREATE_DETACHED);
      rc=pthread_create(&thread, &thread_attr, (void *(*)())IF_connection_handle,
                        connection);
      if (rc)
      {
        VS_log(LOGINFO,TELNET,
               "The creation of a thread for a new connection doesn't succed !");
        free(connection);
      }
    }
  }
  
  close(sock);
  VS_log(LOGINFO,TELNET,"The module telnet is down");
  
  return NULL;
}


/*****************************************************************************
 * IF_connection_handle()
 *****************************************************************************
 * Handle the connection
 * Close the socket at the end of the connection
 *****************************************************************************/
static ERR_CODE IF_connection_handle(struct IF_connection* connection)
{
  IF_telnet_init(connection->iSocket);
  connection->sUser=IF_telnet_login(connection->iSocket);
  if(connection->sUser!=NULL)
  {
    connection->uLogout=0;
    IF_telnet_commands(connection);
  }

  VS_log(LOGINFO,TELNET,"Session closed for user %s",connection->sUser);
  
  close(connection->iSocket);
  free(connection);

  return VS_R_OK;
}


/*****************************************************************************
 * IF_telnet_init()
 *****************************************************************************
 * Send the welcome message
 * Send the terminal specifications
 *****************************************************************************/
static void IF_telnet_init(signed int isocket)
{
  VS_log(LOGDEBUG, TELNET, "Connection attempt");
 
  /* Send the Welcome message */
  Message_send(isocket, WELCOME, strlen(WELCOME));
    
  /* Need to negociate Terminal ... */
  Request_send(isocket, WILL, TELOPT_ECHO);
  Request_send(isocket, WILL, TELOPT_SGA);
  Request_send(isocket, WILL, TELOPT_OUTMRK);
  Request_send(isocket, DONT, TELOPT_LINEMODE);
}


/*****************************************************************************
 * IF_telnet_login()
 *****************************************************************************
 * Send the login sequence
 * Get the login and the passwd
 *****************************************************************************/
static char* IF_telnet_login (int iSocket)
{
  char* sUser;
  char* sPasswd;
  ERR_CODE rc;
  
  sUser=NULL;
  sPasswd=NULL;
  
  /*
   * Send LOGIN sequence
   */
  Message_send(iSocket, LOGIN, strlen(LOGIN));
  
  /*
   * Catch a valid user
   * TODO write a catch login
   */
  sUser=Catch_Word(iSocket, NULL);
  if(sUser == NULL || strlen(sUser)>9)
  {
    VS_log(LOGERROR,TELNET,"Non valid user login");
    return NULL;
  }
  
  /*
   * Send the PASSWD sequence
   */
  
  Message_send(iSocket, PASSWD, strlen(PASSWD));
  sPasswd=Catch_Passwd(iSocket);
  if(sPasswd == NULL)
  {
    VS_log(LOGERROR,TELNET,"Non valid user password");
    return NULL;
  }

  /*
   * Ask the interface to authentificate user.pass
   * Return sUser in case of success, NULL otherwise
   */
  rc=IF_Auth(sUser,sPasswd);
  switch(rc)
  {
    case VS_R_PARSE:
      Message_send(iSocket, "Error while parsing password file\r\n\r\n",37);
      return NULL;
    
    case VS_R_ABSENT:
    case VS_R_AUTH_FAILURE:
      Message_send(iSocket, "Login incorrect\r\n\r\n",19);
      return NULL;

    case VS_R_FILE:
      Message_send(iSocket, "Error while opening password file\r\n\r\n",37);
      return NULL;

    case VS_R_OK:
      return sUser;
      
    default:
      return NULL;
  }
  return NULL;
}

/*****************************************************************************
 * IF_telnet_commands()
 *****************************************************************************
 * Once logged main function
 * Catch the commands and interprete them
 *****************************************************************************/
static void IF_telnet_commands (struct IF_connection* connection)
{
  char* sCmd;
  char* sAnswer;
  struct IF_Menu* mMenu;
  unsigned int z; 

  mMenu = IF_Menu_init();
  
  while(!connection->module->uDie && !connection->uLogout)
  {
    Menu_send(connection->iSocket,mMenu);
    Prompt_send(connection->iSocket,connection->sUser,mMenu);
    
    sCmd=Catch_Word(connection->iSocket,mMenu);
    
    for(z=0;z<mMenu->uCnb;z++)
    {
      if(!strncmp(sCmd,mMenu->cCommand[z].sName,
                  strlen(mMenu->cCommand[z].sName))
          && (
            sCmd[strlen(mMenu->cCommand[z].sName)]==' ' ||
            sCmd[strlen(mMenu->cCommand[z].sName)]=='\0' ))
      {
        switch(mMenu->cCommand[z].ctType)
        {
          case COMMAND_TYPE_COMMAND:
            if (strlen(sCmd) > strlen(mMenu->cCommand[z].sName) )
            {
              sAnswer = mMenu->cCommand[z].uAction.function(
                           connection,
                           &sCmd[strlen(mMenu->cCommand[z].sName)+1]);
            }
            else
            {
              sAnswer = mMenu->cCommand[z].uAction.function(connection, NULL);
            }

            if (sAnswer)
            {
              Message_send(connection->iSocket, sAnswer, strlen(sAnswer));
              free(sAnswer);
            }
            break;
            
          case COMMAND_TYPE_SUBMENU:
            mMenu = mMenu->cCommand[z].uAction.mSubmenu;
            break;

          default:
        }
      }
    }
    free(sCmd);
  }  
}

 
/*****************************************************************************
 * Menu_send()
 *****************************************************************************
 * Send the Menu using Message_send() 
 *****************************************************************************/
static void Menu_send (int iSocket, struct IF_Menu* mMenu)
{
  unsigned int z;

  Message_send(iSocket,"\r\n",2);
  Message_send(iSocket,
  "------------------------- VLCS menu ------------------------------\r\n",68); 
  for(z=0; z<mMenu->uCnb; z++)
  {
    Message_send(iSocket,"- ",2);
    Message_send(iSocket,mMenu->cCommand[z].sName,
                                           strlen(mMenu->cCommand[z].sName));
    Message_send(iSocket,"                    ",
                                        21-strlen(mMenu->cCommand[z].sName));
    Message_send(iSocket,mMenu->cCommand[z].sDescr,
                                          strlen(mMenu->cCommand[z].sDescr));
    Message_send(iSocket,"\r\n",2);
  }
}



/*****************************************************************************
 * Catch_Word()
 *****************************************************************************
 * Read in the socket and catch chars with local echo
 * Return after a \r\n sequence
 *****************************************************************************/
static char* Catch_Word (int iSocket, struct IF_Menu* mMenu)
{
  unsigned char bBuff;
  char* sData;
  int iPos;
  unsigned int i,j,l;
  
  sData=malloc(300*sizeof(char));
  iPos=0;

  while(iPos<254)
  {
    read(iSocket, &bBuff,1);
    switch(bBuff)
    {
      case IAC:
        read(iSocket, &bBuff,1);
        read(iSocket, &bBuff,1);
        break;                                      //TODO Terminal negociation
      
      case '\r':
        sData[iPos]='\0';
        read(iSocket, &bBuff,1);         //Need to read next byte: should be \n
        Message_send(iSocket,"\r\n",2);
        return sData;

      case 0x7f:
        if(iPos > 0)
        {
          unsigned char b4Buff[4];
	  iPos--;
          sData[iPos]='\0';
          b4Buff[0]=0x08;
          b4Buff[1]=0x20;
          b4Buff[2]=0x08;
          Message_send(iSocket,b4Buff,3);
        }
	break;
         
      case 0x1b:
        read(iSocket, &bBuff,1);
        switch(bBuff)
        {
          case KEY_UP:
            /* TODO Write it */
          case KEY_DOWN:
        }
        break;                              // TODO Add test 0x1b 91 KEY_UP ../

      case 0x09:
        for(i=0;i<mMenu->uCnb;i++)                       /* loop on commands */
        {
          for(j=0;j<iPos;j++)                          /* loop on characters */
          {
            if(sData[j]!=mMenu->cCommand[i].sName[j])
            {
              break;
            }
            if(j==iPos-1)                                /* Time to complete */
            {
              for(l=iPos;l<strlen(mMenu->cCommand[i].sName);l++)
              {
                sData[l]=mMenu->cCommand[i].sName[l];
                Message_send(iSocket,&sData[l],1);
                iPos++;
              }
              i=mMenu->uCnb;
              break;
            }
          }
        }
        break;
        
      default:
        if (((bBuff>='0')&&(bBuff<='9')) || ((bBuff>='A')&&(bBuff<='Z')) || \
            ((bBuff>='a')&&(bBuff<='z')) || (bBuff=='.') || (bBuff==' ') || \
            (bBuff==':'))
        {
          sData[iPos]=bBuff;
          iPos++;
          Message_send(iSocket,&bBuff,1);
        }
        break;
    }
  }
  return NULL;
}


/*****************************************************************************
 * Catch_Passwd()
 *****************************************************************************
 * Read in the socket and catch chars BUT without any local echo
 * Return after a \r\n sequence
 *****************************************************************************/
static char* Catch_Passwd (int iSocket)
{
  unsigned char bBuff;
  char* sData;
  int iPos;
  
  sData=malloc(300*sizeof(char));
  if(sData==NULL)
  {
    VS_log(LOGERROR,TELNET,"Unable to allocate memory");
    return NULL;
  }

  iPos=0;

  while(iPos<254)
  {
    read(iSocket, &bBuff, 1);
    switch(bBuff)
    {
      case IAC:
        read(iSocket, &bBuff, 2);
        break;                                      //TODO Terminal negociation
      
      case '\r':
        sData[iPos]='\0';
        read(iSocket, &bBuff, 1);        //Need to read next byte: should be \n
        Message_send(iSocket,"\r\n", 2);
        return sData;

      default:
        sData[iPos]=bBuff;
        iPos++;
        break;
    }
  }
  return NULL;
}


/*****************************************************************************
 * Request_send()
 *****************************************************************************
 * Send all commands to the telnet client
 * A command should begin with IAC, then Action, then Option
 *****************************************************************************/
static void Request_send (int iSocket, char bAction, char bOption)
{
  unsigned char req[3];
  
  req[0]=IAC;
  req[1]=bAction;
  req[2]=bOption;
  
  write(iSocket, req, 3);
}



/*****************************************************************************
 * Message_send()
 *****************************************************************************
 * Write in the socket
 *****************************************************************************/
static void Message_send (int iSocket, char* sMessage, size_t size)
{
  write(iSocket, sMessage,size);
}

/*****************************************************************************
 * Prompt_send()
 *****************************************************************************
 * Send a prompt, with the PROMPT def and the sUser variable
 * Begin with a \r\n sequence
 *****************************************************************************/
static void Prompt_send (int iSocket, char* sUser, struct IF_Menu* mMenu)
{
  Message_send(iSocket,"\r\n",2);
  Message_send(iSocket, sUser,strlen(sUser));
  Message_send(iSocket, PROMPT,strlen(PROMPT));
  Message_send(iSocket, mMenu->sTitle, strlen(mMenu->sTitle));
  Message_send(iSocket, PROMPT_END, strlen(PROMPT_END));
}
