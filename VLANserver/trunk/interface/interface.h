/*****************************************************************************
 * interface.h
 * Header file for interface.c 
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: interface.h,v 1.14 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#define PASSWD_FILE "passwd"

struct IF_connection;

ERR_CODE IF_start(void);
char* IF_module_stop(struct IF_connection* connection, char* douze);
char* IF_connection_stop(struct IF_connection* connection, char* douze);
char* IF_reload(struct IF_connection* connection);
char* IF_dump_macs(struct IF_connection* connection);
char* IF_setVlan(struct IF_connection* connection, char* sMess);
char* IF_walk_channel(struct IF_connection* connection, char* sChannel);
char* IF_reset_channels(struct IF_connection* connection);
char* IF_info(struct IF_connection* connection, char* sMac);
char* IF_log(struct IF_connection* connection);
struct IF_Menu* IF_Menu_init();
ERR_CODE IF_Auth(char* sUser, char* sPasswd);

#define COMMAND_NAME_MAX_LEN 17
#define COMMAND_DESCR_MAX_LEN 40

#define MODULE_SPECIFIC_COMMANDS_NB 1

typedef unsigned char CommandT;

#define COMMAND_TYPE_COMMAND 0
#define COMMAND_TYPE_SUBMENU 1

/* The number of the line displayed by the IF_walk_channel function */
#define IF_LINES_NB 22

/*********************
 * Menu descriptions *
 *********************/
struct IF_Menu
{
  struct IF_Menu* mPrev;  
  struct IF_Command* cCommand;
  unsigned int uCnb;
  char*  sTitle;
};

struct IF_Command
{
  unsigned int uIndex;
  CommandT ctType;
  char* sName;
  char* sDescr;
  union
  {
    char* (*function)(struct IF_connection*,char*);
    struct IF_Menu* mSubmenu;
  } uAction;
};

/************************
 * Modules descriptions *
 ************************/

struct IF_module
{
  unsigned int uDie;
};

/**************************
 * Connection dexcription *
 **************************/

struct IF_connection
{
  unsigned int uLogout;
  signed int iSocket;
  struct IF_module * module;
  char* sUser;
};

#endif __INTERFACE_H__
