/*****************************************************************************
 * interface.c
 * Main functions that are use by interfaces
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: interface.c,v 1.21 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <string.h>                                               /* strncmp */
#include <arpa/inet.h>                                            /* types.h */
#include <stdlib.h>                                          /* malloc, free */
#include <unistd.h>                                                  /* read */
#include <sys/time.h>                                      /* struct timeval */
#include <pthread.h>                                       /* pthread_create */
#include <semaphore.h>                                               /* db.h */
#include <crypt.h>                                         /* MD5 encryption */ 
#include <ucd-snmp/asn1.h>                                  /* snmp_switch.h */
#include <ucd-snmp/snmp_impl.h>                             /* snmp_switch.h */
#include <ucd-snmp/snmp_api.h>                              /* snmp_switch.h */

#include "../types.h"
#include "../logger.h"
#include "interface.h"

#include "../snmp/snmp_switch.h"                       /* struct SNMP_switch */
#include "../snmp/if_snmp.h"
#include "../db/db.h"                                    /* DB_machines_lock */
#include "../config/config.h"                                     /* vs->cfg */
#include "telnet.h"                                             /* IF_telnet */
#include "../server/server.h"                          /* VS_reinit_switches */
#include "../vlanserver.h"                                            /* vs  */

static inline ERR_CODE IF_Menu_New_Directory(struct IF_Menu* mMenu,
                                              struct IF_Menu* mPrev,
                                              const char* sTitle);

static inline ERR_CODE IF_Menu_Add_Command(struct IF_Menu* mMenu,
                                           unsigned int uIndex,
                                           CommandT ctType,
                                           void* pAction,
                                           const char* sName,
                                           const char* sDescr);


/*****************************************************************************
 * IF_start : Starts the interface module
 *****************************************************************************
 * Called from the main part without argument it will start the telnetd
 *****************************************************************************/
ERR_CODE IF_start(void)
{
  pthread_t iftelnet;
  struct IF_module* telnet;
  
  VS_log(LOGINFO,IF,"Creating the Interface..");
  
  /*
   * Creates the structure telnet
   */
  telnet=malloc(sizeof(struct IF_module));
  if(telnet==NULL)
  {
    VS_log(LOGERROR,IF,"Error allocating memory for telnet structure");
    return VS_R_MEMORY;
  }
  telnet->uDie=0;
  
  
  /*
   * Starts the new thread
   */
  if (pthread_create(&iftelnet,NULL,IF_telnet_main,telnet))
  {
    VS_log(LOGERROR,IF,"Error creating interface module thread");
    return VS_R_PTHREAD;
  }
  return 0;
}


/*****************************************************************************
 * IF_module_stop : Stops the interface module
 *****************************************************************************
 * Called from the main part it will stop
 * the appropriate module
 *****************************************************************************/
char* IF_module_stop(struct IF_connection* connection, char* douze)
{
  connection->module->uDie=1;
  return NULL;
}


/*****************************************************************************
 * IF_connection_stop : Stops the connectionj of a user
 *****************************************************************************
 * Called from the main part it will stop
 * the appropriate connection
 *****************************************************************************/
char* IF_connection_stop(struct IF_connection* connection, char* douze)
{
  connection->uLogout=1;
  return NULL;
}


/*****************************************************************************
 * IF_Menu_init : Returns a Menu structure
 *****************************************************************************
 * Called from a interface module, it will return a Menu structure
 * describing all the available commands
 * Generates submenus. Main menu commands are module specific
 *****************************************************************************/
struct IF_Menu* IF_Menu_init()
{
  struct IF_Menu* mMenu;

  /*
   * Allocating Memory for mMenu structure
   */
  mMenu = malloc(sizeof(struct IF_Menu));
  if(mMenu == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory for menu structure");
    return NULL;
  }

  /*
   * Initializing mMenu base structure
   */
  


  /*
   * Initializing main menu
   */
  IF_Menu_New_Directory(mMenu, NULL, "/");

  IF_Menu_Add_Command(mMenu, 0,COMMAND_TYPE_SUBMENU,NULL,"db",
                                             "DATABASE specific functions ->");

  IF_Menu_Add_Command(mMenu, 1,COMMAND_TYPE_SUBMENU,NULL,"snmp",
                                             "SNMP specific functions ->");

  IF_Menu_Add_Command(mMenu, 2, COMMAND_TYPE_SUBMENU, NULL,
                                        "log", "LOGGER specific functions ->");

  IF_Menu_Add_Command(mMenu, 3,COMMAND_TYPE_SUBMENU,NULL,"system",
                                             "SYSTEM specific functions ->");

  IF_Menu_Add_Command(mMenu, 4, COMMAND_TYPE_COMMAND, IF_connection_stop,
                                        "logout", "Close current session");
 

  /*
   * Initializing db submenu
   */
  IF_Menu_New_Directory(mMenu->cCommand[0].uAction.mSubmenu, mMenu, "/db/");

  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 0,
                      COMMAND_TYPE_COMMAND, IF_reload, 
                      "reload", "Reload the database");
    
  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 1,
                      COMMAND_TYPE_COMMAND, IF_info,
                      "info", "Displays supposed location of a MAC");

  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 2,
                      COMMAND_TYPE_COMMAND, IF_walk_channel,
                      "channel", "Displays all clients in specified channel");
  
  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 3,
                      COMMAND_TYPE_COMMAND, IF_reset_channels,
                      "reset", "Puts everyone to the default channel : 0");
  
  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 4,
                      COMMAND_TYPE_COMMAND, IF_dump_macs,
                      "dump", "Dumps to log all macs in the db (debug only)");
  
  IF_Menu_Add_Command(mMenu->cCommand[0].uAction.mSubmenu, 5,
                      COMMAND_TYPE_SUBMENU, mMenu,
                      "..", "->");
 

  /*
   * Initializing snmp submenu
   */
  IF_Menu_New_Directory(mMenu->cCommand[1].uAction.mSubmenu, mMenu, "/snmp/");

  IF_Menu_Add_Command(mMenu->cCommand[1].uAction.mSubmenu, 0,
                      COMMAND_TYPE_COMMAND, IF_setVlan,
                      "setVlan", "Put a port in a vlan");
  IF_Menu_Add_Command(mMenu->cCommand[1].uAction.mSubmenu, 1,
                      COMMAND_TYPE_SUBMENU, mMenu,
                      "..", "->");
   

  /*
   * Initializing log submenu
   */
  IF_Menu_New_Directory(mMenu->cCommand[2].uAction.mSubmenu, mMenu, "/log/");

  IF_Menu_Add_Command(mMenu->cCommand[2].uAction.mSubmenu, 0,
                      COMMAND_TYPE_COMMAND, IF_log,
                      "log", "Draws the log on the interface N/A");
    
  IF_Menu_Add_Command(mMenu->cCommand[2].uAction.mSubmenu, 1,
                      COMMAND_TYPE_SUBMENU, mMenu,
                      "..", "->");
  
  /*
   * Initializing log submenu
   */
  IF_Menu_New_Directory(mMenu->cCommand[3].uAction.mSubmenu, mMenu, "/system/");

  IF_Menu_Add_Command(mMenu->cCommand[3].uAction.mSubmenu, 0,
                      COMMAND_TYPE_COMMAND, IF_module_stop,
                      "modLogout", "Close the telnet module");
  
  IF_Menu_Add_Command(mMenu->cCommand[3].uAction.mSubmenu, 1,
                      COMMAND_TYPE_SUBMENU, mMenu,
                      "..", "->");

  return mMenu;
}

/****************************************************************************
 * IF_Menu_New_Directory: Creates a new subdirectory
 ****************************************************************************/
static inline ERR_CODE IF_Menu_New_Directory(struct IF_Menu* mMenu,
                                              struct IF_Menu* mPrev,
                                              const char* sTitle)
{
  mMenu->uCnb = 0;
  mMenu->mPrev = mPrev;
  mMenu->cCommand = NULL;
  mMenu->sTitle = strdup(sTitle);
  if(mMenu->sTitle == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory for mMenu structure");
    return VS_R_MEMORY;
  }
  return VS_R_OK; 
}
/*****************************************************************************
 * IF_Menu_Add_Command: Add a command inside a menu structure
 *****************************************************************************/
static inline ERR_CODE IF_Menu_Add_Command(struct IF_Menu* mMenu,
                                           unsigned int uIndex,
                                           CommandT ctType,
                                           void* pAction,
                                           const char* sName,
                                           const char* sDescr)
{
  struct IF_Command* cCurrent;
  
  /*
   * Reallocating to improve size of menu
   */
  mMenu->uCnb = uIndex+1;
  mMenu->cCommand = realloc(mMenu->cCommand, 
                                (uIndex+1)*sizeof(struct IF_Command));
  if(mMenu->cCommand == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory for Menu struct");
    return VS_R_MEMORY;
  }
  cCurrent = mMenu->cCommand+uIndex;
  
  
  /*
   * Writing index id
   */
  cCurrent->uIndex = uIndex;

  
  /*
   * Writing command type
   */
  cCurrent->ctType = ctType;


  
  if(ctType == COMMAND_TYPE_SUBMENU)
  {
   
    if(pAction!=NULL)
    {
      /*
       * If pAction set, pointer to the submenu
       */
      cCurrent->uAction.mSubmenu = pAction;
    }
    else
    {
      /*
       * If NULL given, we need here to malloc for the new submenu
       */
      cCurrent->uAction.mSubmenu = malloc(sizeof(struct IF_Menu));
      if(cCurrent->uAction.mSubmenu==NULL)
      {
        VS_log(LOGERROR,IF,"Unable to allocate memory for mMenu struture");
        return VS_R_MEMORY;
      }
    }
  }
  else
  {
    /*
     * We need here to point on the appropriate function
     */
    cCurrent->uAction.function = pAction;
  }


  /*
   * Writing sName
   */
  cCurrent->sName = strdup(sName);
  if(cCurrent->sName == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory for mMenu structure");
    return VS_R_MEMORY;
  }

  
  /*
   * Write sDescr
   */
  cCurrent->sDescr = strdup(sDescr);
  if(cCurrent->sDescr == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory for mMenu structure");
    return VS_R_MEMORY;
  }

  return VS_R_OK;
}


/*****************************************************************************
 * IF_reload: Order for a reload of DB
 *****************************************************************************
 * Called from any of the interface submodules
 * Warning: no return values have been defined !
 *****************************************************************************/
char* IF_reload(struct IF_connection* connection)
{
  char * sMessage;
  
  vs->info_poller->uTime=VS_REINIT_TIME;
  
  VS_reinit_switchs(vs->info_poller); 
  
  sMessage=malloc(256*sizeof(char));
  if(sMessage==NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    return NULL;
    // return "Memory error";
  }
  
  strcpy(sMessage,"Reload completed\r\n");
  return sMessage;
}


/*****************************************************************************
 * IF_Auth: Log a user with a password
 *****************************************************************************
 * Called from any of the interface submodules
 * Use MD5 encryption
 *****************************************************************************/
ERR_CODE IF_Auth(char* sUser, char* sPasswd)
{
  FILE * fPasswd;                           /* File descriptor for passwords */ 
  char sCryptedPasswd[14];                            /* stored MD5 password */
  char sSalt[2];                              /* Password encryption entropy */
  char sName[9];                                  /* users in passwords file */
  
  /*
   * Open Password file
   */
  fPasswd = fopen(PASSWD_FILE, "r");
  if(fPasswd==NULL)
  {
    VS_log(LOGERROR,TELNET, "Password file open error : %s",PASSWD_FILE);
    return VS_R_FILE;
  }
  VS_log(LOGDEBUG,TELNET, "Connection opened for %s",sUser);
  
  
  /*
   * Search the right entry inside the file
   */
  do
  {
    if(fscanf(fPasswd, "%8s %13s", sName, sCryptedPasswd) != 2)
    {
      VS_log(LOGINFO,TELNET, "User %s not found or unknown", sUser);
      fclose(fPasswd);
      return VS_R_ABSENT;
    }
  } while (strcmp(sUser, sName));

  
  /*
   * Close Password file
   */
  fclose(fPasswd);
  
  
  /*
   * Gets the salt (2 first bytes)
   * And compare sCryptedPasswd with result of crypt
   */
  sSalt[0]=sCryptedPasswd[0];
  sSalt[1]=sCryptedPasswd[1];
  
  if(strncmp(crypt(sPasswd, sSalt), sCryptedPasswd,strlen(sCryptedPasswd)))
  {
    VS_log(LOGINFO,TELNET, "Wrong password for user %s", sUser);
    return VS_R_AUTH_FAILURE;
  }

  /*
   * At this point, authentification is ok
   */
  VS_log(LOGINFO,TELNET, "User %s, login successful", sUser);
  
  
  return VS_R_OK;
}


/*****************************************************************************
 * IF_setVlan : Order for setting a port in a vlan
 *****************************************************************************
 * Called from any of the interface submodules
 *****************************************************************************/
char* IF_setVlan(struct IF_connection* connection, char* sMess)
{
  unsigned char bBuff;
  char sMac[18];
  int end;
  int iSocket = connection->iSocket;
  unsigned int rc;
  unsigned int ui1, ui2, ui3, ui4, ui5, ui6;
  unsigned int ui7, ui8, ui9, ui10;
  unsigned int uVlan;
  unsigned int uChannel;
  unsigned int uVlanType;
  struct timeval tv;
  struct VS_info_client* sInfoClient;
  FILE* streamOut;
  char* sMessage;
  char sVlanType[7];
  
  sMessage=malloc(256*sizeof(char));
  if(sMessage==NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    return NULL;
  }
  sInfoClient=malloc(sizeof(struct VS_info_client));
  if(sInfoClient == NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    free(sMessage);
    return NULL;
  }

  sMac[17]=0;
  
  if((sMess == NULL) || (sscanf(sMess, "%2x:%2x:%2x:%2x:%2x:%2x %3u.%3u.%3u.%3u %2u %1u",
                                &ui1, &ui2, &ui3, &ui4, &ui5, &ui6, &ui7, &ui8,
                                &ui9, &ui10, &uVlan, &uVlanType) != 12))
  {
    VS_log(LOGINFO,IF,"No MAC/IP/vlanType specify in snmp->setVlan function");
    strcpy(sMessage,"You have to specify the MAC IP vlanNb vlanType after "\
                    "the command\r\n"\
                    "For the type choose in \r\n"\
                    "0->NONE, 1->802.1Q, 2->VLT, 3->ATM\r\n");
    free(sInfoClient);
    return sMessage;
  }
  
  /* Check the type of the mac */
  sprintf(sMac,"%02X:%02X:%02X:%02X:%02X:%02X",ui1,ui2,ui3,ui4,ui5,ui6);
  /* Chack the type of the IP */
  sprintf(sInfoClient->ipaddr,"%u.%u.%u.%u",ui7,ui8,ui9,ui10);

  sInfoClient->session_id=0;
  rc=gettimeofday(&tv,NULL);
  if(rc)
  {
    sprintf(sMessage,"Unable to get the date of the server\r\n");
    free(sInfoClient);
    return sMessage;
  }
  sInfoClient->date_se=tv.tv_sec;
  
  VlanToChannel(*(vs->cfg),uVlan,uChannel);
  sprintf(sInfoClient->mess,"%u %u %lu %s",uChannel,12,tv.tv_sec,
          sMac);

  sInfoClient->iSocket = connection->iSocket;
  
  switch(uVlanType)
  {
    case 0:
      sInfoClient->vlanType=NONE;
      sprintf(sVlanType,"NONE");
      break;
    case 1:
      sInfoClient->vlanType=T8021Q;
      sprintf(sVlanType,"802.1 Q");
      break;
    case 2:
      sInfoClient->vlanType=VLT;
      sprintf(sVlanType,"VLT");
      break;
    case 3:
      sInfoClient->vlanType=ATM;
      sprintf(sVlanType,"ATM");
      break;
    default:
      sprintf(sMessage,"vlanType unknown\r\n");
      free(sInfoClient);
      return sMessage;
  }

  sprintf(sMessage,"\r\nMac: %s IP: %s vlanDestination: %u vlanType: %s\r\n"\
                   "Do you want to continue y/n ? [y]",
                   sMac, sInfoClient->ipaddr, uVlan, sVlanType);
  
  write(iSocket, sMessage, strlen(sMessage));
  streamOut=fdopen(iSocket, "w");
  end=0;
  while(end==0)
  {
    read(iSocket, &bBuff, 1);
    switch(bBuff)
    {
      case '\r':
      case 'y':
        read(iSocket, &bBuff,1);
        end=1;
        break;
        
      default:
        sprintf(sMessage,"\r\nAborting\r\n");
        free(sInfoClient);
        return sMessage;
    }
  } 
  
  sprintf(sMessage,"\r\nThe server is handling your request\r\n");
  
  VS_request_handler_thread(sInfoClient);

  write(iSocket, sMessage, strlen(sMessage));
  
  /* KLUDGE */
  sleep(2);
 
  free(sMessage);
  
  return NULL;
}


/*****************************************************************************
 * IF_info: Order for info request
 *****************************************************************************
 * Called from any of the interface submodules
 * Return value: the answer (char *)
 *****************************************************************************/
char* IF_info(struct IF_connection* connection, char* sMac)
{
  unsigned int rc;
  unsigned int i1, i2, i3, i4, i5;
  VS_MachineId mMac;
  char* sBuffer;
  struct DB_machines_elt* machine_elt;
  char* sMessage;
  struct SNMP_switch * zwitch;
  struct DB_port * port;
  struct VS_vlan* vlanLoop;

  sMessage=malloc(256*sizeof(char));
  if(sMessage==NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    return NULL;
    // return "Memory error";
  }
  sBuffer=malloc(256*sizeof(char));
  if(sBuffer==NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    strcpy(sMessage,"Unable to allocate memory");
    return sMessage;
  }
  
 
  if((sMac==NULL) || (sscanf(sMac, "%2x:%2x:%2x:%2x:%2x:%2x", (unsigned *)&mMac,&i1,
                      &i2, &i3, &i4, &i5)!=6))
  {
    VS_log(LOGINFO,IF,"No MAC specify in db->info function");
    strcpy(sMessage,"You have to specify the MAC after the command\r\n");
    free(sBuffer);
    return sMessage;
  }
    
  mMac<<=8;
  mMac|=i1;
  mMac<<=8;
  mMac|=i2;
  mMac<<=8;
  mMac|=i3;
  mMac<<=8;
  mMac|=i4;
  mMac<<=8;
  mMac|=i5;
  
  /* 2. Looking for Mac inside db */
  rc=DB_machines_lock(vs->db, mMac, &machine_elt);

  if(rc)
  {
    sprintf(sMessage,"Mac not found in database\r\n");
  }
  else
  {
    rc=DB_switchs_port_lock(vs->db, machine_elt->switch_ip,\
                                     machine_elt->port, &port, &zwitch);
    if(rc)
    {
      sprintf(sMessage,"Mac found on switch:%s port:%u\r\n but switch"\
                               "unknown\r\n",VS_SID2A(machine_elt->switch_ip),\
                                                  (unsigned)machine_elt->port);
    }
    else
    {
      if(zwitch->machines[(unsigned)machine_elt->port]->next!=NULL)
      {
        sprintf(sMessage,"Mac found on switch:%s port:%u\r\n"\
                         "mac is NOT alone on this port\r\n"\
                         "and the port is a member of the VLANs:\r\n",\
                                             VS_SID2A(machine_elt->switch_ip),\
                                                  (unsigned)machine_elt->port);
        for(vlanLoop=zwitch->ports[machine_elt->port].vlan_struct;
            vlanLoop!=NULL;
            vlanLoop=vlanLoop->next)
        {
          sprintf(sBuffer,"VLAN Nb: %hu Tagging: ",vlanLoop->vlan);
          sMessage=strcat(sMessage,sBuffer);
          switch(vlanLoop->vlanType)
          {
            case NONE:
              sprintf(sBuffer,"None\r\n");
              break;
            case VLT:
              sprintf(sBuffer,"VLT\r\n");
              break;
            case T8021Q:
              sprintf(sBuffer,"802.1Q\r\n");
              break;
            case ATM:
              sprintf(sBuffer,"ATM\r\n");
              break;
            default:
              sprintf(sBuffer,"Unknow\r\n");
              break;
          }
          sMessage=strcat(sMessage,sBuffer);
        }
      }
      else
      {
        sprintf(sMessage,"Mac found on switch:%s port:%u\r\n"\
                                               "mac is alone on this port\r\n"\
                                   "and the port is a member of the VLANs:\r\n",\
                                             VS_SID2A(machine_elt->switch_ip),\
                                                  (unsigned)machine_elt->port);
        for(vlanLoop=zwitch->ports[machine_elt->port].vlan_struct;
            vlanLoop!=NULL;
            vlanLoop=vlanLoop->next)
        {
          sprintf(sBuffer,"VLAN Nb: %hu Tagging: ",vlanLoop->vlan);
          sMessage=strcat(sMessage,sBuffer);
          switch(vlanLoop->vlanType)
          {
            case NONE:
              sprintf(sBuffer,"None\r\n");
              break;
            case VLT:
              sprintf(sBuffer,"VLT\r\n");
              break;
            case T8021Q:
              sprintf(sBuffer,"802.1Q\r\n");
              break;
            case ATM:
              sprintf(sBuffer,"ATM\r\n");
              break;
            default:
              sprintf(sBuffer,"Unknow\r\n");
              break;
          }
          sMessage=strcat(sMessage,sBuffer);
        }
      }
      DB_switchs_port_unlock(vs->db, port);
    }
    DB_machines_unlock(vs->db, machine_elt);
  }

  free(sBuffer);
  /* 3. Message has to send to the module */
  return sMessage; 
}


/*****************************************************************************
 * IF_reset_channels : Puts everyone back to default channel
 *****************************************************************************
 * Called from any of the interface submodules
 * Return value: nothing
 * TODO : test if the port uses 802.1Q tagging
 *****************************************************************************/
char* IF_reset_channels(struct IF_connection* connection)
{
  VS_VLAN vlanDest;
  unsigned int rc;
  struct CFG_SWITCH * zwitch_loop;
  VS_PORT num_port;
  struct DB_port * port;
  struct SNMP_switch * zwitch;

  vlanDest=ChannelToVlan(*vs->cfg,0); /* gets the default VLAN, channel 0 */
  
  zwitch_loop=vs->cfg->zwitches;                /* first switch in the list */
  while(zwitch_loop!=NULL)
  {

    for( num_port=1 ; num_port <= zwitch_loop->nports ; num_port++ )
    {
      /* locks the port we are going to modify */
      rc=DB_switchs_port_lock(vs->db,zwitch_loop->ip, num_port, &port, &zwitch);
      if (rc)
      {
        VS_log(LOGWARNING,IF,"Can't lock : Port n�%u, switch %s",num_port,
                              VS_SID2A(zwitch_loop->ip));
        continue;
      }
      /* don't want to modify a protected port */
      if (zwitch->ports[num_port].protection==VS_PL_ZERO)
      {
        rc=SNMP_set_vlan(vs->snmp, zwitch, num_port, vlanDest, NONE);
        if (rc) 
        {
          VS_log(LOGWARNING,IF,"Can't set default vlan : Port n�%u, switch %s",
                              num_port, VS_SID2A(zwitch_loop->ip));
        }
      }
      
      DB_switchs_port_unlock(vs->db, port);
    }
    zwitch_loop=zwitch_loop->next;              /* next switch in the list */
  }
  return NULL;
}

/*****************************************************************************
 * IF_dump_macs : Dumps raw db entry to the log intf
 *****************************************************************************
 * Called from any of the interface submodules
 * Return value: nothing
 *****************************************************************************/
char* IF_dump_macs(struct IF_connection* connection)
{
  struct DB_machines_elt * z;
  unsigned int hash;
  
  for(hash=0;hash<DB_MACHINES_HASH;hash++)
  {
    for (z=vs->db->machines[hash];
          z!=NULL;z=z->next)
    {
      VS_log(LOGDEBUG,DB,"In DB : %012LX",z->mac);   
    }
  }
  
  return NULL;
}

/*****************************************************************************
 * IF_walk_channel: Ask the db for all client in the specified channel
 *****************************************************************************
 * Called from the telnet submodule
 * Return value: NULL for the moment
 * KLUDGE : this function writes directly to the socket !
 *****************************************************************************/
char* IF_walk_channel(struct IF_connection* connection, char* sChannel)
{
  VS_VLAN vlanTarget;
  unsigned int rc;
  int end;
  VS_CHANNEL channel;
  char* sMessage;
  struct CFG_SWITCH * zwitch_loop;
  VS_PORT num_port;
  unsigned char bBuff;
  unsigned char count=0;
  struct DB_port * port;
  struct SNMP_switch * zwitch;
  struct VS_vlan* vlanLoop;
  unsigned int audimat;
/*
  unsigned int i1, i2, i3, i4, i5;
  VS_MachineId mMac;
  char* sBuffer;
  struct DB_machines_elt* machine_elt;
*/
  sMessage=calloc(256,sizeof(char));
  if(sMessage==NULL)
  {
    VS_log(LOGERROR,IF,"Unable to allocate memory");
    return NULL;
    // return "Memory error";
  }
  
 
  if((sChannel==NULL) || (sscanf(sChannel, "%u", (unsigned *)&channel)!=1))
  {
    VS_log(LOGINFO,IF,"No channel specified in IF_walk_channel");
    strcpy(sMessage,"You have to specify the channel after the command\r\n");
    return sMessage;
  }
    
  if (channel>=vs->cfg->nchannels)
  {
    VS_log(LOGWARNING,IF,"Wrong channel number in IF_walk_channel:%u",channel);
    sprintf(sMessage,"Wrong channel number specified (max=%u)\r\n",
                                                          vs->cfg->nchannels-1);
    return sMessage;
  }

  vlanTarget=ChannelToVlan(*vs->cfg,channel);   /* gets the VLAN */
  
  audimat=0;
  
  zwitch_loop=vs->cfg->zwitches;                /* first switch in the list */
  while(zwitch_loop!=NULL)
  {

    for( num_port=1 ; num_port <= zwitch_loop->nports ; num_port++ )
    {
      rc=DB_switchs_port_lock(vs->db,zwitch_loop->ip, num_port, &port, &zwitch);
      if (rc)
      {
        VS_log(LOGWARNING,IF,"Port n�%u, does not exist on switch:%s",num_port,
                              VS_SID2A(zwitch_loop->ip));
      }
      else
      {

        /* Checks all the vlans the port belongs to to see if the
         * requested one is in the list */
        for(vlanLoop=zwitch->ports[num_port].vlan_struct;
            vlanLoop!=NULL;
            vlanLoop=vlanLoop->next)
        {
          if (vlanLoop->vlan==vlanTarget)
          {
            /* Print IF_LINES_NB lines and wait */
            if(count==IF_LINES_NB)
            {
              sprintf(sMessage,"Press q to exit or any other key to continue "\
                                                                     "...\r\n");
              write(connection->iSocket, sMessage, strlen(sMessage));
              fdopen(connection->iSocket, "w");
              end=0;
              while(end==0)
              {
                read(connection->iSocket, &bBuff, 1);
                switch(bBuff)
                {
                  case 'q':
                      DB_switchs_port_unlock(vs->db, port);
                      return NULL;
                  default:
                    end=1;
                    break;
                }
              }
              count=0;
            }
            
          // TODO : le port est bien dans le vlan, �� serait bien d'avoir la
          // liste des MACs derri�re
            audimat++;
            sprintf(sMessage,"Switch : %s \tPort : %u (tagging :", 
                      VS_SID2A(zwitch_loop->ip), num_port);
            write(connection->iSocket, sMessage, strlen(sMessage));
            switch(vlanLoop->vlanType)
            {
              case NONE:
                sprintf(sMessage,"None)\r\n");
                write(connection->iSocket, sMessage, strlen(sMessage));
                break;
              case T8021Q:
                sprintf(sMessage,"802.1Q)\r\n");
                write(connection->iSocket, sMessage, strlen(sMessage));
                break;
              case VLT:
                // do not write because it is too much verbose
                // just go at the begining of the line in order to rewrite it !
                sprintf(sMessage,"\r");
                write(connection->iSocket, sMessage, strlen(sMessage));
                break;
              case ATM:
                sprintf(sMessage,"\r");
                write(connection->iSocket, sMessage, strlen(sMessage));
                break;
              default:
                sprintf(sMessage,"Unknow)\r\n");
                write(connection->iSocket, sMessage, strlen(sMessage));
                break;
            }
            count++;
          }
        } /* for(vlanLoop) */
        
        DB_switchs_port_unlock(vs->db, port);
      } /* if DB_switch_port_lock */
    }
    zwitch_loop=zwitch_loop->next;              /* next switch in the list */
  } /* while(zwitch_loop) */
  sprintf(sMessage,"Audimat de la chaine en ce moment : %u\r\n", audimat);
  write(connection->iSocket, sMessage, strlen(sMessage));
  return NULL;
}


/*****************************************************************************
 * IF_log()
 *****************************************************************************
 * Display logs on the interface socket until data is received from this
 * socket (eg: key pressed in the telnet interface)
 * TODO : test some err_codes
 *****************************************************************************/

char * IF_log(struct IF_connection * connection)
{
  FILE * streamOut;
  unsigned char bBuff;
  char * msg = "<< Press any key to stop logging >>\r\n\r\n";
  int end;
  int iSocket = connection->iSocket;
  
  write(iSocket, msg, strlen(msg));

  
  /* convertit la socket en flux de donn�es */
  streamOut=fdopen(iSocket, "w");

  /* inscrit le flux correspondant dans la liste du logger*/
  LOG_create(streamOut, LOGTYPE_TELNET);

  VS_log(LOGINFO, IF, "Log stream succesfully opened on interface");
  
  
  /* attend qu'on appuie sur une touche */
  end=0;
  while(end==0)
  {
    read(iSocket, &bBuff, 1);
    switch(bBuff)
    {
/*      case IAC:
        read(iSocket, &bBuff,1);
        read(iSocket, &bBuff,1);
        break; */                                     //TODO Terminal negociation
                                                    // see below about telnet

      case '\r':
        read(iSocket, &bBuff,1);         // Need to read next byte: should be \n
                                         // XXX : telnet specific, this should be
                                         // encapsulated by the telnet interface
        
      default:                           // simple key -> end
        end=1;
        break;
    }
  } 
  /* retire le flux de la liste des sorties de log */
  LOG_delete(streamOut);
 
  return NULL;
}
