/*****************************************************************************
 * connect.h
 * Header file for conect.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: connect.h,v 1.6 2001/10/24 20:35:16 marcari Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __CONNECT_H__
#define __CONNECT_H__

/* Tells the vlanbridge a machine has changed channel. The VLANserver must have
 * logged on and its fdescr field must be set */
unsigned int CNX_vlb_set_vlan(unsigned int session_id,\
                              char *MAC,char *IP,VS_CHANNEL CHANdest,\
                              VS_CHANNEL CHANsrc);

/* Logs on the VLANbridge. fdescr should point to the fdescr field of the
 * VLANserver. Current vlb requires version>=3.1 */
unsigned int CNX_vlb_login(char *version);

#endif
