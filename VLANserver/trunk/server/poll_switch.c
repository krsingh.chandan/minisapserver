/*****************************************************************************
 * poll_switch.c
 * Function to order SNMP init of switches
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: poll_switch.c,v 1.22 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Laurent Rosier <gunther@via.ecp.fr>
 *          Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>                                                /* malloc */
#include <unistd.h>                                                /* usleep */
#include <semaphore.h>                                           /* sem_wait */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "../snmp/snmp_switch.h"                                /* if_snmp.h */
#include "../snmp/if_snmp.h"                                       /* snmp.h */
#include "../snmp/snmp.h"                                /* SNMP_init_switch */
#include "../config/config.h"                           /* vs->cfg->switches */
#include "../vlanserver.h"                                             /* vs */
#include "server.h"

/****************************************************************************
 * VS_switch_init Inits all the switches
 ****************************************************************************
 * Calls SNMP_init_switch and wait for a sem_post to notify the end
 * No need to try a second time for badinited switches SNMP has already
 * tried
 *****************************************************************************/
ERR_CODE VS_switch_init(struct VS_switches * list_switch)
{
  struct CFG_SWITCH * z;
  unsigned int i,k;
  unsigned short int l;  
  unsigned int iter;
  struct VS_vlan* vlanLoop;

  vs->snmp->sem_end_init=malloc(sizeof(sem_t));
  sem_init(vs->snmp->sem_end_init,0,0);
  
  /* Init of ALL switches */
  for(z=vs->cfg->zwitches, i=1 ; z!=NULL ; z=z->next, i++)
  {
    SNMP_init_switch(vs->snmp, z->type, z->ip, z->nports, z->community,
                                              z->unit, list_switch->zwitch[i]);
  }
  for(z=vs->cfg->zwitches, i=1; z!=NULL ; z=z->next, i++)
  {
    SNMP_get_vlans(vs->snmp, list_switch->zwitch[i]);
  }
  for(z=vs->cfg->zwitches, i=1; z!=NULL ; z=z->next, i++)
  {
    SNMP_get_macs(vs->snmp, list_switch->zwitch[i]);
  }
  
  sem_wait(vs->snmp->sem_end_init);
  for(iter=1;iter<i;iter++)                                   /* switch loop */
  {
    for(k=1;k<list_switch->zwitch[iter]->nb_ports;k++)         /* ports loop */
    {
      for(vlanLoop = list_switch->zwitch[iter]->ports[k].vlan_struct;
            vlanLoop != NULL;
            vlanLoop = vlanLoop->next)
      {
        for(l=1;l<MAX_VLAN_NB+1;l++)                           /* vlans loop */
        {
          if(vlanLoop->vlan == list_switch->zwitch[iter]->vlanid[l])
          {
            vlanLoop->vlan = l;
            vlanLoop->vlanType = NONE;
            break;
          }
          else if(vlanLoop->vlan == list_switch->zwitch[iter]->ulVLTid[l])
          {
            vlanLoop->vlan = l;
            vlanLoop->vlanType = VLT;
            break;
          }
          else if(vlanLoop->vlan == list_switch->zwitch[iter]->ul8021Qid[l])
          {
            vlanLoop->vlan = l;
            vlanLoop->vlanType = T8021Q;
            break;
          }
          else if(vlanLoop->vlan == list_switch->zwitch[iter]->ulATMid)
          {
            vlanLoop->vlan = 0;
            vlanLoop->vlanType = ATM;
            break;
          }
        }
        if(l > MAX_VLAN_NB)
        {
          list_switch->zwitch[iter]->badinit=1;
          break;
        }
      }
    }
    if(!list_switch->zwitch[iter]->badinit)
    {
      list_switch->init[iter]=1;
      for(k=1;k<list_switch->zwitch[iter]->nb_ports;k++)
      {
        if((list_switch->zwitch[iter]->ports[k].vlan_struct == NULL) &&
           ((list_switch->zwitch[iter]->ports[k].flags & PF_UNUSEDPORT) == 0))
        {
          VS_log(LOGWARNING,SERVER,"The port %u of the switch %s is in no vlan",
                                      k,list_switch->zwitch[iter]->s2.peername);
        }
      }
    }
  }
  return VS_R_OK;
}
