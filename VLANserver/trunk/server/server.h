/*****************************************************************************
 * server.h
 * Header file for server.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: server.h,v 1.14 2001/11/12 15:46:10 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __SERVER_H__
#define __SERVER_H__

/* Define the length of the message */
#define VS_MESS_LENGTH 80

/* Define the maximum number of switches */
/* KLUDGE TODO */
#define MAX_SWITCH 50

/* Define the info_client struct */
struct VS_info_client
{
  unsigned int session_id;
  long unsigned int date_se;
  char mess[VS_MESS_LENGTH];
  char ipaddr[VS_IP_LENGTH];
  VS_VLAN_TYPE vlanType;
  int iSocket;
};


struct VS_info_poller
{
  enum
  {
    VS_POLL_RUN,                            /* the reinit_switchs is running */
    VS_POLL_HOLD,           /* the reinit_switchs does no longer init the db */
    VS_POLL_STOP,              /* the reinit_switchs engine is shutting down */
    VS_POLL_CANCEL,                        /* the reinit_switchs is canceled */
  } runlevel;
  int iSocket;
  unsigned int uTime;
};


struct VS_switches
{
  struct SNMP_switch *zwitch[MAX_SWITCH+1];
  unsigned int init[MAX_SWITCH+1];
};

ERR_CODE VS_start();

ERR_CODE VS_db_thread(struct VS_info_poller * info_poller);

ERR_CODE VS_db_init(struct VS_info_poller * info_poller);

ERR_CODE VS_connect(struct sockaddr_in server_cient);

ERR_CODE VS_select(int iSocket, unsigned int* session_id);

ERR_CODE VS_request_handler_thread(struct VS_info_client * info_client);

ERR_CODE VS_stop(struct VS_info_poller * info_poller);

ERR_CODE VS_reinit_switchs_thread(struct VS_info_poller * info_poller);

ERR_CODE VS_reinit_switchs(struct VS_info_poller * info_poller);
#endif
