/*****************************************************************************
 * connect.c
 * Functions to communicate with the vlanbridge
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: connect.c,v 1.15 2001/10/26 23:16:10 marcari Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <errno.h>                                                  /* EBUSY */
#include <sys/time.h>                                     /* struct time_val */
#include <unistd.h>                                          /* close, sleep */
#include <sys/socket.h>                                      /* bind, socket */
#include <pthread.h>                                   /* pthread_mutex_lock */
#include <stdio.h>                                                /* sprintf */

#include "../types.h"
#include "../logger.h"

#include "../config.h"                                            /* VERSION */
#include "../config/config.h"                     /* vs->cfg->vlanbridge ... */
#include "../vlanserver.h"                                             /* vs */
#include "../common.h"                                       /* for protocol */
#include "connect.h"

/****************************************************************************
 * CNX_cnx_lost
 ****************************************************************************
 * Tries to log on again on the VLANbridge when the connection has been lost
 ****************************************************************************/
static void CNX_cnx_lost()
{
  unsigned int retry_delay;
  unsigned int still_sleep;

  retry_delay=12;

  if(pthread_mutex_trylock(&(vs->cnx_lock))==EBUSY)
    return;

  if (vs->fdescr)
  {
    if (!close(vs->fdescr))
      vs->fdescr=0;
    else
      VS_log(LOGERROR, SERVER, "Unable to close socket for communication "
                               "with the VLANbridge.");
  }
  
  while (vs->runlevel<VS_STOP)
  {
    VS_log(LOGINFO, SERVER, "Reconnection to the VLB trial failed or "
           "connection lost, retrying in %u seconds.", retry_delay);
    /* this sleeps retry_delay seconds */
    for (still_sleep=retry_delay;still_sleep;still_sleep=sleep(still_sleep));

    if (!CNX_vlb_login(VERSION))
    {
      VS_log(LOGINFO, SERVER, "Connection to VLB is up again.");
      pthread_mutex_unlock(&(vs->cnx_lock));
      return;
    }

    if (vs->fdescr)
    {
      if (!close(vs->fdescr))
        vs->fdescr=0;
      else
        VS_log(LOGERROR, SERVER, "Unable to close socket for communication "
                                 "with the VLANbridge.");
    }

    retry_delay*=2;
  }
}

/****************************************************************************
 * CNX_vlb_set_vlan
 ****************************************************************************
 * Tells the VLANbridge that a machine has changed vlan
 * Returns VS_R_SEND in case of problems
 * (in which case CNX_cnx_lost will be called), 0 otherwise
 ****************************************************************************/
unsigned int CNX_vlb_set_vlan(unsigned int session_id,
                              char * MAC, char * IP, VS_VLAN VLANdest,
                              VS_VLAN VLANsrc)
{
  unsigned char message[VLAN_MSG_LEN];
  size_t j;
  int i;

  j=sprintf(message,"%u %u %s %s %u %u\n", VLAN_ROUTE_REQUEST, session_id, \
                       MAC, IP, (unsigned int)VLANdest, (unsigned int)VLANsrc);
  i=send(vs->fdescr, message, j, 0);
  if (i==j)
  {
    VS_log(LOGDEBUG, SERVER, "Request (%u) to change %s %s from VS_VLAN %u to"\
                     " %u successfully sent to VLANbridge",session_id, IP, MAC,\
                                (unsigned int)VLANsrc, (unsigned int)VLANdest);
    return 0;
  }
  else 
  {
    if (i==-1)
    {
      VS_log(LOGERROR, SERVER, "Unable to send the request (%u) to change %s "\
                 "%s from VS_VLAN %u to %u to the VLANbridge", session_id, IP,\
                           MAC, (unsigned int)VLANsrc, (unsigned int)VLANdest);
    }
    else
    {
      VS_log(LOGERROR, SERVER, "Unable to send the whole request (%u) to "\
                       "change %s %s from VS_VLAN %u to %u to the VLANbridge",\
                   session_id, IP, MAC, (unsigned)VLANsrc, (unsigned)VLANdest);
    }
    CNX_cnx_lost(vs);
    return VS_R_SEND;
  }
}

/****************************************************************************
 * CNX_vlb_login
 ****************************************************************************
 * logs on on the VLANbridge
 * returns 0 if everything's ok
 ****************************************************************************/
unsigned int CNX_vlb_login(char * version)
{
  unsigned char message[VLAN_MSG_LEN];
  unsigned int i;
  unsigned int j;
  int k;
  fd_set set;
  struct timeval tout={VS_VLB_TIMEOUT, 0};

  vs->fdescr=0;

  VS_log(LOGINFO, SERVER, "Trying to login on VLANbridge");
  for (i=0;version[i];i++)
    if (version[i]=='.')
      break;
  if (version[i]=='.')
    j=sprintf(message, "%u %s %s %s\n", VLAN_LOGIN_REQUEST, version,\
              vs->cfg->vlanbridge_login, vs->cfg->vlanbridge_password);
  else
    j=sprintf(message, "%u %s.0 %s %s\n", VLAN_LOGIN_REQUEST, version,\
              vs->cfg->vlanbridge_login, vs->cfg->vlanbridge_password);

  k=socket(AF_INET, SOCK_STREAM, 0);
  if (k==-1)
  {
    VS_log(LOGERROR, SERVER, "Unable to open a stream socket to connect to "\
           "VLB");
    return VS_R_SOCKET;
  }
  vs->fdescr=k;
  VS_log(LOGDEBUG, SERVER, "Stream socket opened. File descriptor is %u", k);

  VS_log(LOGDEBUG, SERVER, "Trying to connect to VLANbridge");
  if(connect(k, (struct sockaddr *)&vs->vlb_params,\
                                                  sizeof(struct sockaddr))==-1)
  {
    VS_log(LOGERROR, SERVER, "Unable to connect to VLANbridge");
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the VLB");
    return VS_R_CONNECT;
  }
  VS_log(LOGINFO, SERVER, "Successfully connected to VLANbridge");
  
  VS_log(LOGDEBUG, SERVER, "Sending string to VLANbridge: %s", message);
  i=send(k, message, j, 0);
  if (((int)i)==-1)
  {
    VS_log(LOGERROR, SERVER, "Unable to send anything to the VLB");
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the VLB");
    return VS_R_SEND;
  }
  if (i!=j)
  {
    VS_log(LOGERROR, SERVER, "Unable to send the whole string to the VLB");
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the VLB");
    return VS_R_SEND;
  }
  VS_log(LOGDEBUG, SERVER, " %u bytes sent",j);

  VS_log(LOGDEBUG, SERVER, "Waiting for VLANbridge answer");
  FD_ZERO(&set);
  FD_SET(k, &set);
  i=select(1+k, &set, NULL, NULL, &tout);
  if (((int)i)==-1)
  {
    VS_log(LOGERROR, SERVER, "Unable to select() VLANbridge's file "\
                                                                 "descriptor");
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the VLB");
    return VS_R_SELECT;
  }
  if (!i)
  {
    VS_log(LOGERROR, SERVER, "tout(%us): VLB answer not arrived",
           VS_VLB_TIMEOUT);
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the"\
                                                                " VLANbridge");
    return VS_R_TIMEOUT;
  }

  VS_log(LOGDEBUG, SERVER, "Answer has arrived, reading it");
  i=recv(k, message, VLAN_MSG_LEN*sizeof(char), 0);
  if (((int)i)==-1)
  {
    VS_log(LOGERROR, SERVER, "Unable to read the answer");
    if (close(k))
      VS_log(LOGERROR, SERVER, "Unable to close the socket for the"\
                                                                " VLANbridge");
    return VS_R_RECV;
  }
  message[i]=0;
  VS_log(LOGINFO, SERVER, "Answer from VLANbridge: %s", message);

  return 0;
}
