/*****************************************************************************
 * if_snmp.c
 * Interface part of the snmp
 * All functions accessible from outside should be there
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: if_snmp.c,v 1.25 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <semaphore.h>                                           /* sem_post */
#include <stdlib.h>                                                /* malloc */
#include <sys/time.h>                                      /* struct timeval */
#include <pthread.h>                                   /* phtread_mutex_lock */
#include <unistd.h>                                    /* write, read, close */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "snmp_switch.h"                               /* struct SNMP_switch */
#ifdef TRAPS
#include "callback.h"                                    /* SNMP_SW_callback */
#endif
#include "../vlanserver.h"                                             /* vs */
#include "if_snmp.h"
unsigned int current_requests;


/*****************************************************************************
 * SNMP_push_req : Local Function called to push a request on the fifo
 *****************************************************************************/
static ERR_CODE SNMP_push_req(struct SNMP_snmp *snmp,struct SNMP_req *request)
{
  request->next=NULL;
  pthread_mutex_lock(&snmp->fifo_mutex);
  if (snmp->first!=NULL)
  {
    snmp->last->next=request;
  }
  else
  {
    snmp->first=request;
  }
  snmp->last=request;
  pthread_mutex_unlock(&snmp->fifo_mutex);
  if (write(snmp->pipe[1],snmp,1)==1)
  {
    return 0;
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to write to pipe");
    return VS_R_PIPE_WRITE;
  }
}



/*****************************************************************************
 * SNMP_set_vlan : User Function called to send a set_vlan command
 *****************************************************************************
 * This function could be called by users
 * It just pushes a request to send the command
 *****************************************************************************/
ERR_CODE SNMP_set_vlan(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch,\
                       VS_PORT port,VS_VLAN vlan,VS_VLAN_TYPE vlanType)
{
  struct SNMP_req *request;

  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->change_vlan;
    request->argument.change_vlan.vlan=vlan;
    request->argument.change_vlan.vlanType=vlanType;
    request->argument.change_vlan.port=port;
    request->argument.change_vlan.zwitch=zwitch;
    request->argument.change_vlan.uRowStatus=4;
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  }
}



/*****************************************************************************
 * SNMP_unset_vlan : User Function called to send a unset_vlan command
 *****************************************************************************
 * This function could be called by users
 * It just pushes a request to send the command
 *****************************************************************************/
ERR_CODE SNMP_unset_vlan(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch,\
                       VS_PORT port,VS_VLAN vlan,VS_VLAN_TYPE vlanType)
{
  struct SNMP_req *request;

  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->change_vlan;
    request->argument.change_vlan.vlan=vlan;
    request->argument.change_vlan.vlanType=vlanType;
    request->argument.change_vlan.port=port;
    request->argument.change_vlan.zwitch=zwitch;
    request->argument.change_vlan.uRowStatus=6;
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  }
}



/*****************************************************************************
 * SNMP_get_vlanids: User Function called to begin a get_vlanids walk
 *****************************************************************************
 * This function could be called bu users
 * It just puch a request to start the walk
 *****************************************************************************/
ERR_CODE SNMP_get_vlanids(struct SNMP_snmp *snmp, struct SNMP_switch *zwitch)
{
  struct SNMP_req *request;
    
  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->fill_table;
    request->argument.fill_table.zwitch=zwitch;
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  } 
}



/*****************************************************************************
 * SNMP_get_macs : User Function called to begin a get_macs walk
 *****************************************************************************
 * This function could be called by users
 * It just pushes a request to start the walk
 *****************************************************************************/
ERR_CODE SNMP_get_macs(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch)
{
  struct SNMP_req *request;

  sem_wait(&zwitch->semaMac);
  
  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->get_macs;
    request->argument.get_macs.zwitch=zwitch;
    sem_destroy(&zwitch->semaMac);
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    sem_destroy(&zwitch->semaMac);
    return VS_R_MEMORY;
  }
}



/*****************************************************************************
 * SNMP_get_portids : User Function called to begin a get_portids walk
 *****************************************************************************
 * This function could be called by users
 * It just pushes a request to start the walk
 *****************************************************************************/
ERR_CODE SNMP_get_portids(struct SNMP_snmp *snmp, struct SNMP_switch *zwitch)
{
  struct SNMP_req *request;
    
  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->fill_ports;
    request->argument.fill_ports.zwitch=zwitch;
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  } 
}


/*****************************************************************************
 * SNMP_get_vlans : User Function called to begin a get_vlans walk
 *****************************************************************************
 * This function could be called bu users
 * It just pushes a request to start the walk
 *****************************************************************************/
ERR_CODE SNMP_get_vlans(struct SNMP_snmp *snmp,struct SNMP_switch *zwitch)
{
  struct SNMP_req *request;
  
  /* Wait the end of the portId's and vlanId's walks */
  sem_wait(&zwitch->semaVlan);
  sem_wait(&zwitch->semaVlan);
      
  request=malloc(sizeof(struct SNMP_req));
  if (request!=NULL)
  {
    request->cmd=zwitch->get_vlans;
    request->argument.get_vlans.zwitch=zwitch;
    sem_destroy(&zwitch->semaVlan);
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    sem_destroy(&zwitch->semaVlan);
    return VS_R_MEMORY;
  }
}



/*****************************************************************************
 * SNMP_get_next : User Function called to send a get_next after the callback
 *****************************************************************************
 * This function should be called ONLY from the callback !
 * WARNING: As called from the callback, we shouldn't use any snmp command
 *          that uses a snmp_session as argument.
 * Be carefull not to use pdu, it will be freed by snmplib
 *****************************************************************************/
ERR_CODE SNMP_get_next(struct SNMP_snmp *snmp,
                       struct SNMP_switch *zwitch,
                       struct snmp_pdu *pdu,
                       struct snmp_session *session)
{
  struct SNMP_req *request;

  request=malloc(sizeof(struct SNMP_req));
  if(request!=NULL)
  {
    request->cmd=zwitch->get_next;
    request->argument.get_next.zwitch=zwitch;
    request->argument.get_next.session=session;
    
    /* snmp_pdu_create doesn't refer to any session
     * so we could think it is thread safe */
   request->argument.get_next.pdu=\
      (struct snmp_pdu *)snmp_pdu_create(SNMP_MSG_GETNEXT);
   if(request->argument.get_next.pdu==NULL)
   {
     VS_log(LOGERROR,SNMP,"Unable to create the pdu");
     return VS_R_MEMORY;
   }
   if(!snmp_add_null_var(request->argument.get_next.pdu,\
                      pdu->variables->name,\
                      pdu->variables->name_length))
   {
     VS_log(LOGERROR,SNMP,"Unable to complete pdu in PORT searching");
     return VS_R_SNMP;
   }
   return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  }
}


/*****************************************************************************
 * SNMP_end_session : User Function called to end a session after the callback
 *****************************************************************************
 * This function should be called ONLY from the callback !
 * WARNING: As called from the callback, we shouldn't use any snmp command
 *          that uses a snmp_session as argument.
 *****************************************************************************/
ERR_CODE SNMP_end_session(struct SNMP_snmp *snmp,
                          struct SNMP_switch *zwitch,
                          struct snmp_session *session)
{
  struct SNMP_req *request;

  request=malloc(sizeof(struct SNMP_req));
  if(request!=NULL)
  {
    request->cmd=zwitch->end_session;
    request->argument.end_session.zwitch=zwitch;
    request->argument.end_session.session=session;
   
    return SNMP_push_req(snmp,request);
  }
  else
  {
    VS_log(LOGERROR,SNMP,"Unable to allocate memory to push a request");
    return VS_R_MEMORY;
  }
}



/*****************************************************************************
 * SNMP_loop : Main loop for snmp
 *****************************************************************************
 * This function is the basic function of the snmp thread
 *****************************************************************************/

static void *SNMP_loop()
{
  struct timeval timeout;
  fd_set meuuh;
  unsigned int numfds;
  unsigned int block;
  int count;
  struct SNMP_req *z;
  char foobar;


#ifdef TRAPS
  struct snmp_session session, *ss;
  /* Need to create an snmp session to catch traps */
  session.peername=NULL;
  session.community=NULL;
  session.community_len=0;
  session.retries=12;
  session.timeout=10000000;
  session.authenticator=NULL;
  session.callback=\
     (int (*)(int,struct snmp_session *,int,struct snmp_pdu *,void *))\
     SNMP_SW_callback;
  session.callback_magic=NULL;
/*session.local_port=SNMP_TRAP_PORT;*/
  session.local_port=SNMP_TRAP_PORT+3000;
  ss=snmp_open(&session);
  if (ss==NULL)
  {
    VS_log(LOGERROR,SNMP,"Unable to open the session to catch traps");
  }
  else
  {
    VS_log(LOGINFO,SNMP,"The session to catch traps is opened");
  }
#endif /* TRAPS */

  
  vs->snmp->session_num=1;
  
  /* Ready to start */
  VS_log(LOGINFO,SNMP,"Snmp loop started");
  vs->snmp->runlevel=SNMP_RUN;
  sem_post(vs->snmp->sem);
  
  /* Beginning of the main loop */                      
  while (vs->snmp->runlevel<SNMP_STOP)
  {
    timerclear(&timeout);
    timeout.tv_sec=8;
    block=1;
    numfds=vs->snmp->numfds;
    FD_ZERO(&meuuh);
    FD_SET(vs->snmp->pipe[0],&meuuh);
    snmp_select_info(&numfds,&meuuh,&timeout,&block);
    count=select(numfds,&meuuh,NULL,NULL,&timeout);
    if(count==0)
    {
      snmp_timeout();
    }
    else if (count>0)
    {
      if (FD_ISSET(vs->snmp->pipe[0],&meuuh))
      {
        if (read(vs->snmp->pipe[0],&foobar,1)==1)
        {
          pthread_mutex_lock(&vs->snmp->fifo_mutex);
          z=vs->snmp->first;
          vs->snmp->first=z->next;
          pthread_mutex_unlock(&vs->snmp->fifo_mutex);
          z->ret_value=z->cmd(&z->argument,vs->snmp);
          free(z);
        }
        else
        {
          VS_log(LOGERROR,SNMP,"Unable to read from pipe");
        }
      }
      else
      {
        snmp_read(&meuuh);
      }
    }
    else
    {
      VS_log(LOGERROR,SNMP,"Error in select statement");
    }
  }
  VS_log(LOGINFO, SNMP, "SNMP engine stopped");
  sem_destroy(vs->snmp->sem);
  return NULL;
}

/*****************************************************************************
 * SNMP_cancel : Cancels the init of the snmp_thread in case of error
 *****************************************************************************/
static void SNMP_cancel(struct SNMP_snmp * snmp)
{
  close(snmp->pipe[0]);
  close(snmp->pipe[1]);
  pthread_mutex_destroy(&snmp->fifo_mutex);
  VS_log(LOGERROR,SNMP,"Unable to spawn the snmp engine");
}



/*****************************************************************************
 * SNMP_start : Starts the main loop for snmp
 *****************************************************************************
 * This function is called to init the snmp thread
 *****************************************************************************/
ERR_CODE SNMP_start(struct SNMP_snmp * snmp)
{
  VS_log(LOGINFO,SNMP,"Starting the SNMP engine...");
  snmp->runlevel=SNMP_INIT;
  init_snmp("snmp_app");
  snmp->first=NULL;
  if (pipe(snmp->pipe))
  {
    VS_log(LOGERROR,SNMP,"Could not open pipe for snmp locking");
    return VS_R_PIPE;
  }
  snmp->numfds=snmp->pipe[0]+1;
  pthread_mutex_init(&snmp->fifo_mutex,NULL);
  if (pthread_create(&snmp->loop,NULL,(void *(*)())SNMP_loop,NULL))
  {
    SNMP_cancel(snmp);
    return VS_R_PTHREAD;
  }
  return 0;
}

/*****************************************************************************
 * SNMP_stop : Stops the main loop for snmp
 *****************************************************************************
 * This function is called to stop the snmp thread
 *****************************************************************************/
ERR_CODE SNMP_stop(struct SNMP_snmp * snmp)
{
  close(snmp->pipe[0]);
  close(snmp->pipe[1]);
  pthread_mutex_destroy(&snmp->fifo_mutex);
  VS_log(LOGINFO, SNMP, "Stopping the snmp engine");
  snmp->runlevel=SNMP_STOP;
  return 0; 
}
