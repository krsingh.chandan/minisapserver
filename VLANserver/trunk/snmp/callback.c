/*****************************************************************************
 * callback.c
 * Provide all callbacks called by the snmplib when snmp_read and
 * snmp_timeout
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: callback.c,v 1.23 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Damien Lucas <nitrox@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>                                            /* types.h */
#include <stdlib.h>                                                /* malloc */
#include <pthread.h>
#include <semaphore.h>                                           /* sem_post */
#include <netdb.h>                                              /* if_snmp.h */
#include <ucd-snmp/asn1.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp.h>                                      /* if_snmp.h */
#include <ucd-snmp/snmp_impl.h>                                 /* if_snmp.h */
#include <ucd-snmp/default_store.h>                             /* if_snmp.h */
#include <ucd-snmp/snmp_api.h>                                  /* if_snmp.h */
#include <ucd-snmp/snmp_client.h>                               /* if_snmp.h */
#include <ucd-snmp/mib.h>                                       /* if_snmp.h */

#include "../types.h"
#include "../logger.h"

#include "snmp_switch.h"                               /* struct SNMP_swicth */
#include "if_snmp.h"                                   /* SNMP_get_vlan, ... */
#include "../db/db.h"		                       /* DB_machines_delete */
#include "snmp.h"                                         /* current_request */
#include "../vlanserver.h"                                             /* vs */
#include "callback.h"

/* Local static function */
static void end_init (void);

/*****************************************************************************
 *  Callback_Vlans()
 *****************************************************************************
 *  Treat the pdu in order to get the VLAN of a port
 *  And put it in the struct zwitch
 *  And send the GETNEXT packet if the walk is not at the end
 *****************************************************************************/
inline static unsigned int Callback_Vlans(struct snmp_session *session,
                                          struct SNMP_switch *zwitch,
                                          struct variable_list *pdu_var)
{
  char buffer[SPRINT_MAX_LEN];
  unsigned long long int vlan;
  unsigned int port;
  struct VS_vlan* vlanLoop;
  struct VS_vlan* vlanLoopPrev;
   
  session->sessid++;
  zwitch->walk_vlans.session_num++;
  sprint_variable(buffer,pdu_var->name,pdu_var->name_length,pdu_var);

  /* Test if the walk is at the end */
  if(!memcmp(buffer, zwitch->walk_vlans.stop,zwitch->walk_vlans.stop_size))
  {
    /* Need to operate on received pdu
     * ifMIB.ifMIBObjects.ifStackTable.ifStackEntry.ifStackStatus.P.V
     *                                                           ^59  */
            
    switch(zwitch->type)
    {
      case VS_ST_3C1000:
      case VS_ST_3CDESK:
      case VS_ST_3C1100:
      case VS_ST_3C3300:
        if(sscanf(buffer+59,"%llu.%u = active(1)",&vlan,&port)==2)
        {
          if (port<Snmp_PORT(zwitch->nb_ports,zwitch->unit)+1 \
                                                      && port>100*zwitch->unit) 
          {
            vlanLoop=zwitch->ports[PORT_Snmp(port,zwitch->unit)].vlan_struct;
            
            if(vlanLoop==NULL)
            {
              vlanLoop=malloc(sizeof(struct VS_vlan));
              if(vlanLoop==NULL)
              {
                VS_log(LOGERROR,SNMP,"Unable to allocate memory");
                return 3; // Mmmmm pb : que dois je retouner ??
              }
              vlanLoop->next=NULL;
              vlanLoop->prev=NULL;
              vlanLoop->vlan=vlan;
              zwitch->ports[PORT_Snmp(port,zwitch->unit)].vlan_struct=vlanLoop;
            }
            else
            {
              do
              {
                vlanLoopPrev=vlanLoop;
                if(vlanLoop->vlan == vlan)
                {
                  /* Need to send the next request */
                  return 1;
                }
                vlanLoop=vlanLoop->next;
              }while(vlanLoop != NULL);

              vlanLoop=malloc(sizeof(struct VS_vlan));
              if(vlanLoop==NULL)
              {
                VS_log(LOGERROR,SNMP,"Unable to allocate memory");
                return 3; // Mmmmm pb : que dois je retouner ??
              }
              vlanLoop->next=NULL;
              vlanLoop->prev=vlanLoopPrev;
              vlanLoop->vlan=vlan;
            }
          }
        } 
        break;
    }
            
    /* Need to send the next request */
    return 1;
  }
  else
  {
    /* We reached end of the walk 
     * Let's see if the init was correctly done */
    for(port=1;port<zwitch->nb_ports+1;port++)
    {
      if((zwitch->ports[port].vlan_struct == NULL) &&
         ((zwitch->ports[port].flags & PF_UNUSEDPORT) == 0))
      {
        /* We are just making 2 loops because it could be normal that
         * a port is in no vlan */
        if(zwitch->ports[port].sLoopNumber<2)
        {
          VS_log(LOGERROR,SNMP,"Switch %s init: Walk on VLANs failed. Number "\
                               "of try : %i",zwitch->s2.peername,\
                                             zwitch->ports[port].sLoopNumber+1);
          zwitch->ports[port].sLoopNumber++;
          SNMP_get_vlans(vs->snmp,zwitch);
          return 3;
        }
        else
        {
          VS_log(LOGWARNING,SNMP,"Switch %s init: Walk on VLANs not completed",\
                                                           zwitch->s2.peername);
        }
      }
      for(vlanLoop=zwitch->ports[port].vlan_struct;vlanLoop!=NULL;
                                                        vlanLoop=vlanLoop->next)
      {
        if(vlanLoop->vlan==0)
        {
          VS_log(LOGERROR,SNMP,"Switch %s init: Walk on VLANs failed",\
                                                           zwitch->s2.peername);
          SNMP_get_vlans(vs->snmp,zwitch);
          return 3;
        }
      }
    }
    /* When this part is reached init is correctly done */
    current_requests--;
    VS_log(LOGDEBUG,SNMP,"Walk on vlans succeeded. (CR: %u)",current_requests);
    zwitch->walk_vlans.session_num=0;
    
    sem_post(&zwitch->semaMac);
    
    return 2;
  }
} 


/*****************************************************************************
 *  Callback_Vlanids()
 ******************************************************************************
 *  Treat the pdu in order to get the VLANid of a VLAN
 *  And put it in the struct zwitch
 *  And send the GETNEXT packet if the walk is not at the end
 ******************************************************************************/

inline static unsigned int  Callback_Vlanid(struct snmp_session *session,
                                            struct SNMP_switch *zwitch,
                                            struct variable_list *pdu_var)

{
  char buffer[SPRINT_MAX_LEN];
  char sarass[64];
  unsigned long long int vlan;
  unsigned int i;
  
  session->sessid++;
  zwitch->walk_table.session_num++;
  sprint_variable(buffer,pdu_var->name,pdu_var->name_length,pdu_var);

  /* Test if the walk is at the end */
  if(!memcmp(buffer,zwitch->walk_table.stop,zwitch->walk_table.stop_size))
  {
    /* Need to operate on received pdu
     * interfaces.ifTable.ifEntry.ifDescr.xxyy = RMON VLAN 1
     *                                   ^35                 */
    
    switch(zwitch->type)
    {
      case VS_ST_3C1000:
      case VS_ST_3CDESK:
        if(sscanf(buffer+35,"%Lu = RMON:VLAN %u:%s",&vlan,&i,sarass)>=2)
        {
          zwitch->vlanid[i]=vlan;
        }
        if(sscanf(buffer+35,"%Lu = ATM Port %u, %s",&vlan,&i,sarass)==3)
        {
          if(!strcmp(sarass,"AAL5"))
          {
            zwitch->ulATMid=vlan;
          }
        }
        break;

      case VS_ST_3C1100:
      case VS_ST_3C3300:
        if(sscanf(buffer+35,"%Lu = RMON VLAN %u",&vlan,&i)==2)
        {
          zwitch->vlanid[i]=vlan;
        }
        if(sscanf(buffer+35,"%Lu = Local Workgroup Encapsulation Tag %u",&vlan,
                  &i)==2)
        {
          zwitch->ulVLTid[i]=vlan;
        }
        if(sscanf(buffer+35,"%Lu = 802.1Q Encapsulation Tag %u",&vlan,&i)==2) 
        {
          zwitch->ul8021Qid[i]=vlan;
        }
        break;
    }
            
    /* Need to send the next request */
    return 1;
  }
  else
  {
    /* We reached end of the walk 
     * Let's see if the init was correctly done */
    for(i=1;i<MAX_VLAN_NB+1;i++)
    {
      if(zwitch->vlanid[i]==0)
      {
        /* Consider here init failed */
        VS_log(LOGERROR,SNMP, "Switch %s init: Walk on VLANids failed",
                                                           zwitch->s2.peername);
        SNMP_get_vlanids(vs->snmp,zwitch);
        return 3;
      }
      switch(zwitch->type)
      {
        case VS_ST_3C1100:
        case VS_ST_3C3300:
          if(zwitch->ulVLTid[i]==0)
          {
            /* Consider here init failed */
            VS_log(LOGERROR,SNMP, "Switch %s init: Walk on VLTids failed",
                                                           zwitch->s2.peername);
            SNMP_get_vlanids(vs->snmp,zwitch);
            return 3;
          }
          if(zwitch->ul8021Qid[i]==0)
          {
            /* Consider here init failed */
            VS_log(LOGERROR,SNMP,
                   "Switch %s init: Walk on 802.1Qids failed",
                                                           zwitch->s2.peername);
            SNMP_get_vlanids(vs->snmp,zwitch);
            return 3;
          }
        default:
          break;
      }
    }
    /* When this part is reached init is correctly done */
    current_requests--;
    VS_log(LOGDEBUG,SNMP,"Walk on Vlan/VLT/802.1Q/LECids succeeded.(CR: %u)",\
                                                             current_requests);
    zwitch->walk_table.session_num=0;

    sem_post(&zwitch->semaVlan);
    
    return 2;
  }
}

/*****************************************************************************
 *  Callback_Macs()
 *****************************************************************************
 *  Treat the pdu in order to get one the MAC of a port
 *  And put it in the struct zwitch
 *  And send the GETNEXT packet if the walk is not at the end
 *****************************************************************************/
inline static unsigned int Callback_Macs(struct snmp_session *session,
                                         struct SNMP_switch *zwitch,
                                         struct variable_list *pdu_var)
{
  char buffer[SPRINT_MAX_LEN];
  VS_MachineId mac,i2;
  unsigned int i3,i4,i5,i6;
  unsigned int port;
  unsigned int pur_port;
  
  session->sessid++;
  zwitch->walk_macs.session_num++;
        
  sprint_variable(buffer,pdu_var->name,pdu_var->name_length,pdu_var);

  /* Test if the walk is at the end */
  if(!memcmp(buffer, zwitch->walk_macs.stop,zwitch->walk_macs.stop_size))
  {
    /* Need to operate on received pdu
     * enterprises.43.10.22.2.1.3.unit.port.i1.i2.i3...
     *                                ^ 29             */
                
    if(sscanf(buffer+29,"%u.%Lu.%Lu.%u.%u.%u.%u",&port,&mac,\
                                                       &i2,&i3,&i4,&i5,&i6)!=7)
    {
      VS_log(LOGERROR,SNMP,"Bad snmp GETRESPONSE");
      *zwitch->walk_macs.machine=NULL;
      for(i3=zwitch->walk_macs.last_port+1;i3<=zwitch->nb_ports;i3++)
      {
        zwitch->machines[i3]=NULL;
      }
      zwitch->walk_macs.session_num=0;
      return 3;
    }
   
    /* Check for the port number */
    if(port>zwitch->nb_ports)
    {
      VS_log(LOGERROR,SNMP, "Found MAC on an out of range port, "
                            "skipping, current port %u / %u %s",
                            port,zwitch->nb_ports,zwitch->s2.peername);
      return 1;
    }

    mac<<=8; mac|=i2;
    mac<<=8; mac|=i3;
    mac<<=8; mac|=i4;
    mac<<=8; mac|=i5;
    mac<<=8; mac|=i6;

    /* Check for the mac */
    if(mac>0xffffffffffff)
    {
      VS_log(LOGERROR,SNMP,"Found an illegal mac, skipping");
      return 1;
    }

    /* If port has changed, */
    if(port!=zwitch->walk_macs.last_port)
    {
      (*zwitch->walk_macs.machine)=NULL;
      for(pur_port=zwitch->walk_macs.last_port+1;pur_port<port;pur_port++)
      {
        zwitch->machines[pur_port]=NULL;
      }

      zwitch->walk_macs.machine=zwitch->machines+port;
      zwitch->walk_macs.last_port=port;
    }

    
    *zwitch->walk_macs.machine=malloc(sizeof(struct SNMP_machines_elt));
    
    if(*zwitch->walk_macs.machine==NULL)
    {
      VS_log(LOGERROR,SNMP,"Unable to allocate memory");
      for(pur_port=zwitch->walk_macs.last_port+1;
          pur_port<=zwitch->nb_ports;
          pur_port++)
      {
        zwitch->machines[pur_port]=NULL;
      }
      zwitch->walk_macs.session_num=0;
      return 3;
    }
    
    /* adds the machine to the linked list */
    (*zwitch->walk_macs.machine)->machine=mac;
    zwitch->walk_macs.machine=&(*zwitch->walk_macs.machine)->next;

    /* Need to send the next request */
    return 1;
  }
  else
  {
    /* complete the MAC table */
    *zwitch->walk_macs.machine=NULL;
    for(i3=zwitch->walk_macs.last_port+1;i3<=zwitch->nb_ports;i3++)
    {
      zwitch->machines[i3]=NULL;
    }

    /* We reached end of the walk
     * BUT we can't check if all the macs have correctly been learned
     * so we'll just check for their size */
    
    /* When this part is reached init is correctly done */
    zwitch->walk_macs.session_num=0;
    current_requests--;
    VS_log(LOGDEBUG,SNMP,"Walk on macs succeeded. (CR: %u)",current_requests);
    if(current_requests==0)
    {
      /* We reach end of initialization */
      end_init();
    }
    
    return 2;
  }
}


/*****************************************************************************
 *  Callback_Portid()
 *****************************************************************************
 *  Treat the pdu in order to get the PORTid of a PORT (on Unit X)
 *  And put it in the struct zwitch
 *  And send the GETNEXT packet if the walk is not at the end
 *****************************************************************************/

inline static unsigned int Callback_Portid(struct snmp_session *session,
                                   struct SNMP_switch *zwitch,
                                   struct variable_list *pdu_var)

{
  char buffer[SPRINT_MAX_LEN];
  unsigned long long int port;
  unsigned int i,unit;
  char pouet[64];
  
  zwitch->walk_ports.session_num++;
  session->sessid++;
  sprint_variable(buffer,pdu_var->name,pdu_var->name_length,pdu_var);

  /* Test if the walk is at the end */
  if(!memcmp(buffer,zwitch->walk_ports.stop,zwitch->walk_ports.stop_size))
  {
    /* Need to operate on received pdu
     * interfaces.ifTable.ifEntry.ifDescr.xxyy = RMON VLAN 1
     *                                   ^35                 */

    if (sscanf(buffer+35,"%Lu = RMON:V3 Port %u",&port,&i) == 2)
    {
      zwitch->portid[i][1]=port;	   
    }
    
    else if (sscanf(buffer+35,"%Lu = RMON:V2 Port %u on Unit %u",\
	                                                  &port,&i,&unit) == 3)
    {
      zwitch->portid[i][unit]=port;
    }

    else if (sscanf(buffer+35,"%Lu = RMON:V2 Port %u",&port,&i) == 2)
    {
      zwitch->portid[i][1]=port;
    }
    
    else if (sscanf(buffer+35,"%Lu = RMON:10/100 Port %u on Unit %u",\
		                                          &port,&i,&unit) == 3)
    {
      zwitch->portid[i][unit]=port;
    }	    
	    
    else if (sscanf(buffer+35,"%Lu = ATM Port %u, %s",\
		                                          &port,&i,pouet) == 3)
    {
      if(!strcmp(pouet,"ATM"))
      {
        zwitch->portid[i][1]=port;
      }
    }	    
	    /* Need to send the next request */
    return 1;
  }
  else
  {
    /* We reached end of the walk 
     * Let's see if the init was correctly done 
     * 
     * (We only test the nb_ports ports of the unit n
     * we are not sure that the others ports do exist)    */ 
	  
    for(i=1;i<=zwitch->nb_ports;i++)
    {
      if((zwitch->portid[i][zwitch->unit]==0) &&
         !(zwitch->ports[i].flags&PF_ATMPORT) &&
         !(zwitch->ports[i].flags&PF_UNUSEDPORT))
      {
        /* Consider here init failed */
        VS_log(LOGERROR,SNMP,"Switch %s init: Walk on PORTids failed",
                                                           zwitch->s2.peername);
        SNMP_get_portids(vs->snmp,zwitch);
        return 3;
      }
    }
    /* When this part is reached init is correctly done */
    current_requests--;
    VS_log(LOGDEBUG,SNMP,"Walk on Portids succeeded.(CR: %u)",\
                                                             current_requests);
    zwitch->walk_ports.session_num=0;

    sem_post(&zwitch->semaVlan);
    
    return 2;
  }
}


/*****************************************************************************
 *  Callback_Trap_Linkdown
 *****************************************************************************
 *  Treats a "link-down" trap 
 *
 *****************************************************************************/

inline static unsigned int Callback_Trap_LinkDown(struct snmp_session *session,
                                   struct SNMP_switch *zwitch,
                                   struct snmp_pdu *pdu)

{
  struct SNMP_machines_elt * mac;
  struct variable_list * pdu_var;
  unsigned int snmpport,foo;
  VS_PORT vsport;
  char buffer[SPRINT_MAX_LEN];
  //struct SNMP_switch * zzwitch;
  long lip;		//for conversions
  char sip[VS_IP_LENGTH];		  //for conversions
  VS_SwitchId ip;
  int rc;
  struct DB_port * port;
  pdu_var=pdu->variables;

  
  /* Need to operate on received pdu
   * interfaces.ifTable.ifEntry.ifIndex.X =X*/

  sprint_variable(buffer,pdu_var->name,pdu_var->name_length,pdu_var);
 
  if(sscanf(buffer,"interfaces.ifTable.ifEntry.ifIndex.%u = %u", &snmpport, &foo) !=2)
  {
    VS_log(LOGERROR,SNMP,"Bad snmp GETRESPONSE in Callback_Trap_Linkdown");
    return 3;
  }
  
  vsport=PORT_Snmp(snmpport,1);
  
  // Let's find the ip address of the trap switch.
  lip=pdu->agent_addr.sa_align;
  sprintf(sip,"%lu.%lu.%lu.%lu",lip%256,(lip>>8)%256, \
		  (lip>>16)%256,(lip>>24)%256);
  VS_A2SID(sip,ip); //inet_aton
 
  
  /*let's build a valid zwitch variable...we grab the correct zwitch session 
  with "DB_switch_port_lock" */
  
  rc=DB_switchs_port_lock(vs->db, ip,vsport, &port, &zwitch);

  switch(rc)
  {
    case VS_R_ABSENT:
      VS_log(LOGINFO,SNMP,"LD-Trap on unkonw switch : %s",sip);
      return 3;
      break;
      
    case VS_R_MEMORY:
      VS_log(LOGINFO,SNMP,"LD-Trap Memory Error on switch : %s",sip);
      return 3;
      break;
  }	  

  // We can unlock  
  rc=DB_switchs_port_unlock(vs->db,port);

  // Now it is time for deletion
  
  // in db 
  for (mac=zwitch->machines[vsport];mac!=NULL;mac=mac->next)
    {
      VS_log(LOGINFO,SNMP,"Mac %Lx disconnected : %s port %u", \
		      mac->machine, sip, vsport);	    
      rc=DB_machines_delete(vs->db,mac->machine);
       
      if ((rc==VS_R_MEMORY) || (rc==VS_R_ABSENT))
      {
        VS_log(LOGERROR,DB,"Unable to delete machine %x from DB",mac);
        return 3; 
      }
    
    }
  
    // zwitch
  
  zwitch->machines[vsport]=NULL;
  
  return 3;
}


/*****************************************************************************
 * SNMP_SW_callback: Called after any reply of a switch.
 *****************************************************************************
 * This function is called each time a snmp_read occured
 * In fact, it is called for each answer and each walk step
 * It calls SNMP_get_next cause we can't send anything
 *  (we are inside snmp_read !)
 * Return value of the sub functions are:
 *   3 means nothing to be done
 *   2 means session is to be ended
 *   1 means everything ok, send next packet
 *   0 means errored packet, need to be reasked
 *****************************************************************************/

int SNMP_SW_callback(int op,struct snmp_session *session,int reqid,\
                     struct snmp_pdu *pdu, struct SNMP_switch *zwitch)
{
  unsigned int rc=3;
  
  if(op==RECEIVED_MESSAGE)
  {
    switch(pdu->command)
    {
      case SNMP_MSG_RESPONSE:
        /* Response to a set or the next response to a walk */ 
        if(session->sessid == zwitch->walk_macs.session_num)
        {
          /* Pdu received is part of the walk on Macs */
          rc=Callback_Macs(session,zwitch,pdu->variables);
        }
        else if(session->sessid == zwitch->walk_table.session_num)
        {
          /* Pdu received is part of the walk on vlanids */
          rc=Callback_Vlanid(session,zwitch,pdu->variables);
        }

        else if(session->sessid == zwitch->walk_ports.session_num)
        {
          /* Pdu received is part of the walk on portids */
          rc=Callback_Portid(session,zwitch,pdu->variables);
	      }
	
	      else if(session->sessid == zwitch->walk_vlans.session_num)
        {
          /* Pdu received is part of the walk on vlans */
          rc=Callback_Vlans(session,zwitch,pdu->variables);
        }
        session->s_snmp_errno=SNMPERR_SUCCESS;
        break;

      case SNMP_MSG_TRAP:
        /* Packet was a snmp-trap */
	/* Be carefull not to free it */
        switch(pdu->trap_type)
	      {
	        case SNMP_TRAP_LINKDOWN:
	          rc=Callback_Trap_LinkDown(session,zwitch,pdu);
	          break;
            
	        //case SNMP_TRAP_LINKUP:
	        //  rc=Callback_Trap_LinkUp(session,zwitch,pdu);
	        //  break;  
	      }
        session->s_snmp_errno=SNMPERR_SUCCESS;
        break;
    }

  }
  else if(op==TIMED_OUT)
  {
    if(!zwitch->badinit)
    {
      /* pdu have been resent n times (default is 5 in surrent lib */
      /* we have to consider the switch is unusable */
      VS_log(LOGERROR,SNMP,"Timeout about switch %s!",zwitch->s2.peername);
      zwitch->badinit=1;
      sem_post(&zwitch->semaVlan);
      sem_post(&zwitch->semaVlan);
      sem_post(&zwitch->semaMac);
    }
    else
    {
      VS_log(LOGDEBUG,SNMP,"Timeout about switch %s!",zwitch->s2.peername);
    }
    current_requests--;
    if(current_requests==0)
    {
      /* We reach end of initialization */
      end_init();
    }
      
  }

  switch(rc)
  {
    case 0:
      return 0;
      break;
      
    case 1:
      /* Need to send next packet */
      SNMP_get_next(vs->snmp,zwitch,pdu,session);
      return 1;
      break;
      
    case 2:
      SNMP_end_session(vs->snmp,zwitch,session);
      return 1;
      break;

    case 3:
      return 1;
      break;
     
    default:
      return 1;
      break;
  }
}


/*****************************************************************************
 * end_init: Called when current_request reach 0 means end of init
 *****************************************************************************/
static void end_init(void)
{
  sem_post(vs->snmp->sem_end_init);
}

