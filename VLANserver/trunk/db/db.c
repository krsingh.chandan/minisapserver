/*****************************************************************************
 * db.c
 * database file
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: db.c,v 1.25 2001/11/20 13:03:57 gunther Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdlib.h>                                          /* free, malloc */
#include <pthread.h>                                   /* pthread_mutex_lock */
#include <semaphore.h>                                           /* sem_wait */
#include <arpa/inet.h>                                               /* ip.h */
#include <ucd-snmp/asn1.h>                                  /* snmp_switch.h */
#include <ucd-snmp/snmp_impl.h>                             /* snmp_switch.h */
#include <ucd-snmp/snmp_api.h>                              /* snmp_switch.h */

#include "../types.h"
#include "../logger.h"

#include "../snmp/snmp_switch.h"                       /* struct SNMP_switch */
#include "db.h"

/*****************************************************************************
 * DB_M_push_req
 *****************************************************************************
 * inserts a request in a machine's fifo. request->next won't be set to NULL.
 * wakes the thread up if it can process. this function is called by the
 * database engine
 *****************************************************************************/
static inline void DB_M_push_req(struct DB_machines_elt * machine,\
                                 struct DB_M_request * request)
{
  pthread_mutex_lock(&machine->requests_lock);
  if (machine->first==NULL)
  {
    machine->first=request;
    machine->last=request;
    pthread_mutex_unlock(&machine->requests_lock);
    sem_post(&request->semaphore);
  }
  else
  {
    machine->last->next=request;
    machine->last=request;
    pthread_mutex_unlock(&machine->requests_lock);
  }
}

/*****************************************************************************
 * DB_S_push_req
 *****************************************************************************
 * inserts a request in a port's fifo. request->next won't be set to NULL.
 * wakes the thread up if it can process. this function is called by the
 * database engine
 *****************************************************************************/
static inline void DB_S_push_req(struct DB_port * port,\
                                 struct DB_S_request * request)
{
  pthread_mutex_lock(&port->requests_lock);
  if (port->first==NULL)
  {
    port->first=request;
    port->last=request;
    pthread_mutex_unlock(&port->requests_lock);
    sem_post(&request->semaphore);
  }
  else
  {
    port->last->next=request;
    port->last=request;
    pthread_mutex_unlock(&port->requests_lock);
  }
}

/*****************************************************************************
 * DB_M_pop_req
 *****************************************************************************
 * removes a request from a machine's fifo and executes the following one if
 * there is a one. Note there must be something in the fifo.
 * This function is called by clients of the database engine
 *****************************************************************************/
static inline void DB_M_pop_req(struct DB_machines_elt * machine)
{
  struct DB_M_request * z;
  struct DB_M_request * y;
  
  pthread_mutex_lock(&machine->requests_lock);
  z=machine->first;
  y=z->next;
  machine->first=y;
  pthread_mutex_unlock(&machine->requests_lock);
  if (y!=NULL)
    sem_post(&y->semaphore);
  free(z);
}

/*****************************************************************************
 * DB_S_pop_req
 *****************************************************************************
 * removes a request from a port's fifo and executes the following one if there
 * is a one. Note there must be something in the fifo.
 * This function is called by clients of the database engine
 *****************************************************************************/
static inline void DB_S_pop_req(struct DB_port * port)
{
  struct DB_S_request * z;
  struct DB_S_request * y;
  
  pthread_mutex_lock(&port->requests_lock);
  z=port->first;
  y=z->next;
  port->first=y;
  pthread_mutex_unlock(&port->requests_lock);
  if (y!=NULL)
    sem_post(&y->semaphore);
  free(z);
}

/*****************************************************************************
 * DB_M_insert
 *****************************************************************************
 * inserts a machine in the machines table. this function is called by the
 * database engine when executing a request
 * this function never fails, so req->ret_value is set to 0
 *****************************************************************************/
static void DB_M_insert(struct DB_db * db, struct DB_M_request * req)
{
  unsigned int i;

  i=DB_machines_hash(req->argument.glop->mac);
  req->argument.glop->next=db->machines[i];
  db->machines[i]=req->argument.glop;
  req->ret_value=0;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_S_insert
 *****************************************************************************
 * inserts a switch in the switchs table. this function is called by the
 * database engine when executing a request
 * this function never fails, so req->ret_value is set to 0
 *****************************************************************************/
static void DB_S_insert(struct DB_db * db,struct DB_S_request * req)
{
  unsigned int i;

  i=DB_switchs_hash(req->argument.glop->ip);
  req->argument.glop->next=db->switchs[i];
  db->switchs[i]=req->argument.glop;
  req->ret_value=0;
  req->wait_count=0;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_M_lock
 *****************************************************************************
 * takes the lock on a machine in the table. this function is called by the
 * database engine when executing a request
 * req->ret_value will be set to VS_R_ABSENT if the machine does not exist,
 * and to 0 otherwise
 *****************************************************************************/
static void DB_M_lock(struct DB_db * db, struct DB_M_request * req)
{
  struct DB_machines_elt * z;

  for (z=db->machines[DB_machines_hash(req->argument.reuh->meuuh)];z!=NULL;
       z=z->next)
    if (VS_MID_NE(z->mac, req->argument.reuh->meuuh))
      continue;
    else
    {
      *(req->argument.reuh->coin)=z;
      req->ret_value=0;
      DB_M_push_req(z,req);
      return;
    }
  req->ret_value=VS_R_ABSENT;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_S_port_lock
 *****************************************************************************
 * takes the lock on a port in the table. this function is called by the
 * database engine when executing a request
 * req->ret_value will be set to VS_R_ABSENT if the switch does not exist, and
 * to 0 otherwise
 *****************************************************************************/
static void DB_S_port_lock(struct DB_db * db,struct DB_S_request * req)
{
  struct DB_switchs_elt * z;

  req->wait_count=0;
  for (z=db->switchs[DB_switchs_hash(req->argument.rah->meuuh)];
       z!=NULL;z=z->next)
    if (VS_SID_NE(z->ip, req->argument.rah->meuuh))
      continue;
    else
    {
      *(req->argument.rah->coin)=&(z->ports[req->argument.rah->zarma]);
      *(req->argument.rah->zwitch)=z->zwitch;
      req->ret_value=0;
      DB_S_push_req(&(z->ports[req->argument.rah->zarma]), req);
      return;
    }
  req->ret_value=VS_R_ABSENT;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_S_lock
 *****************************************************************************
 * takes the lock on all the ports of a switch in the table. this function is
 * called by the database engine when executing a request
 * req->ret_value will be set to VS_R_ABSENT if the switch does not exist, and
 * to 0 otherwise
 *****************************************************************************/
static void DB_S_lock(struct DB_db * db, struct DB_S_request * req)
{
  unsigned int i;
  struct DB_switchs_elt * z;

  for (z=db->switchs[DB_switchs_hash(req->argument.switch_fetcher->switch_id)];
       z!=NULL;z=z->next)
    if (VS_SID_NE(z->ip, req->argument.switch_fetcher->switch_id))
      continue;
    else
    {
      req->wait_count=z->nports-1;
      *(req->argument.rah->coin)=&(z->ports[req->argument.rah->zarma]);
      *(req->argument.rah->zwitch)=z->zwitch;
      req->ret_value=0;
      for (i=1;i<=z->nports;i++)
        DB_S_push_req(&(z->ports[i]), req);
      return;
    }
  req->wait_count=0;
  req->ret_value=VS_R_ABSENT;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_M_unlock
 *****************************************************************************
 * releases the lock on a machine in the table. this function is called by the
 * database engine when executing a request
 * as this function always succeeds, req->ret_value is set to 0
 *****************************************************************************/
static void DB_M_unlock(struct DB_db * db,struct DB_M_request * req)
{
  DB_M_pop_req(req->argument.glop);
  req->ret_value=0;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_S_port_unlock
 *****************************************************************************
 * releases the lock on a switch's port in the table. this function is called
 * by the database engine when executing a request
 * as this function always succeeds, req->ret_value is set to 0
 *****************************************************************************/
static void DB_S_port_unlock(struct DB_db * db,struct DB_S_request * req)
{
  DB_S_pop_req(req->argument.glip);
  req->ret_value=0;
  req->wait_count=0;
  sem_post(&req->semaphore);
}

/*****************************************************************************
 * DB_M_delete
 *****************************************************************************
 * removes a machine from the machines table. this function is called by the
 * database engine when executing a request
 * req->ret_value will be set to VS_R_ABSENT if the machine is not found, and
 * to 0 otherwise
 *****************************************************************************/
static void DB_M_delete(struct DB_db * db,struct DB_M_request * req)
{
  unsigned int zob;
  struct DB_machines_elt * z;
  struct DB_machines_elt * y;

  zob=DB_machines_hash(req->argument.reuh->meuuh);
  if (db->machines[zob]!=NULL)
  {
    z=db->machines[zob];
    if (VS_MID_NE(z->mac,req->argument.reuh->meuuh))
    {
      y=z;
      z=z->next;
      for(;z!=NULL;z=z->next)
        if (VS_MID_NE(z->mac,req->argument.reuh->meuuh))
          y=z;
        else
        {
          y->next=z->next;
          *(req->argument.reuh->coin)=z;
          req->ret_value=0;
          DB_M_push_req(z,req);
          return;
        }
      req->ret_value=VS_R_ABSENT;
      sem_post(&req->semaphore);
    }
    else
    {
      db->machines[zob]=z->next;
      *(req->argument.reuh->coin)=z;
      req->ret_value=0;
      DB_M_push_req(z,req);
      return;
    }
  }
  else
  {
    req->ret_value=VS_R_ABSENT;
    sem_post(&req->semaphore);
  }
}

/*****************************************************************************
 * DB_M_req
 *****************************************************************************
 * inserts a request on the machines table in the fifo. this function is
 * called by clients of the database engine 
 *****************************************************************************/
static void DB_M_req(struct DB_db * db, struct DB_M_request * req,
                     void (* func)(struct DB_db * db, struct DB_M_request *),
                     union DB_M_union argument)
{
  sem_init(&req->semaphore,0,0);
  req->next=NULL;
  req->argument=argument;
  req->func=(void (*)(void *, struct DB_M_request *))func;
  pthread_mutex_lock(&db->machines_requests_lock);
  if (db->machines_requests!=NULL)
    db->machines_last->next=req;
  else
    db->machines_requests=req;
  db->machines_last=req;
  pthread_cond_signal(&db->machines_requests_cond);
  pthread_mutex_unlock(&db->machines_requests_lock);
  sem_wait(&req->semaphore);
  sem_destroy(&req->semaphore);
}

/*****************************************************************************
 * DB_S_req
 *****************************************************************************
 * inserts a request on the switchs table in the fifo. this function is called
 * by clients of the database engine
 *****************************************************************************/
static void DB_S_req(struct DB_db *db, struct DB_S_request *req,
                     void (* func)(struct DB_db * db, struct DB_S_request *),
                     union DB_S_union argument)
{
  sem_init(&req->semaphore,0,0);
  req->next=NULL;
  req->argument=argument;
  req->func=(void (*)(void *, struct DB_S_request *))func;
  pthread_mutex_lock(&db->switchs_requests_lock);
  if (db->switchs_requests!=NULL)
    db->switchs_last->next=req;
  else
    	  
  db->switchs_requests=req;
  db->switchs_last=req;
  pthread_cond_signal(&db->switchs_requests_cond);
  pthread_mutex_unlock(&db->switchs_requests_lock);
  sem_wait(&req->semaphore);
  while (req->wait_count)
  {
    sem_wait(&req->semaphore);
    req->wait_count--;
  }
  sem_destroy(&req->semaphore);
}

/*****************************************************************************
 * DB_machines_loop
 *****************************************************************************
 * the database loop for machines, always running, called by the database
 * engine
 *****************************************************************************/
static void * DB_machines_loop(struct DB_db * db)
{
  struct DB_M_request * z;

  VS_log(LOGINFO, DB, "Machines loop started");
  pthread_mutex_lock(&db->db_lock);
  pthread_mutex_unlock(&db->db_lock);
  pthread_mutex_destroy(&db->db_lock);
  if (db->runlevel==DB_CANCEL)
  {
    VS_log(LOGERROR, DB, "Machines loop canceled");
    return NULL;
  }
  while (db->runlevel<DB_STOP)
  {
    pthread_mutex_lock(&db->machines_requests_lock);
    z=db->machines_requests;
    if (z!=NULL)
    {
      db->machines_requests=z->next;
      pthread_mutex_unlock(&db->machines_requests_lock);
      z->func(db, z);
    }
    else
    {
      pthread_cond_wait(&db->machines_requests_cond,
                        &db->machines_requests_lock);
      pthread_mutex_unlock(&db->machines_requests_lock);
    }
  }
  VS_log(LOGINFO, DB, "Machines loop stopped");
  return NULL;
}

/*****************************************************************************
 * DB_switchs_loop
 *****************************************************************************
 * the database loop for switchs, always running, called by the database
 * engine
 *****************************************************************************/
static void * DB_switchs_loop(struct DB_db * db)
{
  struct DB_S_request * z;

  VS_log(LOGINFO, DB, "Switchs loop started");
  while (db->runlevel<DB_STOP)
  {
    pthread_mutex_lock(&db->switchs_requests_lock);
    z=db->switchs_requests;
    if (z!=NULL)
    {
      db->switchs_requests=z->next;
      pthread_mutex_unlock(&db->switchs_requests_lock);
      z->func(db, z);
    }
    else
    {
      pthread_cond_wait(&db->switchs_requests_cond, &db->switchs_requests_lock);
      pthread_mutex_unlock(&db->switchs_requests_lock);
    }
  }
  VS_log(LOGINFO,DB,"Switchs loop stopped");
  return NULL;
}

/*****************************************************************************
 * DB_cancel
 *****************************************************************************
 * the function whose role is to cancel the database engine when its
 * initialization has failed
 *****************************************************************************/
static void DB_cancel(struct DB_db * db)
{
  db->runlevel=DB_CANCEL;
  pthread_mutex_destroy(&db->db_lock);
  pthread_mutex_destroy(&db->machines_requests_lock);
  pthread_mutex_destroy(&db->switchs_requests_lock);
  pthread_cond_destroy(&db->machines_requests_cond);
  pthread_cond_destroy(&db->switchs_requests_cond);
  VS_log(LOGERROR,DB,"Unable to spawn the database engine");
}



/*****************************************************************************/
/*                           user functions                                  */
/*****************************************************************************/

/*****************************************************************************
 * DB_start
 *****************************************************************************
 * starts the database engine *
 * returns VS_R_PTHREAD in case of problemes, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_start(struct DB_db * db)
{
  unsigned int i;

  VS_log(LOGINFO, DB, "Starting the database engine...");
  db->runlevel=DB_INIT;
  pthread_mutex_init(&db->machines_requests_lock, NULL);
  pthread_mutex_init(&db->switchs_requests_lock, NULL);
  pthread_mutex_init(&db->db_lock, NULL);
  pthread_cond_init(&db->machines_requests_cond, NULL);
  pthread_cond_init(&db->switchs_requests_cond, NULL);
  db->machines_requests=NULL;
  db->switchs_requests=NULL;
  for (i=0;i<DB_MACHINES_HASH;i++)
    db->machines[i]=NULL;
  for (i=0;i<DB_SWITCHS_HASH;i++)
    db->switchs[i]=NULL;
  pthread_mutex_lock(&db->db_lock);
  if (pthread_create(&db->thread_machines, NULL, (void *(*)())DB_machines_loop,
                     db))
  {
    pthread_mutex_unlock(&db->db_lock);
    DB_cancel(db);
    return VS_R_PTHREAD;
  }
  if (pthread_create(&db->thread_switchs, NULL, (void *(*)())DB_switchs_loop,
                     db))
  {
    DB_cancel(db);
    return VS_R_PTHREAD;
  }
  db->runlevel=DB_RUN;
  pthread_mutex_unlock(&db->db_lock);
  VS_log(LOGINFO, DB, "Database engine successfully started");
  return 0;
}

/*****************************************************************************
 * DB_machines_insert
 *****************************************************************************
 * inserts a new machine in the database *
 * returns VS_R_MEMORY in case of problems, and 0 otherwise
 *****************************************************************************/
ERR_CODE DB_machines_insert(struct DB_db * db, VS_MachineId mac,
                            VS_SwitchId switch_ip, VS_PORT port,
                            VS_CHANNEL channel)
{
  union DB_M_union z;
  struct DB_M_request req;

  z.glop=malloc(sizeof(struct DB_machines_elt));
  if (z.glop!=NULL)
  {
    VS_MID_SET(z.glop->mac,mac);
    VS_SID_SET(z.glop->switch_ip, switch_ip);
    z.glop->channel=channel;
    z.glop->port=port;
    pthread_mutex_init(&z.glop->requests_lock, NULL);
    z.glop->first=NULL;
    DB_M_req(db, &req, DB_M_insert, z);
    /* ACHTUNG !!! if the request fails (not possible currently), don't forget
     * to free(z.glop) */
    return req.ret_value;
  }
  else
    return VS_R_MEMORY;
}

/*****************************************************************************
 * DB_switchs_insert
 *****************************************************************************
 * inserts a new switch in the database, and initializes its snmp stuff
 * returns VS_R_MEMORY in case of problems, and 0 otherwise
 *****************************************************************************/
ERR_CODE DB_switchs_insert(struct DB_db * db, VS_SwitchId ip,
                           struct DB_port * ports, VS_PORT nports,
                           struct SNMP_switch * zwitch)
{
  /* At this point, the switch should have been snmp_initialized and the
   * get_macs should have been done */
    
  union DB_S_union z;
  struct DB_S_request req;
  unsigned int i;

  z.glop=malloc(sizeof(struct DB_switchs_elt));
  if (z.glop!=NULL)
  {
      VS_SID_SET(z.glop->ip,ip);
      z.glop->ports=ports;
      z.glop->nports=nports;
      z.glop->zwitch=zwitch;
      /* don't forget there is an UGLY TRICK : port numbers range from 1 to
       * nports */
      for (i=1;i<=nports;i++)
      {
        pthread_mutex_init(&ports[i].requests_lock,NULL);
        ports[i].first=NULL;
      }
      DB_S_req(db,&req,DB_S_insert,z);
      /* ACHTUNG !!! if the request fails (not possible currently), don't forget
       * to free(z.glop) */
      {
        unsigned int j;
        struct SNMP_machines_elt *y;
        VS_log(LOGDEBUG,DB,"switch %s:", z.glop->zwitch->s2.peername);
        for (j=1;j<=z.glop->zwitch->nb_ports;j++)
        {
          if (!(z.glop->zwitch->ports[j].flags&(PF_BBPORT|PF_AOBPORT|PF_ATMPORT
                                                |PF_UNUSEDPORT)))
          {
            for (y=z.glop->zwitch->machines[j];y!=NULL;y=y->next)
            {
              VS_log(LOGDEBUG,DB,"  port %u - MAC %llx", j, y->machine);
              DB_machines_insert(db,y->machine,ip,(VS_PORT)j,0);
              
            }
          }
        }
      }
      VS_log(LOGINFO,DB,"Switch %s successfuly created",VS_SID2A(ip)); 
      return req.ret_value;
    }
    else
      return VS_R_MEMORY;
}

/*****************************************************************************
 * DB_machines_delete
 *****************************************************************************
 * removes a machine from the database
 * returns VS_R_MEMORY or VS_R_ABSENT in case of problems, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_machines_delete(struct DB_db * db, VS_MachineId mac)
{
  struct DB_M_request req;
  union DB_M_union z;
  struct DB_M_reuh y;
  struct DB_machines_elt * x;

  y.coin=&x;
  y.meuuh=mac;
  z.reuh=&y;
  DB_M_req(db, &req, DB_M_delete, z);
  if (!req.ret_value)
  {
    pthread_mutex_destroy(&x->requests_lock);
    free(x);
    return 0;
  }
  else
    return req.ret_value;
}

/*****************************************************************************
 * DB_switchs_delete
 *****************************************************************************
 * removes a switch from the database
 * returns VS_R_MEMORY or VS_R_ABSENT in case of problems, 0 otherwise
 *****************************************************************************/
/*ERR_CODE DB_switchs_delete(struct DB_db *db,VS_SwitchId mac)
{
  return 0;
}*/

/*****************************************************************************
 * DB_flush
 *****************************************************************************
 * removes all entries (machines and switches) from the database
 * returns 0
 *****************************************************************************/

ERR_CODE DB_flush(struct DB_db * db)
{
  struct DB_machines_elt * machines[DB_MACHINES_HASH];
  struct DB_switchs_elt * switchs[DB_SWITCHS_HASH];
  struct DB_M_request mreq;
  struct DB_S_request sreq;
  unsigned int i;
  unsigned int j;
  struct DB_machines_elt * m;
  struct DB_switchs_elt * s;
  void * sav_ptr;

  VS_log(LOGINFO, DB, "Flushing the db");

  sem_init(&mreq.semaphore, 0, 0);
  sem_init(&sreq.semaphore, 0, 0);
  pthread_mutex_lock(&db->machines_requests_lock);
  VS_log(LOGDEBUG,DB,"All the machines are locked");
  pthread_mutex_lock(&db->switchs_requests_lock);
  VS_log(LOGDEBUG,DB,"All the switchs are locked");
  for (i=0;i<DB_MACHINES_HASH;i++)
  {
    machines[i]=db->machines[i];
    db->machines[i]=NULL;
  }
  for (i=0;i<DB_SWITCHS_HASH;i++)
  {
    switchs[i]=db->switchs[i];
    db->switchs[i]=NULL;
  }
  pthread_mutex_unlock(&db->switchs_requests_lock);
  VS_log(LOGDEBUG,DB,"All the machines are unlocked");
  pthread_mutex_unlock(&db->machines_requests_lock);
  VS_log(LOGDEBUG,DB,"All the switchs are unlocked");
  for (i=0;i<DB_MACHINES_HASH;i++)
    for (m=machines[i];m!=NULL;m=sav_ptr)
    {
      DB_M_push_req(m, &mreq);
      sem_wait(&mreq.semaphore);
      pthread_mutex_destroy(&m->requests_lock);
      sav_ptr=m->next;
      free(m);
    }
  VS_log(LOGDEBUG,DB,"All the DB_machines_elt have been freed");
  sem_destroy(&mreq.semaphore);
  for (i=0;i<DB_SWITCHS_HASH;i++)
    for (s=switchs[i];s!=NULL;s=sav_ptr)
    {
      for (j=1;j<=s->nports;j++)
      {
        DB_S_push_req(s->ports+j, &sreq);
        sem_wait(&sreq.semaphore);
        pthread_mutex_destroy(&s->ports[j].requests_lock);
      }
      free(s->ports);
      free(s->zwitch);
      sav_ptr=s->next;
      free(s);
    }
  VS_log(LOGDEBUG,DB,"All the DB_switchs_elt have been freed");
  sem_destroy(&sreq.semaphore);
  return 0;
}

/*****************************************************************************
 * DB_machines_unlock
 *****************************************************************************
 * releases the lock on an entry of the machines table
 * returns VS_R_MEMORY in case of problems, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_machines_unlock(struct DB_db * db, struct DB_machines_elt * machine)
{
  struct DB_M_request req;
  union DB_M_union z;

  z.glop=machine;
  DB_M_req(db,&req,DB_M_unlock,z);
  return req.ret_value;
}

/*****************************************************************************
 * DB_switchs_port_unlock
 *****************************************************************************
 * releases the lock on a port of an entry of the switchs table
 * returns VS_R_MEMORY in case of problems, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_switchs_port_unlock(struct DB_db * db, struct DB_port * port)
{
  struct DB_S_request req;
  union DB_S_union z;

  z.glip=port;
  DB_S_req(db,&req,DB_S_port_unlock,z);
  return req.ret_value;
}

/*****************************************************************************
 * DB_machines_lock
 *****************************************************************************
 * takes the lock on an entry of the machines table
 * returns VS_R_ABSENT or VS_R_MEMORY in case of problems, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_machines_lock(struct DB_db * db, VS_MachineId mac,
                          struct DB_machines_elt **machine)
{
  union DB_M_union z;
  struct DB_M_reuh y;
  ERR_CODE fuck;
  struct DB_M_request *req;

  req=malloc(sizeof(struct DB_M_request));
  if (req!=NULL)
  {
    y.coin=machine;
    y.meuuh=mac;
    z.reuh=&y;
    DB_M_req(db,req,DB_M_lock,z);
    if (!req->ret_value)
      return 0;
    else
    {
      fuck=req->ret_value;
      free(req);
      return fuck;
    }
  }
  return VS_R_MEMORY;
}

/*****************************************************************************
 * DB_switchs_port_lock
 *****************************************************************************
 * takes the lock on an entry of the switchs table
 * returns VS_R_ABSENT or VS_R_MEMORY in case of problems, 0 otherwise
 *****************************************************************************/
ERR_CODE DB_switchs_port_lock(struct DB_db * db,VS_SwitchId ip, 
                              VS_PORT port_num, struct DB_port ** port,
                              struct SNMP_switch ** zwitch)
{
  union DB_S_union z;
  struct DB_S_rah y;
  ERR_CODE fuck;
  struct DB_S_request *req;

  req=malloc(sizeof(struct DB_S_request));
  if (req!=NULL)
  {
    y.coin=port;
    y.meuuh=ip;
    y.zarma=port_num;
    y.zwitch=zwitch;
    z.rah=&y;
    DB_S_req(db,req,DB_S_port_lock,z);
    if (!req->ret_value)
      return 0;
    else
    {
      
      fuck=req->ret_value;
      
      free(req);
      return fuck;
    }
  }
  return VS_R_MEMORY;
}
