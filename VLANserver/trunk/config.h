/*****************************************************************************
 * config.h
 * Some configurations defines
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: config.h,v 1.5 2001/10/24 20:35:16 marcari Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __CONFIG_H__
#define __CONFIG_H__

/* Server version pas <3.1 */
#define VERSION "3.1"

/* Time between 2 atempts to contact the VLANbridge */
#define BRIDGE_SLEEPTIME 120

/* default VLANbridge port */
#define DEFAULT_ROUTER_PORT 2000

/* default server port */
#define DEFAULT_PORT 6010

/* number of retries when trying to reconnect to the VLANbridge when unable
 * to close the connection */
#define VS_CNX_RETRIES 5

/* timeout before the server stops waiting for VLANbridge's answer when trying
 * to log in */
#define VS_VLB_TIMEOUT 30

/* default config file location */
#define CFG_FILE_PATH "../vlanserver.conf"

#endif
