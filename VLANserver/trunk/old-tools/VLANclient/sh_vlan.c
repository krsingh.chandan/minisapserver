/*****************************************************************************
 * sh_vlan.c
 * Provide an interface to use client.c
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: sh_vlan.c,v 1.4 2001/11/12 15:46:09 gunther Exp $
 *
 * Authors: Laurent Rossier <gunther@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>

#include "../../types.h"
#include "../../server/client.h"

int main(void)
{

    VS_CHANNEL Channel_dest;
    unsigned Channel;
    int	a;

    printf("New Channel ?\n");
    scanf("%u",&Channel);
    Channel_dest=(VS_CHANNEL)Channel;
    a=VS_client(Channel_dest);
    if (a==VS_R_MEMORY)
        printf("Insufficent memory\n");
    else if (a==VS_R_SOCKET)
        printf("The client is enable to bind the socket\n");
    else if (a==-1)
        printf("The client can't have your date\n");
    else if (a==-2)
        printf("No response : try again\n");
  return 0;
}
