/*****************************************************************************
 * vlanc.c
 * mini client to change vlan
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: vlanc.c,v 1.2 2001/04/29 03:41:49 nitrox Exp $
 *
 * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>


#define INTERFACE "eth0"
#define SERVER "138.195.136.158"
#define PORT 31337


int main( int argc, char * argv[] )
{
    int i_socket;
    struct ifreq interface;
    struct sockaddr_in sa_server;
    struct sockaddr_in sa_client;
    char psz_msg[ 1024 ];


    /*
     * Checking whether the VLAN was given or not
     */

    if ( argc != 2 )
    {
        printf( "Usage: %s vlan\n", argv[0] );
	return( 1 );
    }


    /*
     * Looking for informations about the eth0 interface
     */

    interface.ifr_addr.sa_family = AF_INET;
    strcpy( interface.ifr_name, INTERFACE );

    i_socket = socket( AF_INET, SOCK_DGRAM, 0 );

    /* Looking for the interface IP address */
    ioctl( i_socket, SIOCGIFDSTADDR, &interface );
    printf( "ipaddr == %s\n", inet_ntoa((*(struct sockaddr_in *)(&(interface.ifr_addr))).sin_addr) );

    /* Looking for the interface MAC address */
    ioctl( i_socket, SIOCGIFHWADDR, &interface );
    printf( "macaddr == %2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x\n",
	interface.ifr_hwaddr.sa_data[0] & 0xff,
        interface.ifr_hwaddr.sa_data[1] & 0xff,
        interface.ifr_hwaddr.sa_data[2] & 0xff,
        interface.ifr_hwaddr.sa_data[3] & 0xff,
        interface.ifr_hwaddr.sa_data[4] & 0xff,
        interface.ifr_hwaddr.sa_data[5] & 0xff );

    close( i_socket );


    /*
     * Building VLAN Server descriptor
     */

    bzero( &sa_server, sizeof(struct sockaddr_in) );
    sa_server.sin_family = AF_INET;
    sa_server.sin_port = htons( PORT );
    inet_aton( SERVER, &(sa_server.sin_addr) );


    /*
     * Building VLAN Client descriptor
     */

    bzero( &sa_client, sizeof(struct sockaddr_in) );
    sa_client.sin_family = AF_INET;
    sa_client.sin_port = htons( 0 );
    sa_client.sin_addr.s_addr = htonl( INADDR_ANY );


    /*
     * Initializing the socket
     */

    i_socket = socket(AF_INET, SOCK_STREAM, 0);


    /*
     * Binding the socket to the VLAN Client
     */

    printf( "%i\n", bind(i_socket, (struct sockaddr *)(&sa_client), sizeof(sa_client)) );


    /*
     * Connecting the socket to the VLAN Server
     */

    printf( "%i\n", connect(i_socket, (struct sockaddr *)(&sa_server), sizeof(sa_server)) );


    /*
     * Sending message
     */

    sprintf( psz_msg, "%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x %s",
	interface.ifr_hwaddr.sa_data[0] & 0xff,
        interface.ifr_hwaddr.sa_data[1] & 0xff,
	interface.ifr_hwaddr.sa_data[2] & 0xff,
        interface.ifr_hwaddr.sa_data[3] & 0xff,
        interface.ifr_hwaddr.sa_data[4] & 0xff,
        interface.ifr_hwaddr.sa_data[5] & 0xff,
	argv[1] );
    printf( "%i\n", send(i_socket, psz_msg, strlen(psz_msg), 0) );
    printf( "message == %s\n", psz_msg );


    /*
     * Closing the socket
     */

    close( i_socket );


    return( 0 );
}
