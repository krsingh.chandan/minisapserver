#! /bin/sh

#/****************************************************************************
# * vlans.sh
# * script to change VLAN on a generic 3COM 1000  switch
# ****************************************************************************
# * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
# * $Id: vlans.sh,v 1.2 2001/04/29 03:41:49 nitrox Exp $
# *
# * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
# *          Michel Kaempf <maxx@via.ecp.fr>
# *
# * This program is free software; you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation; either version 2 of the License, or
# * (at your option) any later version.
# * 
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
# ****************************************************************************/

IPADDR="138.195.128.128"

MACADDR="$1"
VLAN="$2"

if PORT=`snmpwalk "$IPADDR" community .1.3.6.1.4.1.43.10.22.2.1.3 | grep -i "$(echo $MACADDR | sed s/':'/'.'/g)" | cut -d '.' -f 9`
then

    if [ "$VLAN" = "2" ]
    then
        COIN="52484"
    elif [ "$VLAN" = "3" ]
    then
        COIN="54397"
    elif [ "$VLAN" = "4" ]
    then
        COIN="50502"
    elif [ "$VLAN" = "5" ]
    then
        COIN="44776"
    elif [ "$VLAN" = "6" ]
    then
        COIN="50796"
    elif [ "$VLAN" = "7" ]
    then
        COIN="36233"
    fi

    if [ "$PORT" = "1" ]
    then
        MEUH="101"
    elif [ "$PORT" = "2" ]
    then
        MEUH="102"
    elif [ "$PORT" = "3" ]
    then
        MEUH="103"
    elif [ "$PORT" = "4" ]
    then
        MEUH="104"
    elif [ "$PORT" = "5" ]
    then
        MEUH="105"
    elif [ "$PORT" = "6" ]
    then
        MEUH="106"
    elif [ "$PORT" = "7" ]
    then
        MEUH="107"
    elif [ "$PORT" = "8" ]
    then
        MEUH="108"
    elif [ "$PORT" = "9" ]
    then
        MEUH="109"
    elif [ "$PORT" = "10" ]
    then
        MEUH="110"
    elif [ "$PORT" = "11" ]
    then
        MEUH="111"
    elif [ "$PORT" = "12" ]
    then
        MEUH="112"
    elif [ "$PORT" = "13" ]
    then
        MEUH="113"
    elif [ "$PORT" = "14" ]
    then
        MEUH="114"
    elif [ "$PORT" = "15" ]
    then
        MEUH="115"
    elif [ "$PORT" = "16" ]
    then
        MEUH="116"
    elif [ "$PORT" = "17" ]
    then
        MEUH="117"
    elif [ "$PORT" = "18" ]
    then
        MEUH="118"
    elif [ "$PORT" = "19" ]
    then
        MEUH="119"
    elif [ "$PORT" = "20" ]
    then
        MEUH="120"
    elif [ "$PORT" = "21" ]
    then
        MEUH="121"
    elif [ "$PORT" = "22" ]
    then
        MEUH="122"
    elif [ "$PORT" = "23" ]
    then
        MEUH="123"
    elif [ "$PORT" = "24" ]
    then
        MEUH="124"
    fi

    snmpset "$IPADDR" community "31.1.2.1.3.$COIN.$MEUH" i 4
fi
