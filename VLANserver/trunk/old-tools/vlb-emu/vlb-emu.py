#! /usr/bin/python

#/****************************************************************************
# * vlb-emu.py
# * this is a vlanbridge emulator
# * since the vlandridge currently does not accept to be connected with several
# * vlanservers, we need a fake vlanbridge to see if the vlanserver works
# * currently, it always accepts connections, passwords, and always tells
# * everything's alright
# ****************************************************************************
# * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
# * $Id: vlb-emu.py,v 1.3 2001/04/29 03:41:49 nitrox Exp $
# *
# * Authors: Brieuc Jeunhomme <bbp@via.ecp.fr>
# *
# * This program is free software; you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation; either version 2 of the License, or
# * (at your option) any later version.
# * 
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
# ****************************************************************************/

# this waits for a vlan change request and answers everything's alright
def handle_request(sock):
    s=sock.recv(1024)
    print "received --%s--"%s
    print "    format is : 11 session_id MAC IP VLANdest VLANsrc\n"
    if s:
        return 1
    else:
        return 0


# beginning of the main code
from socket import *

s1=socket(AF_INET,SOCK_STREAM)
s1.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
s1.bind(('',2000))
s1.listen(1)

# main loop
while 1:
    foo=s1.accept()
    s2=foo[0]
    print "\nconnect from",foo[1],"\n"
    print "received --%s--"%s2.recv(1024)
    print "    format is : 98 version login password\n"
    s2.send("A dummy answer. No matter what it is yet.\n")
    while handle_request(s2):pass
    print "connection closed\n\n\n\n\n\n"
    s2.close()
