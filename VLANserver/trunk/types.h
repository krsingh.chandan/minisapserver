/*****************************************************************************
 * types.h
 * Header file for types.c
 * Provide all typedefs
 *****************************************************************************
 * Copyright (C) 1998, 1999, 2000, 2001 VideoLAN
 * $Id: types.h,v 1.24 2001/11/12 17:30:42 marcari Exp $
 *
 * Authors:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#ifndef __TYPES_H__
#define __TYPES_H__


/* VS_SwitchId : switch identifyer to communicate between the database
 * and the server. In practice, the IP */
typedef struct in_addr VS_SwitchId;
/* macros to operate on VS_SwitchIds */
#define VS_SID_EQ(x,y) (x).s_addr==(y).s_addr
#define VS_SID_NE(x,y) (x).s_addr!=(y).s_addr
#define VS_SID_SET(x,y) (x).s_addr=(y).s_addr
/* converts the x ascii representation of a switch's IP to a VS_SwitchId */
#define VS_A2SID(x,sid) inet_aton(x,&(sid))
/* returns a statically allocated ascii representation of a VS_SwitchId */
#define VS_SID2A(sid) inet_ntoa(sid)

/* VS_IP_LENGTH : the length (in chars) of an IP address + 1 */
#define VS_IP_LENGTH 16

/* VS_PROTECTION_LEVEL : the different permission levels in the VLANserver */
enum {VS_PL_ZERO,
      VS_PL_VIDEOLAN,
      VS_PL_DIFF,
      VS_PL_FORBIDDEN,
      VS_PL_NUMBER,
      VS_PL_UNSPECIFIED};
typedef unsigned char VS_PROTECTION_LEVEL;

/* VS_UNIT : the unit number of a switch */
typedef unsigned char VS_UNIT;
#define VS_UNIT_UNSPECIFIED 1
#define VS_UNIT_MAX 4

/* ERR_CODE : error code used internally by the VLANserver
 * PLEASE KEEP ALPHABETICAL ORDER EXCEPT FOR VS_R_OK AND VS_R_NUMBER !!! */
typedef enum {VS_R_OK,
              VS_R_ABSENT,
              VS_R_ALREADY_WALKING,
              VS_R_AUTH_FAILURE,
              VS_R_BAD_CONFIG,
              VS_R_BIND,
              VS_R_CONNECT,
              VS_R_DB_STOP,
              VS_R_DNS,
              VS_R_INIT_FAILED,
              VS_R_FILE,
              VS_R_MEMORY,
              VS_R_NUMBER,
              VS_R_PARSE,
              VS_R_PIPE,
              VS_R_PIPE_WRITE,
              VS_R_PTHREAD,
              VS_R_RECV,
              VS_R_SEND,
              VS_R_SELECT,
              VS_R_SNMP,
              VS_R_SNMP_STOP,
              VS_R_SOCKET,
              VS_R_TIME,
              VS_R_TIMEOUT}ERR_CODE;

/* LOG_MODULES : modules of the VLANserver */
typedef enum {SERVER,
              SNMP,
              DB,
              CFG,
              IF,
              TELNET,
              LOGGER} LOG_MODULES;

/* LOG_LEVELS : levels of loggs */
typedef enum {LOGDEBUG,
              LOGINFO,
              LOGWARNING,
              LOGERROR,
              LOGFATAL} LOG_LEVELS;

/* VS_SwitchType : the type of a switch */
typedef enum {VS_ST_3C1000,
              VS_ST_3C1100,
              VS_ST_3C3300,
              VS_ST_3CDESK} VS_SwitchType;


/* VS_MachineId : machine identifyer to communicate between the client
 * and the server. Also used in the DB. In practice, the MAC */
typedef unsigned long long int VS_MachineId;
/* macros to operate on VS_MachineIds */
#define VS_MID_EQ(x,y) (x)==(y)
#define VS_MID_NE(x,y) (x)!=(y)
#define VS_MID_SET(x,y) (x)=(y)

/* VS_CHANNEL : the type of a VS channel, dude :P */
typedef unsigned char VS_CHANNEL;

/* VS_VLAN : the number of a VLAN */
typedef unsigned char VS_VLAN;

/* VS_PORT : the number of a port number */
typedef unsigned char VS_PORT;

/* the flags for a port's attributes */
typedef unsigned char VS_PORT_FLAGS;
#define PF_BBPORT ((VS_PORT_FLAGS)(1<<0))
#define PF_AOBPORT ((VS_PORT_FLAGS)(1<<1))
#define PF_ATMPORT ((VS_PORT_FLAGS)(1<<2))
#define PF_HUBPORT ((VS_PORT_FLAGS)(1<<3))
#define PF_UNUSEDPORT ((VS_PORT_FLAGS)(1<<4))

/* VS_port_data : all data of a port */
struct VS_port_data
{
  struct VS_vlan* vlan_struct;
  VS_PROTECTION_LEVEL protection;
  VS_PORT_FLAGS flags;
  int sLoopNumber;
};

/* The differents types of the flag for a VLAN */
typedef enum {NONE,
              T8021Q,
              VLT,
              ATM} VS_VLAN_TYPE;
                                                          

struct VS_vlan
{
  unsigned short int vlan;
  VS_VLAN_TYPE vlanType;
  struct VS_vlan* next;
  struct VS_vlan* prev;
};

/* returns the vlan number of a channel */
#define ChannelToVlan(cfg,channel) ((cfg).chan_map[channel].vlan)
/* returns the channel number of a vlan */
#define VlanToChannel(cfg,vl,channel)             {\
  unsigned long macro_loop_idx;\
  for(macro_loop_idx=0;macro_loop_idx<=(cfg).nchannels;macro_loop_idx++)\
  {\
    if((cfg).chan_map[macro_loop_idx].vlan==(vl))\
      break;\
  }\
  (channel)=(VS_CHANNEL)macro_loop_idx;             }
/* returns the protection level of a channel */
#define CHANNEL_PROTECTION(cfg,channel) ((cfg).chan_map[channel].protection)

/* define the length of the community string */
#define MAX_COMMUNITY_SIZE  32

/* define the time between two reload of the db */
#define VS_REINIT_TIME 900

#endif
