/* VideoLAN VLANbridge : logging facilities
 *
 * Definition file
 *
 * You must include <stdio.h> to use this module
*/

#ifndef _LOG_H
#define _LOG_H

/* Max buffer size */
#define LOG_BUFFSIZE	512

/* Log severity levels */
#define LOG_NOTE	0
#define LOG_WARN	1
#define LOG_ERROR	2

/* VLAnserver modules names */
#define MOD_VLANBRIDGE	0
#define MOD_MANAGER	1
#define MOD_LISTENER	2
#define MOD_SENDER	3
#define MOD_PERFORMER	4
#define MOD_ADMIN       5

int OpenLog (char* strLogFile);
void Log (int iLevel, int iModule, const char* strMsg, ...);
void LogScreen (int iLevel, int iModule, const char* strMsg, ...);
int CloseLog ();

#endif
