/* VideoLAN VLANbridge: error codes
 *
 * Definition file
 *
 * You don't need to include anything before including this file
 */


#ifndef _DEFINE_H
#define _DEFINE_H



/* Errors: iRc is < 0 */
#define GLOBAL_ERROR	-1
#define MUTEX_ERROR	-2
#define FILE_ERROR	-3

/* Others: iRc is > 0 */
#define BUFFER_FULL	1
#define BUFFER_EMPTY	2

#define BRIDGE_BUSY	5
#define BRIDGE_UNABLE   6

#endif
