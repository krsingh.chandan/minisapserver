/* VideoLAN VLANbridge: Kernel ARP table management
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * Known bug: Arp_Del doesn't work properly
 *
 * TO DO: No log is done in this module: it must be done by the performer 
*/


/* For Solaris */
#include <strings.h>
#include <sys/types.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "debug.h"
#include "log.h"
#include "arp.h"

/* VLANbridge module name */
#define MOD_NAME MOD_PERFORMER


extern int GetDevHw (char* strIfName, struct sockaddr* psaHwAddr, int iSockFd);



/***************************************************************************/
/* Set an entry in the ARP cache					   */
/***************************************************************************/
int Arp_Add(struct sockaddr_in* psaAddr, char* strDev, int iSockFd)
{
  int iRc = 0;
  struct arpreq arReq;

  ASSERT(psaAddr);
  ASSERT(strDev);

  /* Init the structure */
  bzero (&arReq, sizeof(arReq));

  /* Set the correct flags */
  arReq.arp_flags = ATF_PERM;		/* Permanent entry */
  arReq.arp_flags |= ATF_COM;		/* Complete entry */
  arReq.arp_flags |= ATF_PUBL;       	/* Publish entry */

  /* Set the host name we want to proxy for */
  memcpy(&arReq.arp_pa, psaAddr, sizeof(struct sockaddr));
  /* Set the device on which we want to publish the MAC */
  strcpy(arReq.arp_dev, strDev);
  /* Set the MAC of this device as the MAC for this entry */
  iRc = GetDevHw(strDev, &arReq.arp_ha, iSockFd);

  if (!iRc)
  {
    /* Call the kernel. */
    if (ioctl(iSockFd, SIOCSARP, &arReq) < 0)
      iRc = errno;

#ifdef DEBUG
    else
      Log (LOG_NOTE, MOD_NAME, "Proxy ARP entry for host %s on interface %s added",
           inet_ntoa(((struct sockaddr_in *)psaAddr)->sin_addr), strDev);
#endif

  }

  if (iRc)
    Log (LOG_ERROR, MOD_NAME, "Proxy ARP entry for host %s on if %s not added: %s",
         inet_ntoa(((struct sockaddr_in *)psaAddr)->sin_addr), strDev, strerror (errno)); 

  return iRc;
} 


/***************************************************************************/
/* Delete an entry from the ARP cache					   */
/***************************************************************************/
int Arp_Del (struct sockaddr_in* psaAddr, char* strDev, int iSockFd)
{
  int iRc = 0;
  struct arpreq arReq;

  ASSERT(psaAddr);
  ASSERT(strDev);

  /* Init the structure */
  bzero(&arReq, sizeof(arReq));

  /* Set the flags to only delete the entry we added with Arp_Add */
  arReq.arp_flags = ATF_PERM;		/* Permanent entry */
  arReq.arp_flags |= ATF_PUBL;       	/* Public entry */

  /* Set the host name we don't want to proxy for any more */
  memcpy(&arReq.arp_pa, psaAddr, sizeof(struct sockaddr));
  /* Select the device name on which we will delete the arp entry */
  strcpy(arReq.arp_dev, strDev);

  /* Call the kernel */
  if (ioctl(iSockFd, SIOCDARP, &arReq) < 0)
    iRc = errno;

#ifdef DEBUG
  else
    Log (LOG_NOTE, MOD_NAME, "Proxy ARP entry for host %s on interface %s deleted",
         inet_ntoa(((struct sockaddr_in *)psaAddr)->sin_addr), strDev);
#endif

  if (iRc)
    Log (LOG_ERROR, MOD_NAME, "Proxy ARP entry for host %s on if %s not deleted: %s",
         inet_ntoa(((struct sockaddr_in *)psaAddr)->sin_addr), strDev, strerror (errno));
    
  return iRc;
}
