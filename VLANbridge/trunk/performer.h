/* VideoLAN VLANbridge: Performer module
 *
 * Definition file
 *
 * You need to include <pthread.h> and <netinet/in.h> before this file
*/


#ifndef _PERFORMER_H
#define _PERFORMER_H


/* Struct that holds all informations needed to perform a request */
struct s_Request
{
  /* Request id */
  int iRequestId;
  
  /* Client identity */
  struct sockaddr_in saClientIP;
  struct sockaddr saClientMAC;
  int iVLANdst;
  int iVLANsrc;

  /* Request info */
  int iType;			/* Type of the request */
  int iStatus;			/* What have we done up to now ? */

  /* Internal information used by the VLANbridge threads */
  pthread_t ptId;		/* Id of the performer that will handle the request */
  int iNextStep;		/* What must we do next ? */
  int iMisconfig;		/* Did we detect a potential misconfiguration ? */
  struct s_Record* psRecord;	/* Corresponding entry in the database*/
};


int ConfigPerformer(FILE* hCfgFile);
int InitRouting(FILE* hCfgFile);
int InitArp(FILE* hCfgFile);
int RunPerformer(struct s_Request *psrPerformer);
int FreePerformer(const struct s_Request *psrPerformer);
 
#endif
