/* VideoLAN VLANbridge: Remote administration commands
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * TO DO: everything
 *
 * Note: the commands must be declared in the ascCommand variable of
 * the admin.c file in order to let the perser know about them
*/

/* ATTENTION: Il faudra absolument passer par le manager pour les requetes
parce que la base est pas lockee */


#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/sysinfo.h>

#include "debug.h"
#include "vlanbridge.h"
#include "network.h"
#include "admin.h"

extern struct s_Command ascCommand[];


/***************************************************************************/
/* */
/***************************************************************************/
int Logout(int iSockFd, int i, struct s_Argument* asaArg)
{
  return CMD_CLOSE;
}


/***************************************************************************/
/* */
/***************************************************************************/
int Help(int iSockFd, int iArgCount, struct s_Argument* asaArg)
{
  int iIndex = 0;

  if (iArgCount == 0)
  {
    /* Display a list of available command */
    while (strcmp(ascCommand[iIndex].strName, "") > 0)
    {
      SendResult(iSockFd, "%s: %s\n\r", ascCommand[iIndex].strName,
                 ascCommand[iIndex].strSummary);
      iIndex++;
    }
  }
  else
  {
    ASSERT(asaArg);
    
    /* Display help on a particular command */
    while (strcmp(ascCommand[iIndex].strName, "") > 0)
    {
      if ((!strcmp(ascCommand[iIndex].strName, asaArg[0].uVal.strValue)))
      {
        SendResult(iSockFd, "Usage: %s %s\n\r%s\n\r", ascCommand[iIndex].strName,
                   ascCommand[iIndex].strOptions, ascCommand[iIndex].strSummary);
        break;
      }
      else
        iIndex++;
    } 
    if (strcmp(ascCommand[iIndex].strName, "") == 0)
      SendResult(iSockFd, "Command '%s' does not exist\n\r",
                 asaArg[0].uVal.strValue);
  }
  
  SendResult(iSockFd, "\n\r");
  return 0;
}


/***************************************************************************/
/* */
/***************************************************************************/
int ServerStatus(int iSockFd, int iArgCount, struct s_Argument* asaArg)
{
  int iRc = 0;
  struct sysinfo siStatus;  
  int iDaysUptime, iHoursUptime, iMinUptime;
  
  iRc = sysinfo(&siStatus);
  
  if(!iRc)
  {
    /* Convert the date given in seconds to days/hours/minutes */
    iDaysUptime = siStatus.uptime/86400;
    siStatus.uptime -= iDaysUptime*86400;
    iHoursUptime = siStatus.uptime/3600;
    siStatus.uptime -= iHoursUptime*3600;
    iMinUptime = siStatus.uptime/60;

    /* Extract interessting info from the siStatus struct */
    SendResult(iSockFd,
               "VLANbridge version " VERSION " (built " __DATE__ ")\n\r"
               "  Uptime: %u days, %u hours, %u min\n\r"
               "  Free mem: %lu - Load: %lu\n\r", 
               iDaysUptime, iHoursUptime, iMinUptime,
               siStatus.freeram, siStatus.loads[0]);
  }
  else
  {
    /* Return the error description */
    SendResult(iSockFd, "Could not get system stats: %s", strerror(errno));
  }

  SendResult(iSockFd, "\n\r");
  return iRc;
}



/***************************************************************************/
/* */
/***************************************************************************/
int HostInfo(const char* strHostName)
{  
//  struct s_Record* psrHostInfo;
//  struct sockaddr_in saHost;
//  /* Resol DNS */

  ASSERT(strHostName);

//  psrHostInfo = FindInBase(base, &saHost);
  
//  if(psrHostInfo)
//  {
    
//  }
//  else
//  {
//    /* Not Found */
//  }
  return 0;
}


/***************************************************************************/
/* */
/***************************************************************************/
int HostReinit(const char* strHostName)
{
  return 0;
}


/***************************************************************************/
/* */
/***************************************************************************/
int BridgeReinit()
{
  return 0;
}
