/* VideoLAN VLAN bridge : Sender thread
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr>
 *
 * To-do-list:
 */


/* For Solaris */
#include <strings.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

#include "debug.h"
#include "log.h"
#include "convert.h"
#include "performer.h"
#include "manager.h"
#include "connect.h"
#include "sender.h"
#include "fifo.h"
#include "define.h"
#include "common.h"


/* Used to communicate with the manager thread */
extern struct s_manager manager;

/* Connection state */
extern int iConnectionState;
extern pthread_mutex_t ConnectionLock;


#define MOD_NAME MOD_SENDER



/***************************************************************************/
/* Warn the sender							   */
/***************************************************************************/
int WarnSender(struct s_Sender* pssSender, struct s_Request* psrJob)
{
  int iRc = 0;

  ASSERT(pssSender);
  ASSERT(psrJob);

  /* Push the request in the manager FIFO */
  iRc = FifoPush(pssSender->psfWaitingAnswers, psrJob);

  //printf ("Push -> %d\n", iRc);
  //printf ("Signal sent: %d\n", psrJob->iNextStep);

  return iRc;
}


/*****************************************************************************/
/* Create the thread used to answer to VLANserver			     */
/*****************************************************************************/
int InitSender (struct s_Sender* sender)
{
  /* Thread initialisations */
  int iRc = pthread_cond_init(&sender->WakeUpSignal, NULL);
  iRc |= pthread_cond_init(&sender->SendSignal, NULL);
  iRc |= pthread_mutex_init(&sender->SendLock, NULL);

  ASSERT(sender);

  /* Init the FIFO */
  sender->psfWaitingAnswers = FifoCreate();
  if (sender->psfWaitingAnswers == NULL)
  {
    Log(LOG_ERROR, MOD_NAME, "Unable to create sender fifo");
    iRc = -1;
  }

  /* Now create the thread used to answer to the VLANserver */
  if (!iRc)
  {
    iRc = pthread_create(&(sender->tid), NULL, SenderThread, sender);

    if (!iRc)
      Log (LOG_NOTE, MOD_NAME, "Sender thread successfully created: id %d",
           sender->tid); 
  }

  return iRc;
}




/*****************************************************************************/
/*			     */
/*****************************************************************************/
int ParseAnswer (struct s_Request* psrMsg, char* strBuff)
{
  int iRc = 0;
  char* IP;
  char MAC[18];

  ASSERT(psrMsg);
  ASSERT(strBuff);

  /* Init the buffers */
  PZERO(strBuff);
  bzero(MAC, 18);

  /* Convert MAC adress */
  iRc = mac_btoa(MAC, (unsigned char *)&psrMsg->saClientMAC.sa_data);

  /* Convert IP adress */
  if (!iRc)
  {
    IP = inet_ntoa(psrMsg->saClientIP.sin_addr);
    if (IP == 0)
      iRc = 1;
  }
  
  /* Build answer */
  if (!iRc && (sprintf(strBuff, "%d %d %s %s %d %d\n", psrMsg->iStatus,
      psrMsg->iRequestId, MAC, IP, psrMsg->iVLANdst, psrMsg->iVLANsrc) <= 0))
    iRc = 1;

  return iRc;
}


/*****************************************************************************/
/*			     */
/*****************************************************************************/
int WaitSocket(struct s_Sender* pssSender)
{
  int iRc = pthread_mutex_lock(&ConnectionLock);

  ASSERT(pssSender);
  
  if (!iRc)
  { 
    if (iConnectionState == 0)
    {
      /* Wait for the signal of the main thread */
      iRc = pthread_cond_wait(&pssSender->WakeUpSignal, &ConnectionLock);
    }

    iRc |= pthread_mutex_unlock(&ConnectionLock);
  }
 
  return iRc;
}


/*****************************************************************************/
/* A refaire, le test du iConnectionOK est trop eloigne dans tps du send     */
/*****************************************************************************/
int Answer(struct s_Sender *share)
{
  char buff[VLAN_MSG_LEN]; /*= calloc(MAX_MSG_LEN, sizeof (char));*/
  struct s_Request* psrAnswer = NULL;
  int iRc = 0;
  int iConnectionOK = 1 ;

  pthread_cond_t conds;
  pthread_mutex_t mutexs;
  pthread_mutex_init (&mutexs, NULL);
  pthread_cond_init (&conds, NULL);

  ASSERT(share);
  
  while (iConnectionOK > 0)
  { 
    /* Wait for a message if the FIFO is empty */
//    printf("Sender -> Waiting for a message from manager\n");
    iRc = FifoPull(share->psfWaitingAnswers, &psrAnswer);
//    printf("Sender -> Message received\n");
    /* Convert the msg to ascii */
    iRc = ParseAnswer(psrAnswer, buff);
    /* Message is no more used */
    free (psrAnswer);

    if (iRc)
      Log(LOG_ERROR, MOD_NAME, "Could not convert answer to ASCII: answer not sent");
    else
    {
      Log(LOG_NOTE, MOD_NAME, "Sending answer to VLANserver: %s", buff);

     /* We need those testcancel() for classical system calls are not
         cancellation points at the present time */
      pthread_testcancel();
      iConnectionOK = send(share->iSockFd, buff, strlen(buff), 0);
      pthread_testcancel();
      
      /* Check what we sended */
      if (iConnectionOK <= 0)
      { /* Connection lost : can't do anything else as waiting for a new connexion */
	iRc = errno;
	Log (LOG_WARN, MOD_NAME, "Send error: %s", strerror(errno));
	break;
      }
    }
  }

  return iRc;
}


/*****************************************************************************/
/* Thread sending the answers to the VLANserver				     */
/*****************************************************************************/
#define sender ((struct s_Sender *)arg)

void* SenderThread (void* arg)
{
  /* True until the thread must be stopped */
  int iMustLoop = 1;
  int iRc = 0;

  ASSERT(arg);

#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Sender thread is running");
#endif

  while (iMustLoop)
  {
    /* Get the sockfd on which one must send */
    iRc = WaitSocket(sender);

    /* Send the bridge answers to VLANserver */
    if (!iRc)
    {
      iRc = Answer(sender);

      /* Handle connection losts if needed */
      if (iRc)
      {
	Log (LOG_WARN, MOD_NAME, "Connection with VLANserver lost");

	/* Enter exclusive execution zone */
	iRc = pthread_mutex_lock(&ConnectionLock);

	if (!iRc)
	{
	  if (iConnectionState == 0 || iConnectionState != sender->iSockFd)
	  {
	    /* The sender has already handled the connection lost, and the
               VLANserver is either waiting for a new connection
               (iIsConnected == 0) or has already set up another connection:
               so there is nothing to do */
	  }
	  else
	    iRc = HandleDeadSocket(sender->iSockFd);
	}
 
	/* Leave exclusive execution zone */
	iRc |= pthread_mutex_unlock(&ConnectionLock);

	if (iRc)
	  Log(LOG_ERROR, MOD_NAME, "Connection lost could not be correctly handled by listener because of errors");
      }
    }
    else
    {
      Log(LOG_WARN, MOD_NAME, "Unable to get a valid socket file descriptor, retrying");
      /* Just loop */
    }
  }

  /* Exit the thread returning the NULL value */
  pthread_exit(NULL);
}



/*****************************************************************************/
/* Stop the running sender thread    					     */
/*****************************************************************************/
int CancelSender(struct s_Sender* pssSender)
{
  int iRc = 0;

  ASSERT(pssSender);
  
  if (pssSender->tid != 0)
  {
    Log(LOG_NOTE, MOD_NAME, "Stopping sender thread");

    iRc = pthread_cancel(pssSender->tid);
//    printf("Sender thread canceled with iRc %d and error %s\n", iRc, strerror(iRc));
    /* Synchronise ourselves with the destruction of the thread, to avoid to destroy
       the condition while the sender is waiting on it */
    iRc |= pthread_join(pssSender->tid, NULL);
//    printf("Sender thread joined with iRc %d and error %s\n", iRc, strerror(iRc));
    /* When the thread is cancelled while waiting for a a socket, it relocks
       the ConnectionLock, so that the sender thread cannot anymore work for
       it shares the ConnectionLock with the listener */
    pthread_mutex_trylock(&ConnectionLock);
    pthread_mutex_unlock(&ConnectionLock);
  }
  else
    Log(LOG_WARN, MOD_NAME, "Could not stop sender, none is running");

  return iRc;
}


/*****************************************************************************/
/* Free all ressources used by the running sender thread     		     */
/*****************************************************************************/
int FreeSender(struct s_Sender* pssSender)
{
  int iRc = 0;

  ASSERT(pssSender);
  
  if (pssSender->tid != 0)
  {
    iRc = pthread_cond_destroy(&pssSender->WakeUpSignal);
//    printf("Sender thread cond destoyed with iRc %d and error %s\n", iRc, strerror(iRc));

    iRc |= FifoDestroy(pssSender->psfWaitingAnswers);
//    printf("Sender thread FIFO destoyed with iRc %d and error %s\n", iRc, strerror(iRc));

    if (!iRc)
      Log(LOG_NOTE, MOD_NAME, "Sender thread destroyed");
    else
      Log(LOG_WARN, MOD_NAME, "Sender thread not cleanly destroyed");
  }
  else
    Log(LOG_WARN, MOD_NAME, "Could not destroy sender, none has been created");

  /* No more sender is running */
  pssSender->tid = 0;

  return iRc;
}
