/* VideoLAN VLANbridge: List management module
 *
 * Definition file
 *
 * You need to include "performer.h" before this file
*/


#ifndef _LIST_H
#define _LIST_H


struct cell
{ struct cell *next;
  struct cell *previous;
  struct s_performer *performer;
};

struct list
{ int size;
  struct cell first; /* Not used to store datas but to improve performance */
  struct cell last;  /* id */
};

struct list * create_list ();
void destroy_list ();

int add_to_list (struct list *l, struct s_performer* p);
int delete_from_list (struct list *l, pthread_t id);
int find_in_list (struct list *l, pthread_t id, struct cell **answer);


#endif
