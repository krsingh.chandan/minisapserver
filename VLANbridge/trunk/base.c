/* VideoLAN VLANbridge: Linked lists
 *
 * Benoit Steiner, VIA, ECP, France <benny@via.ecp.fr> 24/10/98
 *
 * TO DO:
*/


/* For Solaris */
#include <sys/types.h>
#include <sys/socket.h>

#include <stdlib.h>
#include <pthread.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdio.h>

#include "debug.h"
#include "base.h"


/***************************************************************************/
/* Remove an element given by its address from the list			   */
/* No check is done on the arguments !					   */
/***************************************************************************/
static int Remove(struct s_Base* psbBase, struct s_Cell* pcCellPos)
{
  ASSERT(psbBase);
  ASSERT(pcCellPos);

  psbBase->iSize--;

  /* Rebuild all links */
  pcCellPos->pcPrevious->pcNext = pcCellPos->pcNext;
  pcCellPos->pcNext->pcPrevious = pcCellPos->pcPrevious;

  /* Delete element */
  free (pcCellPos);
  
  return 0;
};


/***************************************************************************/
/* Create and initialise an empty database				   */
/***************************************************************************/
struct s_Base* CreateBase()
{
  /* Allocate the list */
  struct s_Base* psbBase = malloc(sizeof (struct s_Base));

//printf("Creating base\n");
  
  /* Initialise it */
  if (psbBase != NULL)
  {
    /* List is empty */
    psbBase->iSize = 0;
    /* 'Next' member of 'last' element in the list always points to NULL */
    psbBase->cLast.pcNext = NULL;
    /* 'Previous' member of 'first' element in the list always points to NULL */
    psbBase->cFirst.pcPrevious = NULL; 

    /* Link the 'first' and 'last' elements together */ 
    psbBase->cFirst.pcNext = &(psbBase->cLast);
    psbBase->cLast.pcPrevious = &(psbBase->cFirst);
  };

  return psbBase;
};



/***************************************************************************/
/* Destroy the list							   */
/* Doesn't use the too slow RemoveFromList function	 		   */
/***************************************************************************/
int DestroyBase(struct s_Base* psbBase)
{
  int iRc = 0;

  ASSERT(psbBase);

//printf("Destroying base\n");
  
  /* Empty the list */
  while (psbBase->iSize > 0 && !iRc)
  {
    iRc = Remove (psbBase, psbBase->cFirst.pcNext);
    psbBase->iSize--;
  }

  /* Detroy the list structure */
  free (psbBase); 

  return 0;
};


/***************************************************************************/
/* Add an element at the end of the list (before the 'last' element) 	   */
/***************************************************************************/
int AddToBase(struct s_Base* psbBase, struct s_Record* psrNewMember)
{
  int iRc = 0;
  struct s_Cell* pcNewCell = NULL;

  ASSERT(psbBase);
  ASSERT(psrNewMember);

//  printf("Adding to base\n");

  if ((pcNewCell = (struct s_Cell *)malloc(sizeof(struct s_Cell))) > 0)
  {
    /* initialise the links of the new element */
    pcNewCell->psrJob= psrNewMember;
    pcNewCell->pcNext = &(psbBase->cLast);
    pcNewCell->pcPrevious = psbBase->cLast.pcPrevious;

    /* Initialise the former last element (the one before the 'last' member */
    (psbBase->cLast.pcPrevious)->pcNext = pcNewCell;
    psbBase->cLast.pcPrevious = pcNewCell;

    psbBase->iSize++;
  }
  else
    iRc = 1;
  
  return iRc;
};



/***************************************************************************/
/* Find an element in the list					 	   */
/***************************************************************************/
struct s_Record* FindInBase(struct s_Base* psbBase, struct sockaddr_in* psaHost)
{
  int iIndex = 0;
  struct s_Cell* pcCurrent = psbBase->cFirst.pcNext;
  struct s_Record* psrResult = NULL;

  ASSERT(psbBase);
  ASSERT(psaHost);

//  printf ("Searching in base...\n");

  while (iIndex < psbBase->iSize)
  {
    if (pcCurrent->psrJob->saHost.sin_addr.s_addr == psaHost->sin_addr.s_addr)
    {
      /* Set return value */
      psrResult = pcCurrent->psrJob;
//      printf("Founded at offset: %d\n", iIndex);
      break;
    }
    
    iIndex++;
    pcCurrent = pcCurrent->pcNext;
  };

  return psrResult;
};


/***************************************************************************/
/* Return the niumber of entries in the base    		 	   */
/***************************************************************************/
inline int GetBaseSize(struct s_Base* psbBase)
{
  ASSERT(psbBase);

  return psbBase->iSize;
}


/***************************************************************************/
/* Retrieve an element given by its index from the base		 	   */
/***************************************************************************/
struct s_Record* GetBaseRecord(struct s_Base* psbBase, unsigned int uiPos)
{
  struct s_Cell* pcCurrent = &psbBase->cFirst;
  int iIndex = 0;
  ASSERT(psbBase);

  if (uiPos >= psbBase->iSize)
    return NULL;
  else
  {
    while (iIndex <= uiPos)
    {
      iIndex++;
      pcCurrent = pcCurrent->pcNext;
    }
  }
  
  return pcCurrent->psrJob;
}
