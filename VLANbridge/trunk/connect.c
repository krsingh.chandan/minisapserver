/* VideoLAN VLANbridge: Management of incomming connections
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * TO DO: Check whether incoming conections come from VLANserver
          Refaire la sequence de login (trop mauvaise, password pas checkes,
          etc...)
          Repousser l'identification, parce que si quelqu'un se loggue et ne
          fait rien, il bloque tout jusuqu'au timeout
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#include "debug.h"
#include "admin.h"
#include "listener.h"
#include "performer.h"
#include "sender.h"
#include "common.h"
#include "log.h"
#include "network.h"
#include "connect.h"


#define MOD_NAME MOD_VLANBRIDGE
extern struct s_Listener listener;
extern struct s_Sender sender;

pthread_mutex_t ConnectionLock;
int iConnectionState = 0;

#define VLANSERVER_CALL 1
#define ADMIN_CALL 2
#define UNKNOWN_CALL -1




/***************************************************************************/
/* An answer has the following format:					   */
/* 1: Answer code							   */
/* 2: Human readable msg						   */ 
/***************************************************************************/
int SendAnswer (int iSockFd, int iAnswerCode, const char* strAnswerMsg)
{
  char strAnswer[VLAN_MSG_LEN];

  int iRc = snprintf(strAnswer, VLAN_MSG_LEN, "%d - %s\n", iAnswerCode,
                     strAnswerMsg);

  if (iRc > 0)
    iRc = send(iSockFd, strAnswer, strlen(strAnswer), 0);
  
  if (iRc < 0)
    Log (LOG_WARN, MOD_NAME, "Unable to send answer message");
  
  return iRc;
}


/***************************************************************************/
/*  */
/***************************************************************************/
int SetupAdminConn(int iSockFd)
{
  int iRc = 0;
  struct s_Admin* psaAdmin = malloc(sizeof(struct s_Admin));
  pthread_attr_t ptAttr;

  pthread_attr_init(&ptAttr);
  pthread_attr_setdetachstate(&ptAttr, PTHREAD_CREATE_DETACHED);  

  psaAdmin->iSockFd = iSockFd;

  /* TO DO: Only one admin at a time */
  iRc = SendAnswer(iSockFd, VLAN_LOGIN_OK, "Admin logged in");

//AdminThread(psaAdmin);
  iRc |= pthread_create(&psaAdmin->tId, &ptAttr, AdminThread, psaAdmin);

   if (!iRc)
     Log (LOG_NOTE, MOD_NAME, "Admin thread successfully created: id %d",
          psaAdmin->tId);

  return iRc;
}



/***************************************************************************/
/* Wake up listener and sender if not already connected 		   */
/***************************************************************************/
int SetupVSrvConn(int iSockFd)
{
  int iOption = 1;
  int iRc = pthread_mutex_lock (&ConnectionLock);

  //printf ("Setting up connection\n");

  if (iRc)
  {
    Log(LOG_ERROR, MOD_NAME, "Closing connection with VLANserver: mutex error");
    close (iSockFd);
  }
  else
  {
    //printf ("ConectionState = %d\n", iConnectionState);
    
    if (iConnectionState != 0)
    {
      /* Already connected: close the connection */
      Log (MOD_NAME, LOG_NOTE, "Closing connection with VLANserver: already connected");
      iRc = SendAnswer(iSockFd, VLAN_LOGIN_KO, "Already connected with another VLANserver");	
      iRc = close (iSockFd);
    }
    else 
    {
      iRc = SendAnswer(iSockFd, VLAN_LOGIN_OK, "Now connected with VLANbridge");	

      /* Keep the connection alive when no data is sent */
      if (setsockopt(iSockFd, SOL_SOCKET, SO_KEEPALIVE, &iOption, sizeof(int)))
	Log(LOG_NOTE, MOD_NAME, "Unable to set option KEEP_ALIVE for connection with VLANserver");
      
      /* We are connected now */
      iConnectionState= iSockFd;
    
      /* Commit the socket to both listener and sender threads */
      listener.iSockFd = iSockFd;
      sender.iSockFd = iSockFd;

      /* Wake them up */
      iRc = pthread_cond_signal(&listener.WakeUpSignal);
      iRc |= pthread_cond_signal(&sender.WakeUpSignal);

      if (iRc)
      	Log(LOG_ERROR, MOD_NAME, "Could not wake up communication threads");
      else
	Log(LOG_NOTE, MOD_NAME, "Connection with VLANserver established");
    }
  }
  
  if (pthread_mutex_unlock(&ConnectionLock))
  {
    iRc = -1;
    Log(LOG_ERROR, MOD_NAME, "Could not release mutex");
  }
     
  return iRc;
}


/***************************************************************************/
/* Update the VLANserver status	when communication with VLANserver is lost */
/***************************************************************************/
int HandleDeadSocket(int iSockFd)
{
  iConnectionState = 0;
  return close(iSockFd);
}


/***************************************************************************/
/* Open and initialize the socket which will wait for connections 	   */
/***************************************************************************/
int OpenWaitingSocket (const char* strWaitPort)
{
  struct sockaddr_in saServerAddr;
  int iPort, iWaitSockFd = 0, iWaitSockOpt = 1;
   socklen_t iAddrLen = sizeof(saServerAddr);
  struct servent* sp;
  int iRc = 0;

  ASSERT(strWaitPort);
  
  /* Open socket for waiting on TCP connections */
  if ((iWaitSockFd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
  {
    Log (LOG_ERROR, MOD_NAME, "Cannot open a socket to wait for incoming connections: %s", strerror(errno));
    iRc = errno;
  }

  /* Init the descriptor */
  if (!iRc)
  {
    bzero(&saServerAddr, sizeof(struct sockaddr_in));
    saServerAddr.sin_family = AF_INET;

    /* Receive on all local addresses */
    saServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* Set the port we listen to */
    if ((iPort = atoi(strWaitPort)))
      saServerAddr.sin_port = htons(iPort);
    else
    {
      /* It's given by its service name */
      if ((sp = getservbyname(strWaitPort, "tcp")) == NULL)
      {
	iRc = errno;
	Log(LOG_ERROR, MOD_NAME, "Unknown service port: %s",
	    strWaitPort);
      }
      else
	saServerAddr.sin_port = sp->s_port;
    }
  }  

  /* Allow to re-use the port */
  if (!iRc)
  {
    if (setsockopt(iWaitSockFd, SOL_SOCKET, SO_REUSEADDR, &iWaitSockOpt, sizeof(iWaitSockOpt)) < 0)
    {
      iRc = errno;
      Log (LOG_ERROR, MOD_NAME, "Unable to allow the socket that must wait for incoming connections to re-bind a busy port: %s", strerror (iRc));
    }
  }

  /* Bind the socket */
  if (!iRc)
  {
    if (bind(iWaitSockFd, (struct sockaddr *)&saServerAddr, sizeof(struct sockaddr)) < 0)
    {
      iRc = errno;
      Log(LOG_ERROR, MOD_NAME, "Unable to bind the socket that must wait for incoming connections: %s", strerror (iRc));
    }
  }

  /* Listen for incoming connections */
  if (!iRc)
  {
    if (listen(iWaitSockFd, MAX_IN_CONN) < 0)
    {
      iRc = errno;
      Log(LOG_ERROR, MOD_NAME, "Unable to listen to incoming connections: %s", strerror(iRc));
    }
    else
    {
      getsockname(iWaitSockFd, (struct sockaddr*)&saServerAddr, &iAddrLen);
      Log(LOG_NOTE, MOD_NAME, "Listening on address %s port %d",
          inet_ntoa(saServerAddr.sin_addr), ntohs(saServerAddr.sin_port));
    }
  }

  if (!iRc)
    return iWaitSockFd;
  else
    return -1;
}


/***************************************************************************/
/* */
/***************************************************************************/
//int RecvLogin(int iSockFd, char* strBuff)
//{
//  int iRc = 0;
//  struct sockaddr_in saPeerName;
//  int iNameLen = sizeof(saPeerName);

//  ASSERT(strBuff);

//  iRc = getpeername(iSockFd, &saPeerName, &iNameLen);

//  iRc = RecvReq(iSockFd, strBuff, VLAN_MSG_LEN-1, 60);

  /* Check reception was ok */
//  if (iRc < 0)
//  {
//    Log(LOG_WARN, MOD_NAME, "Unable to receive data from %s: %s",
//        inet_ntoa(saPeerName.sin_addr), strerror(errno));
//  }
//  else if (iRc == 0)
//  {
//    Log(LOG_WARN, MOD_NAME, "Connection with %s lost or timed out",
//        inet_ntoa(saPeerName.sin_addr));
//  }
//  else
//  {
//    /* Add a '\0' char at the end of the buff to be able to deal with it as
//       with a string */
//    strBuff[iRc] = '\0';
//  }
  
//  return iRc;
//}


/***************************************************************************/
/* Authentication of the call						   */
/***************************************************************************/
int Authenticate (const char* strLogin, const char* strPasswd, int* piLogin)
{
  int iRc = 0;

  ASSERT(strLogin);
  ASSERT(strPasswd);
  ASSERT(piLogin);

  if (!strcmp (strLogin, "admin"))
  {
    /* Administrative call */
    *piLogin = ADMIN_CALL;
  }
  else if (!strcmp (strLogin, "VLANserver"))
  {
    *piLogin = VLANSERVER_CALL;
  }
  else
  {
    /* Ou si password n'est pas bon */
    Log (LOG_NOTE, MOD_NAME, "Login from unknown user");
    *piLogin = UNKNOWN_CALL;
  }

  return iRc;
}

      
/***************************************************************************/
/* Check the identity of the caller and it's purpose                       */
/***************************************************************************/
int GetIdentity(int iSockFd, int* piLogin)
{
  char strLoginBuff[VLAN_MSG_LEN];
  int iReqType;
  char strVersion[VLAN_MSG_LEN], strLogin[VLAN_MSG_LEN], strPasswd[VLAN_MSG_LEN];

  /* Receive the login */
  int iRc = RecvCmd(iSockFd, strLoginBuff, sizeof(strLoginBuff), 60);

  ASSERT(piLogin);

  /* Parse the received message */
  if (iRc > 0)
  {
    iRc = sscanf (strLoginBuff, "%d %s %s %s", &iReqType, strVersion,
                  strLogin, strPasswd);

    /* Check the arguments */
    if (iRc == 4 && iReqType == VLAN_LOGIN_REQUEST)
      iRc = Authenticate (strLogin, strPasswd, piLogin);
    else
    {
      Log (LOG_NOTE, MOD_NAME, "Bad login message");
      iRc = -1;
      
      /* Warn the caller */
      SendAnswer(iSockFd, VLAN_ERROR, "Connection refused: Unrecognised message");
    }
  }
  else
  {
    /* Convert the iRc (-1 or 0) to a standard error code */
    iRc = -1;
  }
  
  return iRc;
}
 

/***************************************************************************/
/* Wait for and manage incoming connections				   */
/***************************************************************************/
int ProcessConnections (int iSockFd)
{
  int iRc = 0;
  struct sockaddr_in saRemoteAddr;
  int iRemoteAddrLen;
  int iNewSockFd;
  int iLogin;

  /* We're not connected for the moment */
  iConnectionState = 0;

  /* Init the structures used to store the address of the caller */
  bzero(&saRemoteAddr, sizeof(saRemoteAddr));
  iRemoteAddrLen = sizeof(saRemoteAddr);

  for (;;)
  { 
    iNewSockFd = accept(iSockFd, (struct sockaddr *)&saRemoteAddr, &iRemoteAddrLen);

    if (iNewSockFd < 0 && errno == EBADF)
    {
      /* The socket was closed, this means we must exit the loop to enter the exit process */
      break;
    }
    else if (iNewSockFd < 0)
    {
      /* An error occured */
      Log(LOG_ERROR, MOD_NAME, "Unable to process incoming connection from %s: %s",
           inet_ntoa(saRemoteAddr.sin_addr), strerror(errno));

      /* It may be a fast loop, so slow it down */
      usleep(10000);

      /* Retry with another connection */
      continue;
    }
    else
    { 
      Log(LOG_NOTE, MOD_NAME, "Processing incoming connection from %s...",
           inet_ntoa(saRemoteAddr.sin_addr));

      /* Listen for the initial caller's identification */
      iRc = GetIdentity(iNewSockFd, &iLogin);

      if (iRc)
      {
	/* Login failed: reject the connection */
	Log(LOG_NOTE, MOD_NAME, "Aborting incoming connection from %s",
            inet_ntoa(saRemoteAddr.sin_addr));
	iRc |= close(iNewSockFd);
      }
      else
      {
	/* Check the caller's identity */
	  switch (iLogin)
	  {
	    case UNKNOWN_CALL:
	    {
	      /* Identification failed : reject the connection */
              Log(LOG_NOTE, MOD_NAME, "Rejecting incoming connection from %s: bad identification",
                  inet_ntoa(saRemoteAddr.sin_addr));
	      iRc = SendAnswer(iNewSockFd, VLAN_LOGIN_KO, "Connection refused: Bad login");
	      iRc |= close(iNewSockFd);
	      break;
	    }
	
	    case VLANSERVER_CALL:
	    {
	      /* Identification ok : the VLANserver is calling */
	      Log (LOG_NOTE, MOD_NAME, "VLANserver successfully logged in");
	      iRc = SetupVSrvConn(iNewSockFd);
	      break;
	    }
	
	    case ADMIN_CALL:
	    {
	      /* Identification ok : Administrative call (NOT YET IMPLEMENTED) */
	      Log (LOG_NOTE, MOD_NAME, "Admin successfully logged in");	
	      iRc = SetupAdminConn(iNewSockFd);
	      break;
	    }

	    default:
	    {
	      /* Reject the connexion */
	      Log (LOG_NOTE, MOD_NAME, "Rejecting incoming connection from %s: bad login",
                   inet_ntoa(saRemoteAddr.sin_addr));

	      iRc = SendAnswer(iNewSockFd, VLAN_LOGIN_OK, "Connection refused: Bad login");
	      iRc |= close (iNewSockFd);
	    }
	  }
	}
      }

#ifdef DEBUG    
      Log( LOG_NOTE, MOD_NAME, "Waiting for another connection");
#endif
  }

  /* Never reached */
  iRc |= close(iNewSockFd); /* Close the temporary socket if it hasn't already been done */

  return iRc;
}



/***************************************************************************/
/* Close all open connections						   */
/***************************************************************************/
int CloseConnections(int iSocketFd)
{
  int iRc = close(iSocketFd);

  return iRc;
}
