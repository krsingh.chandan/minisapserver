/* VideoLAN VLANbridge: Kernel ARP table management
 *
 * Definition file
 *
 * You need to include <sys/socket.h> before including this file
*/

#ifndef _ARP_H
#define _ARP_H

int Arp_Del (struct sockaddr_in* psaAddr, char* strDev, int iSockFd);
int Arp_Add (struct sockaddr_in* psaAddr, char* strDev, int iSockFd);

#endif
