/* VideoLAN VLANbridge: network operations
 *
 * Definition file
 *
 * You need to include <nothing> to use this file
 */


void InitDescriptor(struct sockaddr_in* psaDescriptor);
int SetPort(struct sockaddr_in* psaDescriptor, const char* strPort);
int SetHost(struct sockaddr_in* psaDescriptor, const char* strHost);

int RecvReq(int iSockFd, char* strBuff, int iBuffLen, int iTimeout);
int RecvCmd(int iSockFd, char* strBuff, int BuffLen, int iTimeout);

int SendResult(int iSockFd, char* strMsg, ...);
