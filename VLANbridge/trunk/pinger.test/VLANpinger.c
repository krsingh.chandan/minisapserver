/* VideoLAN pinger
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
*/

#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/ip_icmp.h>

#include "pinger.h"

int main ()
{
  char host[] = "caribou.via.ecp.fr";
  struct sockaddr_in target;
  unsigned long inaddr;
  struct hostent *hp;
  struct s_Pinger pinger_cfg;
  int hasmoved = 0;

  bzero (&target, sizeof (struct sockaddr_in));
  target.sin_family = AF_INET;

  if ( (inaddr = inet_addr(host)) != INADDR_NONE)
  {
    /* it's dotted-decimal */
    bcopy(&inaddr, &target.sin_addr, sizeof(inaddr));
  }
  else
  {
    if ( (hp = gethostbyname(host)) == NULL)
      return -1;
    bcopy(hp->h_addr, &target.sin_addr, hp->h_length);
  };

  printf ("Target resolved\n");
  printf ("Its address is: %d, %d\n", target.sin_family, target.sin_addr.s_addr);

  printf("Initialising Pinger\n");
  if (Pinger_Init(&target, &pinger_cfg))
  {
    printf("Unable to initialise pinger\n");
    exit(1);
  }
  
/*  printf("Detecting host move\n");
  if (Pinger_DetectMove (&pinger_cfg, &hasmoved))
  {
    printf("Unable to ping host\n");
    exit(1);
  }
  else
    printf("Has moved = %d\n", hasmoved);
*/  

  printf("Checking host move\n");
  if (Pinger_CheckMove (&pinger_cfg, &hasmoved))
  {
    printf("Unable to ping host\n");
    exit(1);
  }
  else
    printf("Has moved = %d\n", hasmoved);  

  printf("Destroying pinger\n");
  return Pinger_Destroy(&pinger_cfg);
};
