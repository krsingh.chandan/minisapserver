/* VideoLAN VLANbridge: type conversions
 *
 * Definition file
 *
 * You don't need to include any file to use this module
*/


int mac_atob (char* MAC, unsigned char* mac_addr);
int mac_btoa (char* MAC, unsigned char* mac_addr);
