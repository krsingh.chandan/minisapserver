/* VideoLAN VLANbridge: Gratuitous module
 *
 * Definition file
 *
 * You need to include <netinet.in.h> before this file
*/


#ifndef _GRATUITOUSARP_H
#define _GRATUITOUSARP_H

int Gratuitous_Send (const struct sockaddr_in* psaFakeIP,
                     const struct sockaddr* psaFakeHw, const char *strDev);

int GetDevHw (const char* strIfName, struct sockaddr* psaHwAddr, int iSockFd);
int GetDevPa (const char* strIfName, struct sockaddr* psaPaAddr, int iSockFd);

#endif /* _GRATUITOUSARP_H */
