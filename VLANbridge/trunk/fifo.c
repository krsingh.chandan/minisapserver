/* VideoLAN VLANbridge: fifo for thread communication
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * To-do-list : Create a FIFO for communications with others threads
                Make a database that holds the state of each computer
*/


/* For Solaris */
#include <sys/types.h>
#include <sys/socket.h>

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <net/if.h>
#include <netinet/in.h>

#include "debug.h"
#include "log.h"
#include "fifo.h"
#include "define.h"

#define MOD_NAME MOD_VLANBRIDGE



/***************************************************************************/
/* Create the FIFO							   */
/***************************************************************************/
struct s_Fifo* FifoCreate()
{
  /* Create the structure used to describe the fifo state */
  struct s_Fifo* psfFifo = malloc(sizeof(struct s_Fifo));

  if (psfFifo != NULL)
  {
    /* Initialise the fifo */
    psfFifo->psrWhereToPull = psfFifo->asrData;
    psfFifo->psrWhereToPush = psfFifo->asrData;
    psfFifo->iSize = 0;

    /* Initialise the mutex used to access the data */
    pthread_mutex_init(&psfFifo->FifoLock, NULL);
    pthread_cond_init(&psfFifo->FifoSignal, NULL);
  }
  else
    Log(LOG_ERROR, MOD_NAME, "Could not create FIFO");

  return psfFifo;
}


/***************************************************************************/
/* Destroy the FIFO							   */
/***************************************************************************/
int FifoDestroy(struct s_Fifo* psfFifo)
{
  int iRc = pthread_mutex_destroy(&psfFifo->FifoLock);
//  printf("FIFO mutex destruction: %s\n", strerror(iRc));

  iRc |= pthread_cond_destroy(&psfFifo->FifoSignal);
//  printf("FIFO cond destruction: iRc %d, %s\n", iRc, strerror(iRc));

  free(psfFifo);

  return iRc;
}


/***************************************************************************/
/* Add an element in the FIFO						   */
/* Two thread can push something at the the same time			   */
/***************************************************************************/
int FifoPush(struct s_Fifo* psfFifo, struct s_Request* psrToStore)
{
  /* Keep the lock during the complete operation */
  int iRc = pthread_mutex_lock(&psfFifo->FifoLock);

  ASSERT(psfFifo);
  ASSERT(psrToStore);

  if (!iRc)
  {
//    printf ("fifo Size: %d\n", psfFifo->iSize);
    
    if (psfFifo->iSize < FIFO_SIZE)
    {
      /* Add the element: no lock is needed, for readed data are not the ones we write */
      *(psfFifo->psrWhereToPush) = psrToStore;
      
      /* Update the WhereToPush pointer */
      if (++(psfFifo->psrWhereToPush) > psfFifo->asrData + FIFO_SIZE-1)
      {
	/* We must roll to the 1st element of the array */
	psfFifo->psrWhereToPush = psfFifo->asrData;
      }
          
      /* The buffer has now grown */
      psfFifo->iSize++;
 
      /* Wake up any thread waiting by a call to pull */
      iRc = pthread_cond_signal(&psfFifo->FifoSignal);
    }
    else
    {
#ifdef DEBUG
      Log(LOG_NOTE, MOD_NAME, "FIFO full");
#endif
      iRc = BUFFER_FULL;
    }
  }

  /* Unlock te mutex for we have finished */
  iRc |= pthread_mutex_unlock(&psfFifo->FifoLock);

  return iRc;
}


/***************************************************************************/
/* Get 1st element of the FIFO						   */
/* Be carefull not to pull two at the same time, it won't work		   */
/***************************************************************************/
int FifoPull(struct s_Fifo* psfFifo, struct s_Request** psrTask)
{
  int iRc = pthread_mutex_lock(&psfFifo->FifoLock);

  ASSERT(psfFifo); 
  ASSERT(psrTask);

  if (!iRc)
  {
    if (psfFifo->iSize == 0)
    {
      /* Wait for something to be pushed in the stack */
      iRc = pthread_cond_wait(&psfFifo->FifoSignal, &psfFifo->FifoLock);
    }
    
    /* Release the lock */
    iRc |= pthread_mutex_unlock(&psfFifo->FifoLock);
 
    if (!iRc)
    {
      /* Read the element: no lock is needed, for written data are not the ones we read */
      *psrTask = *(psfFifo->psrWhereToPull);

      /* Update the WhereToPull pointer */
      if (++(psfFifo->psrWhereToPull) > psfFifo->asrData + FIFO_SIZE-1)
      {
	/* We must roll to the 1st element of the array */
	psfFifo->psrWhereToPull = psfFifo->asrData;
      }
      
      /* The iSize member can also be accessed by a push, so lock it */
      pthread_mutex_lock(&psfFifo->FifoLock);
      psfFifo->iSize--;
      pthread_mutex_unlock(&psfFifo->FifoLock);
    }
  }

  return iRc;
}
