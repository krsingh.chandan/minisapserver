/* VideoLAN VLANbridge: network operations
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 * Regis Duchesne, ECP, <hpreg@via.ecp.fr>
*/


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <netdb.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "debug.h"
#include "log.h"
#include "network.h"

#define MOD_NAME MOD_VLANBRIDGE


/***************************************************************************/
/* Initialize a descriptor                                                 */
/***************************************************************************/
void InitDescriptor(struct sockaddr_in* psaDescriptor) 
{
  ASSERT(psaDescriptor);

  /* fill it with zero */
  memset(psaDescriptor, 0, sizeof(*psaDescriptor));

  /* set the protocol family */
  psaDescriptor->sin_family = AF_INET;
}


/***************************************************************************/
/* Fill in a descriptor with a given port                                  */
/***************************************************************************/
int SetPort(struct sockaddr_in* psaDescriptor, const char* strPort)
{
  int iPort;
  struct servent* sp;

  ASSERT(psaDescriptor);
  ASSERT(strPort);

  if ((iPort = atoi(strPort)))
  {
    /* It's given by its number */
    psaDescriptor->sin_port = htons(iPort);
  }
  else
  {
    /* It's should be given by its service name */
    sp = getservbyname(strPort, "tcp");
    if (sp == NULL)
    {
      Log(LOG_WARN, MOD_NAME, "Unknown port: %s/tcp", strPort);
      return -1;
    }
    else
      psaDescriptor->sin_port = sp->s_port;
  }
  return 0;
}


/***************************************************************************/
/* Fill in a descriptor with a given address                               */
/***************************************************************************/
int SetHost(struct sockaddr_in* psaDescriptor, const char* strHost)
{
  unsigned long inaddr;
  struct hostent *hp;
        
  if ((inaddr = inet_addr(strHost)) != -1)
  {  
    /* it's dotted-decimal */
    memcpy(&psaDescriptor->sin_addr, &inaddr, sizeof(inaddr));
  }
  else
  {
    /* It should be given by it's host name */
    hp = gethostbyname(strHost);
    if (hp == NULL)
    {
      Log(LOG_WARN, MOD_NAME, "Unknown host: %s", strHost);
      return -1;
    }
    else
      memcpy(&psaDescriptor->sin_addr, hp->h_addr, hp->h_length);
  }
  return 0;
}

/***************************************************************************/
/* Connect to the network                                                  */
/***************************************************************************/
//int OpenSocket(char *port)
//{
//  struct sockaddr_in local_addr;
//  int fd;
//  int sockopt;

  /* open a socket for UDP/IP */
//  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
//    log(LOG_ARG, "socket() error");

//  sockopt = 1;
//  init_descriptor(&local_addr);
  /* receive on all addresses */
//  local_addr.sin_addr.s_addr = htonl(INADDR_ANY);
//  set_port(&local_addr, port);

  /* allow to re-bind() a busy port */
//  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) < 0)
//    log(LOG_ARG, "SO_REUSEADDR setsockopt error");

  /* tell the system about the local port and IP address */
//  if (bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr)) < 0)
//      log(LOG_ARG, "bind() error");

//  return fd;
//}


/***************************************************************************/
/* Receive on a connected socket with a timeout                            */
/***************************************************************************/
int RecvReq(int iSockFd, char* strBuff, int iLen, int iTimeout)
{
  fd_set rfds;
  struct timeval tv;
  int iRc;

  ASSERT(strBuff);

  /* Watch the given fd to see when it has input */
  FD_ZERO(&rfds);
  FD_SET(iSockFd, &rfds);
  
  /* Wait up to timeout seconds. */
  tv.tv_sec = iTimeout;
  tv.tv_usec = 0;

  /* Select return with error EINTR when a signal is caught: detect it
     and retry a select when this occurs */
  do
  {
    iRc = select(iSockFd+1, &rfds, NULL, NULL, &tv);

    if (iRc > 0)
    {
      /* Data were received before timeout */
      iRc = recv(iSockFd, strBuff, iLen, 0);
    }
//    else if (iRc == 0)
//    {
      /* Host didn't respond */
//      Log(LOG_WARN, MOD_NAME, "Timeout");
//    }
//    else
//    {
//      /* Select error */
//      Log(LOG_ERROR, MOD_NAME, "Select error: %s", strerror(errno));

//      printf("AOB recv ?: %d\n", recv( iSockFd, strBuff, iLen, MSG_OOB));
//    }
  }
  while (iRc == -1 && errno == EINTR);

  return iRc;
}


/***************************************************************************/
/* Receive a command from a telnet client                                  */
/* We must receive characters one by one in order to support windows       */
/* client which send characters one at a time                              */
/***************************************************************************/
int RecvCmd(int iSockFd, char* strBuff, int iBuffLen, int iTimeout)
{
  struct sockaddr_in saPeerName;
  int iNameLen = sizeof(saPeerName);
  int iPos = 0;
  int iRc = 1;

  ASSERT(strBuff);

  getpeername(iSockFd, &saPeerName, &iNameLen);

  while (iRc > 0)
  {
    /* Recv next char with a 60 seconds timeout */
    iRc = RecvReq(iSockFd, strBuff+iPos, 1, iTimeout);
//    iRc = recv(iSockFd, strBuff+iPos, 1,0);
    
    /* Check reception was ok */
    if (iRc < 0)
    {
      Log(LOG_WARN, MOD_NAME, "Unable to receive data from %s: %s",
          inet_ntoa(saPeerName.sin_addr), strerror(errno));
      break;
    }
    else if (iRc == 0)
    {
       Log(LOG_WARN, MOD_NAME, "Connection with %s lost or timed out",
           inet_ntoa(saPeerName.sin_addr));
       break;
    }
    /* Parse buffer looking for a termination char */
    else if (strBuff[iPos] == '\n' || strBuff[iPos] == '\0')
    {
      /* Add a '\0' char at the end of the buff to be able to deal with it as
         with a string */
      strBuff[iPos+1] = '\0';
      iRc = strlen(strBuff)+1;
      break;
    }
    /* Avoid buffer overflow */
    else if (iPos >= iBuffLen-1)
    {
      Log(LOG_WARN, MOD_NAME, "Message from %s too long: aborting",
          inet_ntoa(saPeerName.sin_addr));      
      iRc = -1;
      break;
    }
    /* Msg not complete: looping */
    else
      iPos++;
  }

  return iRc;
}


/***************************************************************************/
/* Send an answer on a connected socket                                    */
/***************************************************************************/
int SendResult(int iSockFd, char* strMsg, ...)
{
  va_list ap;
  char* strAnswer;
  int iRc = 0;
  
  ASSERT(strMsg);

  va_start(ap, strMsg);
  vasprintf(&strAnswer, strMsg, ap);
  va_end(ap);

  iRc = send(iSockFd, strAnswer, strlen(strAnswer), 0);

  if (iRc < 0)
    Log(LOG_ERROR, MOD_NAME, "Send error: %s", strerror(errno));
  
  return iRc;
}
