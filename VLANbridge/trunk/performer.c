/* VideoLAN VLANbridge
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 * Arnaud Bienvenu, ECP, <fred@via.ecp.fr>
 *
 * TO DO: Traitement des erreurs plus fin
*/


/* For Solaris */
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <errno.h>
#include <unistd.h>

#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "debug.h"
#include "convert.h"
#include "pinger.h"
#include "arp.h"
#include "route.h"
#include "gratuitous.h"
#include "common.h"
#include "log.h"
#include "performer.h"
#include "manager.h"
#include "cfgfile.h"
#include "define.h"


#define MOD_NAME MOD_PERFORMER


/* Informations on the topology of the network */
int iVLANNumber = 0;
int iVLANDflt = 0;

/* Map VLAN number to the corresponding interface */
char** strMapToDev;
/* Map VLAN number to the corresponding interface IP address */
static struct sockaddr_in* asaMapToIP;
/* Map VLAN number to the corresponding MAC address of our bridge */
static struct sockaddr* asaMapToMAC;

/* Router information */
static struct sockaddr_in saRouterIP;
static struct sockaddr saRouterMAC;

/* To get in touch with the manager thread */
extern struct s_Manager manager;



/***************************************************************************/
/* Setup the MapTo arrays */
/***************************************************************************/
int ConfigPerformer(FILE* hCfgFile)
{
  int iIndex;
  char* strToken;
  int iRc = 0;
  char* strVarName = NULL;
  char* strVarVal = NULL;

  /* Open a socket to read interface configuration */
  int iSockFd = socket(AF_INET, SOCK_DGRAM, 0);

  ASSERT(hCfgFile);

  if (iSockFd < 0)
  {
    iRc = errno;
    Log(LOG_ERROR, MOD_NAME, "Socket error: %s\n", strerror(errno));
  }
  else
  {
    /* Find the right section */
    iRc = FindCfg(hCfgFile, "Interfaces");

    if (iRc == CFG_OK)
    {
      /* Read the number of VLANs handled by the bridge */
      iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);

      if (!iRc && !strcmp(strVarName, "VLANcount"))
      {
	/* Allocate the entries */
	if ((iVLANNumber = atoi(strVarVal)) > 0)
	{
	  strMapToDev = calloc(iVLANNumber+1, sizeof(char *));

	  /* Init them: when not used, an entry points to an empty string */
	  /* Element 0 is not used */
	  strMapToDev[0] = NULL;
	  for (iIndex = 1; iIndex <= iVLANNumber; iIndex++)
	    strMapToDev[iIndex] = "";
	}
	else
	{
	  iRc = 1;
	  Log(LOG_ERROR, MOD_NAME, "VLANcount must be a positive integer");
	}
      }
      else
      {
	iRc = 1;
	Log(LOG_ERROR, MOD_NAME, "VLANcount must be the first entry in the Performer section");
      }

      /* Read the default VLAN */
      if (!iRc)
	iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);

      if (!iRc && !strcmp(strVarName, "VLANdefault"))
      {
	iVLANDflt = atoi(strVarVal);

	if (iVLANDflt <= 0 || iVLANDflt > iVLANNumber)
	{
	  iRc = 1;
	  Log(LOG_ERROR, MOD_NAME, "Invalid value given for VLANdefault");
	}
	else
	  Log(LOG_NOTE, MOD_NAME, "Default VLAN is VLAN %d", iVLANDflt);
      }
      else
      {
	iRc = 1;
	Log(LOG_ERROR, MOD_NAME, "VLANdefault must be the second entry in the Performer section");
      }

      /* Read the entries to fill in the map structure */
      while (iRc == CFG_OK)
      {
	iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);
	  
	if (iRc == CFG_OK)
	{
	  /* Tokenise the string */
	  strToken = strpbrk(strVarName, "1234567890");
	  iIndex = atoi(strToken);

	  /* Set the mapping array */
	  if (iIndex <= iVLANNumber)
	  {
	    strMapToDev[iIndex] = strVarVal;
	    Log(LOG_NOTE, MOD_NAME, "VLAN %d is handled by %s", iIndex, strMapToDev[iIndex]);
	  }
	  else
	  {
	    iRc = CFG_BAD;
	    Log(LOG_ERROR, MOD_NAME, "VLAN %d is upper than VLANcount", iIndex);
	  }
	}
      }
	
      /* Chek that the while loop only ended because the end of the section was reached */
      if (iRc == CFG_END)
	iRc = 0;
      else
	Log(LOG_ERROR, MOD_NAME, "Bad cfg file");
    }
    else
      Log(LOG_ERROR, MOD_NAME, "Section VLANs does not exist in cfg file");
  }

  if (!iRc)
  {
    /* Resolve IP ans MAC adresses of the interfaces */
    asaMapToIP = calloc(iVLANNumber+1, sizeof(struct sockaddr));
    asaMapToMAC = calloc(iVLANNumber+1, sizeof(struct sockaddr));

    /* We won't use element 0 of the arrays */
    //asaMapToMAC[0] = NULL;
    //asaMapToIP[0] = NULL;

    for (iIndex = 1; iIndex < iVLANNumber; iIndex++)
    {
      if (strMapToDev[iIndex] != "")
      {
	/* Get IP of the interface */
	iRc = GetDevPa(strMapToDev[iIndex], (struct sockaddr *)(&asaMapToIP[iIndex]),
                        iSockFd);

	/* Get its MAC address */
	iRc |= GetDevHw(strMapToDev[iIndex], &asaMapToMAC[iIndex], iSockFd);
      }

      /* Stop if there was a problem */
      if (iRc)
	break;
    }
  }

  if (iRc)
    Log(LOG_ERROR, MOD_NAME, "Unable to retrieve all needed informations on the interfaces");

  if (!iRc)

  /* Close the socket */
  if (close(iSockFd))
    Log(LOG_WARN, MOD_NAME, "Unable to close temporary socket: %s", strerror(errno));

  return iRc;
}



/***************************************************************************/
/* Setup the routing tables to suit the bridge needs			   */
/***************************************************************************/
int InitRouting(FILE* hCfgFile)
{
  int iSockFd, iIndex;
  int iRc = 0;
  struct hostent* heInAddr;
  char* strVarName = NULL, *strVarVal = NULL;

  ASSERT(hCfgFile);

  /* Find the right section */
  iRc = FindCfg(hCfgFile, "Router");

  if (iRc == CFG_OK)
  {
    /* Read the IP of the router */
    iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);

    if (!iRc && !strcmp(strVarName, "IP"))
    {
      saRouterIP.sin_family = AF_INET;

      if (inet_aton(strVarVal, &saRouterIP.sin_addr) == 0)
      {
        /* it's not dotted-decimal, try to ask the DNS server */
        if ((heInAddr = gethostbyname(strVarVal)) == NULL)
          iRc = -1;
        else
          bcopy(heInAddr->h_addr, &saRouterIP.sin_addr, heInAddr->h_length);
      }
    }
    else
    {
      iRc = 1;
      Log(LOG_ERROR, MOD_NAME, "IP must be the first entry in the Router section");
    }

    /* Get its MAC address */
    if (!iRc)
      iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);
    if (!iRc && !strcmp(strVarName, "MAC"))
    {
      /* Bug possible, car la conversion ne respecte pas le network byte order */
      iRc = mac_atob(strVarVal, (unsigned char *)&saRouterMAC.sa_data);
    }
    else
    {
      iRc = 1;
      Log(LOG_ERROR, MOD_NAME, "MAC must be the second entry in the Router section");
    }
  }
  else
  {
    iRc = 1;
    Log(LOG_ERROR, MOD_NAME, "No Router section in the Cfg file");
  }

  /* Open a socket to manipulate the routing tables */
  iSockFd = socket(AF_INET, SOCK_DGRAM, 0);
  
  if (iSockFd < 0)
  {
    iRc = errno;
    Log(LOG_ERROR, MOD_NAME, "Could not open temporary socket");
  }
  else
  {
    Log(LOG_NOTE, MOD_NAME, "Adding ARP entries for the router (%s)",
        inet_ntoa(saRouterIP.sin_addr));

    /* Add an ARP entry for the router on each interface except the default one */
    for (iIndex = 1; iIndex < iVLANNumber; iIndex++)
    {
      if (strMapToDev[iIndex] != "" && iIndex != iVLANDflt)
        iRc = Arp_Add(&saRouterIP, strMapToDev[iIndex], iSockFd);

      /* Stop if there was a problem */
      if (iRc)
        break;
    }
  
    /* Close the socket */
    if (close(iSockFd))
      Log(LOG_WARN, MOD_NAME, "Unable to close temporary socket: %s", strerror(errno));
  }
  
  return iRc;
}


/***************************************************************************/
/* Setup the routing tables to suit the bridge needs			   */
/* If not done, only clients known by the bridge (ie which sent a request) */
/* can be join from another VLAN                                           */
/***************************************************************************/
int InitArp(FILE* hCfgFile)
{
  int iIndex, iRc = 0;
  int iSockFd;
  char* strVarName = NULL;
  char* strVarVal = NULL;
  struct sockaddr_in saStartAddr;
  struct sockaddr_in saStopAddr;
  char strStartAddr[17];
  char strStopAddr[17];
  unsigned long lStartAddr;
  unsigned long lStopAddr;
  unsigned long lAddr;

  ASSERT(hCfgFile);

  /* Find the right section */
  iRc = FindCfg(hCfgFile, "Hosts");

  if (iRc != CFG_OK)
    Log(LOG_ERROR, MOD_NAME, "Could not find 'Hosts' section");
  else
  {
    /* Read the IP of the first host */
    iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);

    if (!iRc && !strcmp(strVarName, "Start"))
    {
      bzero(&saStartAddr, sizeof(saStartAddr));
      saStartAddr.sin_family = AF_INET;
      iRc = !inet_aton(strVarVal, &saStartAddr.sin_addr);
      strcpy(strStartAddr, inet_ntoa(saStartAddr.sin_addr));
    }
    else
    {
      iRc = 1;
      Log(LOG_ERROR, MOD_NAME, "Could not read 'Start' entry");
    }

    /* Read the IP of the last host */
    iRc = ReadCfg(hCfgFile, &strVarName, &strVarVal);
    if (!iRc && !strcmp(strVarName, "End"))
    {
      bzero(&saStopAddr, sizeof(saStopAddr));
      saStopAddr.sin_family = AF_INET;
      iRc = !inet_aton(strVarVal, &saStopAddr.sin_addr);
      strcpy(strStopAddr, inet_ntoa(saStopAddr.sin_addr));
    }
    else
    {
      iRc = 1;
      Log(LOG_ERROR, MOD_NAME, "Could not read 'Stop' entry");
    }
  }

  if (!iRc)
  {
    iSockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (iSockFd < 0)
    {
      iRc = errno;
      Log (LOG_ERROR, MOD_NAME, "Unable to open socket: %s", strerror(iRc));
    }
    else
    {
      Log(LOG_NOTE, MOD_NAME, "Adding ARP entries for hosts %s to %s (if needed)",
          strStartAddr, strStopAddr);

      lStartAddr = ntohl(saStartAddr.sin_addr.s_addr);
      lStopAddr = ntohl(saStopAddr.sin_addr.s_addr);

      for (lAddr = lStartAddr; lAddr <= lStopAddr; lAddr++)
      {
        saStartAddr.sin_addr.s_addr = htonl(lAddr);

        for (iIndex = 1; iIndex < iVLANNumber; iIndex++)
        {
          if (strMapToDev[iIndex] != "" && iIndex != iVLANDflt)
            iRc = Arp_Add(&saStartAddr, strMapToDev[iIndex], iSockFd);

          if (iRc)
            break;
        }

        if (iRc)
          break;
      }
    }

    /* Close the socket */
    if (close(iSockFd))
      Log(LOG_WARN, MOD_NAME, "Unable to close temporary socket: %s", strerror(errno));
  }
  
  return iRc;
}  


/***************************************************************************/
/* */
/*	   We always try to keep ARP and route tables synchronised, so we  */
/* test that routing operations were OK before updating the ARP table      */
/*	   If something goes wrong, we try to go back to previous config   */
/***************************************************************************/
int UpdateTables (struct s_Request* psrJob)
{
  int iRc = 0;
  int iSockFd = 0;

  ASSERT(psrJob);

#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Updating bridge config for host %s",
       inet_ntoa(psrJob->saClientIP.sin_addr));
#endif

  /* Open a socket */
  iSockFd = socket (AF_INET, SOCK_DGRAM, 0);
  if (iSockFd < 0)
  {
    iRc = errno;
    Log (LOG_ERROR, MOD_NAME, "Unable to open socket: %s", strerror(iRc));
  }

  /* Delete previous entries */
  if (!iRc)
  {
    /* We never have route to DFLT_VLAN, for we use the default route to forward
       packets to this VLAN */
    if (psrJob->iVLANsrc != iVLANDflt)
      iRc = Route_Del(iSockFd, &psrJob->saClientIP, strMapToDev[psrJob->iVLANsrc]);

    /* !iRc to keep the entries consistent and if VLANsrc == VLANdst, there is not
       entry to delete on VLANdst unless it is the first time */
    if (!iRc && (psrJob->iVLANdst != psrJob->iVLANsrc))
      iRc = Arp_Del(&psrJob->saClientIP, strMapToDev[psrJob->iVLANdst], iSockFd);
  }
  
  /* If both entries where deleted or none existed, add the new ones */
  if (!iRc)
  {
    if (psrJob->iVLANdst != iVLANDflt)
      iRc = Route_Add(iSockFd, &psrJob->saClientIP, strMapToDev[psrJob->iVLANdst]);

    if (iRc)
    {
      /* Error: Cancel all modifications */
      if (psrJob->iVLANdst != psrJob->iVLANsrc)
        iRc |= Arp_Add(&psrJob->saClientIP, strMapToDev[psrJob->iVLANdst], iSockFd);
      if (psrJob->iVLANsrc != iVLANDflt)
        iRc |= Route_Add(iSockFd, &psrJob->saClientIP, strMapToDev[psrJob->iVLANsrc]);
    }
    else
    {
      /* Update ARP table
         If VLANdst == VLANsrc, we won't proxy for the host on the VLAN on which
         it stays, so don't add any entry */
      if (psrJob->iVLANdst != psrJob->iVLANsrc)
        iRc = Arp_Add(&psrJob->saClientIP, strMapToDev[psrJob->iVLANsrc], iSockFd);

      if (iRc)
      {
	/* Error: cancel all modifications */
        if (psrJob->iVLANdst != psrJob->iVLANsrc)
          iRc = Arp_Add(&psrJob->saClientIP, strMapToDev[psrJob->iVLANdst], iSockFd);
        if (psrJob->iVLANdst != iVLANDflt)
          iRc |= Route_Del(iSockFd, &psrJob->saClientIP, strMapToDev[psrJob->iVLANdst]);
        if (psrJob->iVLANsrc != iVLANDflt)
          iRc |= Route_Add(iSockFd, &psrJob->saClientIP, strMapToDev[psrJob->iVLANsrc]);
      }      
    }
  }
  
  return iRc;
};


/***************************************************************************/
/* */
/***************************************************************************/
int WarnNetwork(const struct s_Request* psrJob)
{
  int iRc = 0;
//  int iVLAN = 0; 

  ASSERT(psrJob);
  
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Sending the requested gratuitous ARP packets");
#endif

  /* Tell the client how to reach the VLANbridge */
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "The bridge, it's now me on VLAN %d",
       psrJob->iVLANdst);
#endif
  iRc = Gratuitous_Send (&asaMapToIP[psrJob->iVLANdst],
                         &asaMapToMAC[psrJob->iVLANdst],
                         strMapToDev[psrJob->iVLANdst]);
  
  /* Tell the client how to reach the router */
  if (psrJob->iVLANdst != iVLANDflt)
  {
#ifdef DEBUG
    Log (LOG_NOTE, MOD_NAME, "The router, it's now me on VLAN %d",
         psrJob->iVLANdst);
#endif
   iRc = Gratuitous_Send (&saRouterIP, &asaMapToMAC[psrJob->iVLANdst],
                          strMapToDev[psrJob->iVLANdst]);
  }
  else
  {
#ifdef DEBUG
    Log (LOG_NOTE, MOD_NAME, "The router, it's no longer me on VLAN %d",
         psrJob->iVLANdst);
#endif
    iRc = Gratuitous_Send(&saRouterIP, &saRouterMAC,
                          strMapToDev[psrJob->iVLANdst]);
  };

  /* Tell the computers on the VLANdst they now more need the VLANbridge to
     reach the client since it is now on the same VLAN.
     Not needed if VLANsrc == VLANdst */
  if (psrJob->iVLANsrc != psrJob->iVLANdst)
  {
#ifdef DEBUG
    Log (LOG_NOTE, MOD_NAME, "The client, it's no longer me on VLAN %d",
         psrJob->iVLANdst);
#endif

    iRc = Gratuitous_Send(&psrJob->saClientIP, &psrJob->saClientMAC,
                          strMapToDev[psrJob->iVLANdst]);
  }
  
  /* Tell the computers on the VLANsrc they now need the VLANbridge to
     reach the client since it is no more on the same VLAN.
     Not needed if VLANsrc == VLANdst */
  if (psrJob->iVLANsrc != psrJob->iVLANdst)
  {
#ifdef DEBUG
    Log (LOG_NOTE, MOD_NAME, "The client, it's now me on VLAN %d", psrJob->iVLANsrc);
#endif
    iRc |= Gratuitous_Send (&psrJob->saClientIP, &asaMapToMAC[psrJob->iVLANsrc],
                            strMapToDev[psrJob->iVLANsrc]);
  };

  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
#define psrJob ((struct s_Request *) pvArg)

void* PerformerThread (void* pvArg)
{
  int iHasMoved = 1;
  struct s_Pinger spPingerCfg;
  int iRc = 0;

  ASSERT(pvArg);
  
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Performer thread %d is running", psrJob->ptId);
  //sleep(20);
#endif
  
  /* Init the pinger */
  if ((iRc = Pinger_Init(&psrJob->saClientIP, &spPingerCfg)))
  {
    psrJob->iStatus = VLAN_ERROR;
    Log (LOG_ERROR, MOD_NAME, "Could not init pinger for %s: aborting",
         inet_ntoa(psrJob->saClientIP.sin_addr));
  }

  /* Synchronise with the VLAN change (unless Misconfiguration was
     detected, since it could lead to pbs in detection) */
  else if (!psrJob->iMisconfig &&
           (iRc = Pinger_DetectMove(&spPingerCfg, &iHasMoved)))
  {
    psrJob->iStatus = VLAN_ERROR;
    Log (LOG_ERROR, MOD_NAME, "Could not ping %s: aborting",
         inet_ntoa(psrJob->saClientIP.sin_addr));
  }

  /* Check that the client has moved before updating the bridge config
     Only rely on that if no misconfiguration was detected */
  else if (iHasMoved == 0)
  {
    Log (LOG_WARN, MOD_NAME, "%s has not moved: aborting",
         inet_ntoa(psrJob->saClientIP.sin_addr));
    psrJob->iStatus = VLAN_ROUTE_NOMOVE;
  }

  /* Update the bridge tables */
  else
  {
//    sleep(5);
    
    /* Change routing and ARP tables
       This process is done in order to try to repare misconfigurations */
    iRc = UpdateTables(psrJob);

    if (iRc)
    {
      psrJob->iStatus = VLAN_ERROR;
      Log (LOG_WARN, MOD_NAME, "Tables modif failed for client %s",
           inet_ntoa(psrJob->saClientIP.sin_addr));
    }
    else
    {
      /* Send the requested gratuitous ARP packets */
      if ((iRc = WarnNetwork (psrJob)))
      {
	/* This is not really important if we can ping the client, so just
	   log the error */
	Log (LOG_WARN, MOD_NAME, "Could not warn other computers of %s's move",
            inet_ntoa(psrJob->saClientIP.sin_addr));
      }

      /* Check everything is ok with the client */
      if (Pinger_CheckMove(&spPingerCfg, &iHasMoved))
      {
	iRc = iRc | -1;
	psrJob->iStatus = VLAN_ERROR;
	Log (LOG_WARN, MOD_NAME, "Could not ping %s: Don't know if move was OK",
            inet_ntoa(psrJob->saClientIP.sin_addr));
      }
      else if (iHasMoved == 1)
      {
	psrJob->iStatus = VLAN_ROUTE_OK;
	Log (LOG_NOTE, MOD_NAME, "Request from %s successfully processed",
             inet_ntoa(psrJob->saClientIP.sin_addr));
      }
      else
      {
	psrJob->iStatus = VLAN_ROUTE_KO;	
	Log (LOG_WARN, MOD_NAME, "Host %s lost",
             inet_ntoa(psrJob->saClientIP.sin_addr));
      }
    }
  }

  /* Destroy the pinger */
  iRc |= Pinger_Destroy (&spPingerCfg);

  /* Warn the manager */
  iRc |= WarnManager(&manager, psrJob, CLEAN_PERFORMER);

  /* Exit */
#ifdef DEBUG
  Log (LOG_NOTE, MOD_NAME, "Performer thread %d exiting", psrJob->ptId);
#endif
  pthread_exit (NULL);    /* Exit the thread returning the NULL value */
};

#undef pspConfig



/***************************************************************************/
/* */
/***************************************************************************/
int RunPerformer (struct s_Request* psrPerfId)
{
  int iRc;
  ASSERT(psrPerfId);

  /* Create the thread */
  iRc = pthread_create (&psrPerfId->ptId, NULL, PerformerThread, psrPerfId);

  if (iRc)
    Log (LOG_ERROR, MOD_NAME, "Unable to create performer thread: error %s",
         strerror(iRc));

#ifdef DEBUG
  else
    Log (LOG_NOTE, MOD_NAME, "Performer thread successfully created with id %d",
         psrPerfId->ptId);
#endif

  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
int FreePerformer (const struct s_Request* psrPerfId)
{
  int iRc;
  ASSERT(psrPerfId);

  iRc = pthread_join(psrPerfId->ptId, NULL);
  
  if (iRc)
    Log (LOG_NOTE, MOD_NAME, "Cannot join thread %d: %s",
         psrPerfId->ptId, strerror(iRc));

#ifdef DEBUG
  else
    Log (LOG_NOTE, MOD_NAME, "Performer thread (id %d) destroyed",
         psrPerfId->ptId);
#endif

  return iRc;
}
