/* VideoLAN VLANbridge : Kernel routing tables management
 *
 * Benoit Steiner, VIA, ECP, <benny@via.ecp.fr>
 *
 * TO DO: With ioctl request, we just can send route_add or route_del command. We should use the routing socket, but linux just support read operations at that time (see TCP/IIP Illustrated p 569-570)
*/


//#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/route.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>

#include "../log.h"
#include "route.h"

/* VLANbridge module name */
#define MOD_NAME MOD_PERFORMER


/***************************************************************************/
/* Add a route in the routing table 					   */
/***************************************************************************/
int Route_Add (int iSockFd, struct sockaddr_in* psaAddr, struct sockaddr_in* psaNetMask, char* strDev)
{
  int iRc = 0;
  struct rtentry rteRequest;

  bzero(&rteRequest, sizeof(struct rtentry));

  /* Set the flags associated to the entry */
  rteRequest.rt_flags = RTF_UP;			/* Interface is up */
  rteRequest.rt_flags |= RTF_STATIC;		/* Route was entered by hand */
  rteRequest.rt_flags |= RTF_HOST;		/* Direct route (no gateway) */
  rteRequest.rt_metric = 0;			/* Cost of the route */

  /* Set the host the route points to */
  memcpy(&rteRequest.rt_dst, psaAddr, sizeof(struct sockaddr));
  /* Set the netmask */
  memcpy(&rteRequest.rt_genmask, psaNetMask, sizeof(struct sockaddr));
  /* Set the device used to reach the destination */
  rteRequest.rt_dev = strDev;

  /* Add the route to the routing table */
  if (ioctl(iSockFd, SIOCADDRT, &rteRequest) < 0)
  {
    Log (MOD_NAME, LOG_WARN, "Unable to add route to host %s on interface %s: %s", inet_ntoa(psaAddr->sin_addr), strDev, strerror(errno));
    iRc = errno;;
  }

  return iRc;
};


/***************************************************************************/
/* Remove a route from the routing table 				   */
/***************************************************************************/
int Route_Del (int iSockFd, struct sockaddr_in* psaAddr, char* strDev)
{ 
  int iRc = 0;
  struct rtentry rteRequest;

  bzero(&rteRequest, sizeof(struct rtentry));

  /* Set the flags associated to the entry */
  rteRequest.rt_flags = RTF_UP;			/* Interface is up */
  rteRequest.rt_flags |= RTF_STATIC;		/* Route was entered by hand */
  rteRequest.rt_flags |= RTF_HOST;		/* Direct route (no gateway) */
  rteRequest.rt_metric = 0;			/* Cost of the route */

  /* Set the host the route points to */
  memcpy(&rteRequest.rt_dst, psaAddr, sizeof(struct sockaddr));
  /* Set the device used to reach the destination */
  rteRequest.rt_dev = strDev;

  /* Remove the route from the routing table */
  if (ioctl(iSockFd, SIOCDELRT, &rteRequest) < 0)
  {
    Log (MOD_NAME, LOG_WARN, "Unable to remove route to host %s: %s", inet_ntoa(psaAddr->sin_addr), strerror(errno));
    iRc = errno;;
  }

  return iRc;
};

