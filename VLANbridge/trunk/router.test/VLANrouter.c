/* VideoLAN router
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
*/

#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "route.h"
#include "../log.h"


int main ()
{ 
  char host[] = "tiny.via.ecp.fr";

  struct sockaddr_in target;
  struct sockaddr_in netmask;
//   struct sockaddr *targ; 
  unsigned long inaddr;
  struct hostent *hp; 
  int sockfd;

  bzero (&target, sizeof (struct sockaddr_in));
  target.sin_family = AF_INET;

  if ( (inaddr = inet_addr(host)) != INADDR_NONE)
  { 
    /* it's dotted-decimal */
    bcopy(&inaddr, &target.sin_addr, sizeof(inaddr));
  }
  else
  {
    if ( (hp = gethostbyname(host)) == NULL)
      return -1;
    bcopy(hp->h_addr, &target.sin_addr, hp->h_length);
  };
  
  bzero (&netmask, sizeof (struct sockaddr_in));
  netmask.sin_family = AF_INET;
  netmask.sin_addr.s_addr = (unsigned long)-1;  /* 255.255.255.255 */


  printf("1\n");

  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) <0)
  {
    printf ("Can't open socket\n");
    exit(-1);
  };


  printf("2\n");
  Route_Add (sockfd, &target, &netmask, "lo");
  printf("3\n");

  sleep(10);

  Route_Del (sockfd, &target, "lo");
  printf("4\n");

  close (sockfd);

  return 0;
};
