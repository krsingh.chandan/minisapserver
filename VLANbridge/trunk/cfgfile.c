/* VideoLAN VLANbridge: Config files parsing
 *
 * Benoit Steiner, ECP, <benny@via.ecp.fr>
 *
 * Beware: Not completly thread safe
 *         Buffer overflow are possible
 *
 * File format must be:
 *  BEGIN section
 *    VarName1 = VarVal1
 *    ...
 *    VarNameX = VarValX
 *  END
 * Comments must follow a # char
 *
 * TO DO: 
*/


#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "debug.h"
#include "log.h"
#include "cfgfile.h"


#define MOD_NAME MOD_VLANBRIDGE

/* Maximum size allowed for a line in the file */
#define LINE_MAX_SIZE 256



/***************************************************************************/
/* */
/***************************************************************************/
FILE* OpenCfg (const char* strFileName)
{
  /* Open file for read */
  FILE* pfdCfgFile = fopen (strFileName, "r");

  ASSERT(strFileName);
  
  if (!pfdCfgFile)
  {
    /* Error when opening the file */
    Log (LOG_ERROR, MOD_NAME, "Unable to open the %s config file: %s",
         strFileName, strerror (errno));
  }
  
  return pfdCfgFile;
}


/***************************************************************************/
/* */
/***************************************************************************/
int ReadCfg (FILE* pfdCfgFile, char** pstrVarName, char** pstrVarVal)
{
  int iRc = 0, iStop = 0, iStrBeg = 0;
  char strBuff[LINE_MAX_SIZE];
  char strName[LINE_MAX_SIZE];
  char strValue[LINE_MAX_SIZE];
  char* pcPosInLine = NULL;

  ASSERT(pfdCfgFile);
  ASSERT(pstrVarName);
  ASSERT(pstrVarVal);

  /* Read until one useful line was found or an error occured */
  while (!iStop)
  {
    /* Do not loop unless we explicitly ask for that */
    iStop = 1;
    
    /* Reset the variable storing the error code returned by fgets */
    clearerr(pfdCfgFile);

    /* Read the current line */
    pcPosInLine = fgets(strBuff, LINE_MAX_SIZE, pfdCfgFile);

    if (pcPosInLine == NULL)
    {
      if (ferror(pfdCfgFile) != 0)
      {
	/* Error reading the file */
	Log (MOD_NAME, LOG_WARN, "Error when reading config file");
	iRc = CFG_ERR;
      }
      else
	/* EOF was reached */
	iRc = CFG_EOF;
    }
    else
    {
      /* Erase the final char (that comes from the fgets function) */
      if ((pcPosInLine = (char *)strchr(strBuff, '\n')))
      	*pcPosInLine = '\0';

      /* Erase comments */
      if ((pcPosInLine = (char *)strchr(strBuff, '#')))
      	*pcPosInLine = '\0';

      /* Jump empty lines */      
      if (!strcmp(&strBuff[iStrBeg], ""))
      {
	iStop = 0;
	continue;
      }      

      /* Extract information from the line */
      iRc = sscanf(&strBuff[iStrBeg], "%s %s", strName, strValue);
      
      if (iRc < 0)
      	iRc = CFG_BAD;
      else
      {
	if (!strcmp(strName, "BEGIN") && iRc == 2)
	{
	  *pstrVarName = strdup(strName);
	  *pstrVarVal = strdup(strValue);
	  iRc = CFG_BEGIN;
	}
	else if (!strcmp(strName, "END") && iRc == 1)
	{
	  *pstrVarName = strdup(strName);
	  iRc = CFG_END;
	}
	else
	{
	  if (sscanf(&strBuff[iStrBeg], "%s %*c %s", strName, strValue) == 2)
	  {
	    *pstrVarName = strdup(strName);
	    *pstrVarVal = strdup(strValue);
	    iRc = CFG_OK;
	  }
	  else
	  {
	    Log(LOG_ERROR, MOD_NAME, "Error at offset %d", ftell(pfdCfgFile));	    
	    iRc = CFG_BAD;
	  }
	}
      }
    }
  }

  return iRc;
}


/***************************************************************************/
/* Find a section in the file and go to the first variable of the section  */
/* TO DO */
/***************************************************************************/
int FindCfg(FILE* pfdCfgFile, const char* strSection)
{
  int iRc = CFG_OK;
  char* strVarName = NULL;
  char* strVarVal = NULL;

  ASSERT(pfdCfgFile);
  ASSERT(strSection);
  
  while (iRc == CFG_OK || iRc == CFG_BEGIN || iRc == CFG_END)
  {
    iRc = ReadCfg(pfdCfgFile, &strVarName, &strVarVal);

     if (iRc == CFG_BEGIN && !strcmp(strVarName, "BEGIN") && !strcmp(strVarVal, strSection))
    {
      iRc = CFG_OK;
      break;
    }
  }
 
  return iRc;
}


/***************************************************************************/
/* */
/***************************************************************************/
int CloseCfg (FILE* pfdCfgFile)
{
  int iRc = 0;

  ASSERT(pfdCfgFile);
  
  if (fclose(pfdCfgFile) != 0)
  {
    Log (LOG_ERROR, MOD_NAME, "Unable to close the %s config file: %s",
         "XXX", strerror (errno));
    iRc = errno;
  }
  
  return iRc;
}
