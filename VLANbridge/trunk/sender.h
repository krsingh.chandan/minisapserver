/* VideoLAN VLANbridge: Sender module
 *
 * Definition file
 *
 * You need to include <pthread.h> before including this file
 */


#ifndef _SENDER
#define _SENDER


struct s_Sender
{ /* thread id */
  pthread_t tid;

  /* Handle the alone sockfd used to communicate with the VLANserver */
  int iSockFd;

  /* Signal used to wake up the sender when the bridge is connected to VLANserver */
  pthread_cond_t WakeUpSignal;

  /* Used with the following signal */
  pthread_mutex_t SendLock;
  /* Signal used by manager to tell it it has something to send */
  pthread_cond_t SendSignal;

  /* FIFO buffering messages to be sent */
  struct s_Fifo* psfWaitingAnswers;

  /* Buffer protected by must_send_lock */
  struct s_Request srBuffer;
};

int InitSender (struct s_Sender* pssSender);
void* SenderThread (void* pvArg);
int WarnSender(struct s_Sender* pssSender, struct s_Request* psrJob);
int CancelSender (struct s_Sender* pssSender);
int FreeSender (struct s_Sender* pssSender);


#endif
