/*****************************************************************************
 * vlcs.c: mini-VideoLAN Channel Server
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: vlcs.cc,v 1.3 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <syslog.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#include <getopt.h>
#include <iostream>
#include <list>
#include <vector>  
#include <fstream>
#include <string>
#include <map>
#include <utility>


#include "host.h"
#include "vlcs.h"
#include "logger.h"
#include "config-common.h"
#include "config-txt.h"

#ifdef XERCES
class DOMNode;
#include "config-xml.h"
#endif

#include "socket.h"

#define VLCS_OK 0
#define VLCS_NOTFOUND 1
#define VLCS_PROTECTED 2
#define VLCS_FAILED 3

#define COPYRIGHT \
    "VideoLAN Mini Channel Server - version " VERSION " - (c) 2001-2002 VideoLAN\n"

#define HELP \
    "Usage : %s [OPTION]\n" \
    COPYRIGHT \
    "\n" \
    "Options :\n" \
    "  -c, --config-file <filename>     changes the configuration file\n"\
    "  -h, --help                       display this help and exit\n"\
    "  -4, --ipv4                       force ipv4\n"\
    "  -6, --ipv6                       force ipv6\n"\
    "  -b, --bind <addr>                bind to address : <addr>\n"\
    "  -p, --port <port>                bind to port : <port>\n"\
    "  -v, --version                    output version information and exit\n"\
    ""

/* Long options*/
static const struct option longopts[] =
{
    /* name,            has_arg, flag,  val */
    { "version",        0,      0,      'v' },
    { "help",           0,      0,      'h' },
    { "config-file",    1,      0,      'c' },
    { "ipv4",           0,      0,      '4' },
    { "ipv6",           0,      0,      '6' },
    { "bind",           1,      0,      'b' },
    { "port",           1,      0,      'p' },
    { 0,                0,      0,       0  }
};

/* Short options */
static const char *shortopts = "vhc:46b:p:";

static C_Socket * ip_socket = NULL;
static C_Config * config = NULL;
static C_ChainedLogger * logger;

void Loop( C_Logger &logger ) throw (int)
{
    fd_set sockets;
    struct timeval timeout;
    
    FD_ZERO( &sockets );
    FD_SET( ip_socket->i_handle, &sockets );
    FD_SET( 0, &sockets );

    timeout.tv_usec = 0;
    timeout.tv_sec = 1;

    if( select( ip_socket->i_handle + 1, &sockets, NULL, NULL, &timeout ) < 0 )
    {
        if( errno == EINTR )
        {
            return;
        }
        logger << C_Logger::ALERT << "Cannot select (" << strerror(errno) << "), exiting" 
                << C_Logger::END;
        throw( -1 );
    }

    config->CheckUpdate();
    
    if( FD_ISSET( 0, &sockets ) )
    {
        /* Administration socket */
        char p_buffer[128];
        if( read( 0, p_buffer, sizeof( p_buffer ) ) < 0 )
        {
            if( errno == EINTR )
            {
                return;
            }
            logger << C_Logger::ALERT << "Duh my controlling terminal has gone fishing" << C_Logger::END;
        }
        else if( p_buffer[0] == 'q' )
        {
            /* Quit */
            logger << C_Logger::ALERT << "Exiting now on admin's request" << C_Logger::END;
            throw( 0 );
        }
    }

    if( FD_ISSET( ip_socket->i_handle, &sockets ) )
    {
        /* Client request */
        std::string buffer;
        int i_len, i_version, i_time, i_channel;
        char psz_macad[18];
        char psz_channel[3];
        char * psz_tmp;
        std::string channel;

        if( (i_len = ip_socket->Receive( buffer )) < 0 )
        {
            if( errno == EINTR )
            {
                return;
            }
            logger << C_Logger::ERROR << "Cannot recvfrom :" << strerror(errno) << C_Logger::END;
            return;
        }

        if( sscanf( buffer.c_str(), "%2s %d %d %17s", psz_channel, &i_version,
                    &i_time, psz_macad ) != 4 )
        {
            ip_socket->Reply( "E: Bad request" );
            logger << C_Logger::ERROR << "sscanf failed : " << buffer << C_Logger::END;
            return;
        }
        
        i_channel = strtol( psz_channel, &psz_tmp, 0 );
        if (!config->isAllowed(ip_socket->Ntop(),i_channel))
        {
            return;
        }
        
        if ( i_version < 13 )
        {
            ip_socket->Reply( "E: Version mismatch, please upgrade your software" );
            logger << C_Logger::ALERT << "Break-in attempt ! " 
                    << ip_socket->Ntop() << C_Logger::END;
            return;
        }
        
        if( psz_tmp[0] != '\0' || i_channel < 0 ||
            i_channel >= config->GetChannelNumber() )
        {
            ip_socket->Reply( "E: Channel doesn't exist" );
            logger << C_Logger::ERROR << "Bad channel : " << psz_channel 
                    << C_Logger::END;
            return;
        }

        channel = config->GetChannel( i_channel );
        if( channel == "" )
        {
            logger << C_Logger::ERROR << "Bad channel : " << psz_channel 
                    << C_Logger::END;
            ip_socket->Reply( "E: Channel doesn't exist" );
            return;
        }
        ip_socket->Reply( channel );
    }
}

void Close( int i_ret )
{
    if( ip_socket != NULL )
    {
        delete ip_socket;
    }
    if( config != NULL )
    {
        delete config;
    }
    if( logger != NULL )
    {
        delete logger;
    }

    exit( i_ret );
}

int main( int argc, char ** argv )
{
    signal(SIGINT, Close);
    
    try 
    {
        logger = new C_ChainedLogger();
#ifdef XERCES
        config = new C_ConfigXml(*logger);
#else
        config = new C_ConfigTxt(*logger);
#endif
    
#ifdef LOG_FILE
        logger->Add(new C_FileLogger(LOG_FILE));
#endif

#ifdef LOG_STDOUT
#   if LOG_STDOUT == LINUX_COLORS
        logger->Add(new C_StdLogger(C_FileLogger::LINUX));
#   else
        logger->Add(new C_StdLogger(C_FileLogger::NO_COLOR));
#   endif
#endif

#ifdef SYSLOG_NAME
        logger->Add(new C_SysLogger(SYSLOG_NAME, SYSLOG_FACILITY));
#endif
    
   
        config->setSocketPort(VLCS_PORT);
#ifdef IPV6
        config->setInetType(C_Config::IPV6);
#else
        config->setInetType(C_Config::IPV4);
#endif
        
#ifdef BIND_ADDRESS
        config->setBindAddr(BIND_ADDRESS);
#endif
        
        int opt;
        opterr = 0;
        
        while((opt = getopt_long( argc, argv, shortopts, longopts, 0)) != EOF)
        {
            switch(opt)
            {
            case 'c':
                config->SetConfigFile(optarg);
                break;        
            case '4':
                config->setInetType(C_Config::IPV4);
                break;
            case '6':
                config->setInetType(C_Config::IPV6);
                break;
            case 'b':
                config->setBindAddr(optarg);
                break;
            case 'p':
                config->setSocketPort(atoi(optarg));
                break;
            case 'h':
                fprintf(stderr, HELP, *argv);
                throw( 0 );
                break;
            case 'v':
                fprintf(stderr, COPYRIGHT);
                throw( 0 );
                break;
            case '?':
                fprintf(stderr,"%s: invalid option or missing parameter -- %c\n"
                        "Try `%s --help' for more information.\n",
                        *argv, optopt, *argv);
                throw( 1 );
                break;
            }
        }
    
        config->ReadFile();
        
        if (config->getInetType() == C_Config::IPV6)
        {
#ifdef IPV6_SUPPORT
            if (!config->getBindAddr().empty())
            {
                ip_socket = new C_IPv6Socket(*logger, config->getSocketPort(), 
                        config->getBindAddr());
            }
            else
            {
                ip_socket = new C_IPv6Socket(*logger, config->getSocketPort()); 
            }
#else
            fprintf(stderr,"error No IPv6 support compiled");
#endif
        }
        else
        {
            if (!config->getBindAddr().empty())
            {
                ip_socket = new C_IPv4Socket(*logger,
                        config->getSocketPort(), config->getBindAddr());
            }
            else
            {
                ip_socket = new C_IPv4Socket(*logger, config->getSocketPort()); 
            }
        }
       
        for( ; ; )
        {
            Loop(*logger);
        }
    }
    catch (int i_ret)
    {
        Close( i_ret );
    }

    return( 0 );
}
