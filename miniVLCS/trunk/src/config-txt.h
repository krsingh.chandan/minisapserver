/*****************************************************************************
 * config.c: mini-VideoLAN Channel Server, configuration system
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: config-txt.h,v 1.2 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/


/**
 * \brief Configuration system using a plain text file
 *
 * This class features a simple authorization system.
 *
 * Authorization only works with IPv4
 * 
 * The configuration files specifies a list of hosts or subnets,
 * with the corresonding policy (ALLOW/DENY)
 * 
 * \see C_ConfigXml
 */
class C_ConfigTxt : public C_Config
{
public:
    
    /**
     * \brief creates a C_ConfigTxt with the specified logging system
     */
    C_ConfigTxt(C_Logger &logger);
    
    /**
     * \brief class destructor
     */
    virtual ~C_ConfigTxt();

    /**
     * \brief returns the vlc string of the specified channel
     */
    virtual std::string GetChannel( int i_channel );

    /**
     * \brief returns the number of channels
     */
    virtual int GetChannelNumber( void ); 

    /**
     * \brief Force the object to read its configuration file
     */
    virtual void ReadFile( void );

    /**
     * \brief Asks whether the specified client is allowed to
     * watch this channel or not
     *
     * If no configuration is precised for the client/channel pair,
     * then the default policy is to allow.
     */
    virtual bool isAllowed(std::string client, int channel);

protected:
    /**
     * \brief destroys the channel list
     */
    void DestroyList( void );

    /**
     * \brief adds a channel to the list
     */
    void AddList( std::string item );
 
    /**
     * \brief returns the
     */
    std::list<C_Host *> &getHostsAuth();

    
    /**
     * \brief number of valid channels available 
     */
    int i_channel_max;      

    /**
     * \brief The list of channels
     */
    std::vector<std::string> channels;

    /**
     * \brief list of hosts/subnets with their corresponding policy
     */
    std::list<C_Host *> h_hosts;
    
};
