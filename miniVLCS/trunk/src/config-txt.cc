/*****************************************************************************
 * config.c: mini-VideoLAN Channel Server, configuration system
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: config-txt.cc,v 1.2 2002/12/20 15:53:59 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <syslog.h>
#include <errno.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#include <vector> 
#include <list> 
#include <iostream>                                                             
#include <fstream>
#include <string>

#include "host.h"
#include "vlcs.h"
#include "logger.h"
#include "config-common.h"
#include "config-txt.h"

///////
C_Config::C_Config(C_Logger &logger) : logger(logger)
{
}

C_Config::~C_Config()
{
}

    
C_ConfigTxt::C_ConfigTxt(C_Logger &logger) 
    : C_Config(logger), channels(12)
{
    socket_port = 0;
    socket_type = IPV4;
    configFile = CHANNEL_CONF_TXT;
}

C_ConfigTxt::~C_ConfigTxt()
{
    DestroyList();
}

/*
 *  Destroy the linked list
 */
void C_ConfigTxt::DestroyList( void )
{
    channels.clear();
    i_channel_max = 0;
    h_hosts.clear();

    logger << C_Logger::INFO << "List of channels cleared" << C_Logger::END;
}

char * C_Config::RemoveLF( char * str )
{
    int i_pos = 0;

    while( str[i_pos] )
    {
        if( str[i_pos] == '\n' )
        {
            str[i_pos] = ' ';
        }
        i_pos++;
    }
    return str;
}

/*
 * Adds the item to the list
 */
void C_ConfigTxt::AddList( std::string item )
{
    channels.push_back(item);
    //RemoveLF( p_new->psz_vlc_config);
    i_channel_max++;
    logger << C_Logger::INFO << "Adding channel " << i_channel_max << " : " 
             << item << C_Logger::END;
}

void C_ConfigTxt::ReadFile( void )
{
    char psz_vlc_command[2048];
    char psz_buffer[2048];
    FILE * p_config;
    int config_type = 0;
    
    DestroyList();

    mtime = GetModificationTime();
    
    p_config = fopen( configFile.c_str(), "r" );
    if( p_config == NULL )
    {
        logger << C_Logger::ALERT << "Cannot fopen " << configFile 
                << " : "<< strerror(errno) << C_Logger::END;
        exit( -1 );
    }

    i_channel_max = -1;

    while( fgets( psz_buffer, sizeof( psz_buffer ), p_config ) != NULL )
    {
        if( psz_buffer[0] != '#' 
                && psz_buffer[0] != '\0' 
                && psz_buffer[0] != '\n' 
                && psz_buffer[0] != ' ' )
        {
            std::string * command;
            sscanf(psz_buffer, "%s", psz_vlc_command);
            command = new std::string(psz_vlc_command);
            if (!command->compare("@auth"))
            {
                config_type = 1;
                continue;
            }

            if (config_type == 0)
            {
                AddList(*command);
            } 
            else if (config_type == 1)
            {
                C_Host * h_host = new C_Host(*command);
                h_hosts.push_back(h_host);
                logger << C_Logger::INFO << h_host->getStr() << C_Logger::END;
            }
        }
    }

    fclose( p_config );
}

std::string C_ConfigTxt::GetChannel( int i_channel )
{
    if ( i_channel >= i_channel_max)
    {
        return *new std::string("E:channel does not exist.");
    }
    return channels[i_channel];
}

int C_ConfigTxt::GetChannelNumber( void )
{
    return i_channel_max + 1;
}

////

std::list<C_Host *> &C_ConfigTxt::getHostsAuth() 
{ 
    return h_hosts; 
}


bool C_ConfigTxt::isAllowed(std::string client, int channel)
{
    in_addr_t client_addr = C_Host::pton(client);
    
    std::list<C_Host *>::iterator host = h_hosts.begin();
    while (host != h_hosts.end())
    {
        if ((*host)->test(client_addr))
        {
            if ((*host)->getType() == C_Host::ALLOW)
            {
                return true;
            }
            else
            {
                logger << C_Logger::WARNING << "Break in attempt from " 
                    << client << C_Logger::END;
                return false;
            }
        }
        ++host;
    }
        
    return true;
}

///////////////////////////////////////////////////////////:
// C_Config class implementation

void C_Config::SetConfigFile(std::string new_config_file)
{
    configFile = new_config_file;
}

////

void C_Config::setInetType(inet_type t) 
{ 
    socket_type = t; 
}

C_Config::inet_type C_Config::getInetType() 
{ 
    return socket_type; 
}

////

void C_Config::setSocketPort(int port)  
{ 
    socket_port = port; 
}

int C_Config::getSocketPort() 
{ 
    return socket_port; 
}

////

void C_Config::setBindAddr(std::string addr) 
{ 
    bindAddr = addr; 
}

std::string C_Config::getBindAddr() 
{ 
    return bindAddr; 
}

time_t C_Config::GetModificationTime( void )
{
    struct stat buf;

    stat( configFile.c_str(), &buf);

    return buf.st_mtime;
}

void C_Config::CheckUpdate()
{
    if( mtime != GetModificationTime() )
    {
        ReadFile();
    }
}

