/*****************************************************************************
 * config-xml.h: mini-VideoLAN Channel Server, configuration system
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: config-xml.h,v 1.2 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/**
 * \brief Encapsulates a channel description
 *
 * \see C_ConfigXml
 */
class C_Channel
{
public:
    /**
     * \brief enumeration to define the value of authorization type
     */
    typedef enum 
    { 
        ALLOW, /**<  corresponding hosts are allowed to see this channel */
        DENY   /**< corresponding hosts are not allowed to see this channel */
    } type;

    /**
     * \brief creates an empty channel
     */
    C_Channel();

    /**
     * \brief Creates a channel using a part of an XML tree
     */
    C_Channel(DOMNode * node);

    /**
     * \brief destroy the channel object
     */
    ~C_Channel();

    /**
     * \brief The name of the channel
     *
     * This is a clear description for the user.
     */
    std::string name;

    /**
     * \brief corresponding vlc playlist entry
     *
     * This is in the vlc playlist format :
     * 
     * for example "udp:@239.255.12.42" or "http://192.168.12.42/foobar.mpg"
     */
    std::string vlc_string;

    /**
     * \brief Authorization description for this channel
     *
     * It is a list of groups and with each group the associated policy.
     * Groups are indexed using their name.
     *
     * It is not specified here how groups are created and what they represent
     */
    std::list<std::pair<std::string, type> > auth;
};

/**
 * \brief Configuration system using an XML configuration file
 *
 * This class features a more complex authorization system.
 *
 * Authorization only works with IPv4
 *
 * First groups are defined. Groups are a list of hosts or subnets.
 * Hosts and subnets are defined the same way, except that hosts
 * have a subnet mask of 255.255.255.255
 *
 * \see C_ConfigTxt
 */
class C_ConfigXml : public C_Config
{
public:

    /**
     * \brief creates a C_ConfigXml with the specified logging system
     */
    C_ConfigXml(C_Logger &logger);

    /**
     * \brief class destructor
     */
    virtual ~C_ConfigXml();
    
    /**
     * \brief returns the vlc string of the specified channel
     */
    virtual std::string GetChannel( int i_channel );

    /**
     * \brief returns the number of channels
     */
    virtual int GetChannelNumber( void ); 

    /**
     * \brief Force the object to read its configuration file
     */
    virtual void ReadFile( void );

    /**
     * \brief Asks whether the specified client is allowed to
     * watch this channel or not.
     *
     * If no configuration is precised for the client/channel pair,
     * then the default policy is to allow.
     */
    virtual bool isAllowed(std::string client, int channel);

protected:
    std::pair<std::string, std::list<std::pair<std::string, std::string> > > 
        newGroup(DOMNode * node);

    /**
     * \brief list of all the channels
     */
    std::vector<C_Channel > channels;
    
    /** 
     * \brief number of valid channels available 
     */
    int i_channel_max;   

    /**
     * \brief the list of groups and their contents
     *
     * This object maps a "group name" to a list of subnets.
     * Each subnet is defined by its base address and its mask.
     */
    std::map<std::string, std::list<std::pair<std::string, std::string> > >
        h_hosts;
};

