/*****************************************************************************
 * logger.c: VideoLAN Channel Server logging facility
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: logger.cc,v 1.2 2003/01/18 17:16:11 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdarg.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <errno.h>
#include <time.h>
#include <list> 
#include <vector>  
#include <iostream>                                                             
#include <sstream>
#include <fstream>     
#include <string>

#include "host.h"
#include "vlcs.h"
#include "logger.h"


C_Logger::~C_Logger()
{
}

/**
 * Logger chain 
 */

C_ChainedLogger::C_ChainedLogger() : logger_list()
{
}

C_ChainedLogger::~C_ChainedLogger()
{
}

void C_ChainedLogger::Add(C_Logger * logger)
{
    logger_list.push_back(logger);
}

void C_ChainedLogger::Remove(C_Logger * logger)
{
    logger_list.remove(logger);
}

C_Logger &C_ChainedLogger::operator<<(const char * msg)
{
    for(std::list<C_Logger *>::iterator from = logger_list.begin(); 
        from != logger_list.end() ; ++from)
    {
        (**from) << msg;
    }

    return *this;
}

C_Logger &C_ChainedLogger::operator<<(int value)
{
    for(std::list<C_Logger *>::iterator from = logger_list.begin(); 
        from != logger_list.end() ; ++from)
    {
        (**from) << value;
    }

    return *this;
}

C_Logger &C_ChainedLogger::operator<<(log_command value)
{
    for(std::list<C_Logger *>::iterator from = logger_list.begin(); 
        from != logger_list.end() ; ++from)
    {
        (**from) << value;
    }

    return *this;
}

/**
 * file logger
 */
C_FileLogger::C_FileLogger(const std::string filename, logging_color color) 
    : destroy(true), m_color(color)
{
    log_file = new std::ofstream(filename.c_str(), std::ios::app);
}

C_FileLogger::C_FileLogger(std::ostream * os, logging_color color)
    : destroy(false), log_file(os), m_color(color)
{
}
    
C_FileLogger::~C_FileLogger()
{
    if (destroy)
    {
        delete log_file;
    }
}

C_Logger &C_FileLogger::operator<<(const char * msg)
{
    (*log_file) << msg;
    return *this;
}

C_Logger &C_FileLogger::operator<<(int value)
{
    (*log_file) << value;
    return *this;
}

C_Logger &C_FileLogger::operator<<(log_command value)
{
    switch(value)
    {
    case DEBUG:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[37m";
        }
        (*log_file) << "DEBUG : ";
        break;
    case INFO:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[37;1m";
        }
        (*log_file) << "INFO : ";
        break;
    case WARNING:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[33;1m";
        }
        (*log_file) << "WARNING : ";
        break;
    case ERROR:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[31m";
        }
        *log_file << "ERROR : ";
        break;
    case ALERT:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[31;1m";
        }
        (*log_file) << "ALERT : ";
        break;
    case END:
        if (m_color == LINUX)
        {
            (*log_file) << "\033[0m";
        }
        (*log_file) << std::endl;
    }
    
    return *this;
}

/**
 * Logger to standard error output
 */
C_StdLogger::C_StdLogger(logging_color color) : C_FileLogger(&std::cerr, color)
{
}

C_StdLogger::~C_StdLogger()
{
}

/**
 * Syslog logger 
 */
C_SysLogger::C_SysLogger(const std::string syslog_name, int facility)
    : buffer(), level(LOG_INFO)
{
    openlog( syslog_name.c_str(), 0, facility );
}

C_SysLogger::~C_SysLogger()
{
    closelog();
}

C_Logger &C_SysLogger::operator<<(const char * msg)
{
    buffer += msg;
    return *this;
}

C_Logger &C_SysLogger::operator<<(int value)
{
    std::ostringstream o;
    o << value;
    buffer += o.str();

    return *this;
}

C_Logger &C_SysLogger::operator<<(log_command value)
{
    switch(value)
    {
    case DEBUG:
        level = LOG_DEBUG;
        buffer += "[DEBUG] ";
        break;
    case INFO:
        level = LOG_INFO;
        buffer += "[INFO] ";
        break;
    case WARNING:
        level = LOG_WARNING;
        buffer += "[WARNING] ";
        break;
    case ERROR:
        level = LOG_ERR;
        buffer += "[ERROR] ";
        break;
    case ALERT:
        level = LOG_ALERT;
        buffer += "[ALERT] ";
        break;
    case END:
        syslog(level, buffer.c_str());
        buffer = "";
    }
    
    return *this;
}
