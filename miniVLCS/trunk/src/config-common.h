/*****************************************************************************
 * config.c: mini-VideoLAN Channel Server, configuration system
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: config-common.h,v 1.2 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/**
 * \brief Abstract configuration system
 */
class C_Config
{
public:
    typedef enum { IPV4, IPV6 } inet_type;
    
    /**
     * \brief creates a C_Config with the specified logging system
     */
    C_Config(C_Logger &logger);

    /**
     * \brief class destructor
     */
    virtual ~C_Config();

    /**
     * \brief returns the vlc string of the specified channel
     */
    virtual std::string GetChannel( int i_channel ) = 0;

    /**
     * \brief returns the number of channels
     */
    virtual int GetChannelNumber( void ) = 0; 

    /**
     * \brief Force the object to read its configuration file
     */
    virtual void ReadFile( void ) = 0;

    /**
     * \brief Asks whether the specified client is allowed to
     * watch this channel or not
     */
    virtual bool isAllowed(std::string client, int channel) = 0;

    /**
     * \brief Checks if the configuration file has been update.
     *
     * If the configuration file has been updated, then configuration
     * is automatically reloaded.
     */
    void CheckUpdate( void );

    /**
     * \brief Modifies the configuration file 
     *
     * The new configuration file is not automatically loaded.
     */
    void SetConfigFile(std::string psz_config_file);
    
    /**
     * \brief sets the inet_type
     */
    void setInetType(inet_type t); 
    
    /**
     * \brief returns the inet_type
     */
    inet_type getInetType();
 
    
    /**
     * \brief modifies the binding port 
     *
     * this method does not influence any existing C_Socket object
     */
    void setSocketPort(int port);

    /**
     * \brief returns the binding port
     */
    int getSocketPort();
    
    /**
     * \brief modifies the binding address
     *
     * this method does not influence any existing C_Socket object
     */
    void setBindAddr(std::string addr); 

    /**
     * \brief returns the binding address
     */
    std::string getBindAddr();
    
    /**
     * \brief converts '\\n' to ' ' in the string
     */
    static char * RemoveLF( char * str );

protected:
    /**
     * \brief gets the modification time of the configuration file
     */
    time_t GetModificationTime( void );
    
    inet_type socket_type;
    std::string bindAddr;
    int socket_port;
    
    /**
     * \brief logging facility object 
     */
    C_Logger &logger;       
    
    /** 
     * \brief filename of the config file 
     */
    std::string configFile; 
    
    /**
     * \brief last modification time of the config file 
     */
    time_t mtime;           
};
