/*****************************************************************************
 * socket.h: VideoLAN Channel Server network facilities
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: socket.h,v 1.3 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/**
 * \brief Network access encapsulation using sockets
 *
 * Design pattern : composite
 */

class C_Socket
{
public:
    /** \brief Constructor :
     * \param logger Logging facility of the system
     */
    C_Socket(C_Logger &logger);

    /**
     * \brief Class destructor
     *
     * The destructor closes the socket
     */
    virtual ~C_Socket();

    /**
     * \brief Copy the received message in buffer
     */
    virtual int Receive( std::string &buffer ) = 0;

    /**
     * \brief replies message to the client
     */
    virtual void Reply( std::string message ) = 0;

    /**
     * \brief Returns the client ip as a string
     */
    virtual const std::string Ntop( ) = 0;
    
    /**
     * \brief the handle of the underlaying socket
     */
    int i_handle;
protected:
    template<typename sockaddr_inx> 
        void _Reply( const sockaddr_inx sa_client, std::string message );
    template<typename sockaddr_inx>
        int _Receive( sockaddr_inx &sa_client, std::string &buffer );

    /**
     * \brief the logging facility
     */
    C_Logger &logger;
};

/**
 * \brief C_Socket implementation using IPv4
 */
class C_IPv4Socket : public C_Socket
{
public:
    C_IPv4Socket( C_Logger &logger, int port, 
            std::string bind_addr = std::string() ) throw (int);

    /**
     * \brief Copy the received message in buffer
     */
    virtual int Receive( std::string &buffer );

    /**
     * \brief replies message to the client
     */
    virtual void Reply( std::string message );

    /**
     * \brief Returns the client ip as a string
     */
    virtual const std::string Ntop( );

protected:
    /**
     * \brief the client description
     */
    sockaddr_in m_sa_client;
};

#ifdef IPV6_SUPPORT
/**
 * \brief C_Socket implementation using IPv6
 */
class C_IPv6Socket : public C_Socket
{
public:
    C_IPv6Socket( C_Logger &logger, int port, 
            std::string bind_addr = std::string() ) throw(int);

    /**
     * \brief Copy the received message in buffer
     */
    virtual int Receive( std::string &buffer );

    /**
     * \brief replies message to the client
     */
    virtual void Reply( std::string message );

    /**
     * \brief Returns the client ip as a string
     */
    virtual const std::string Ntop( );
    
protected:
    /**
     * \brief the client description
     */
    sockaddr_in6 m_sa_client;
};
#endif
