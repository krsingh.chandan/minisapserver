/*****************************************************************************
 * socket.c: VideoLAN Channel Server network facilities
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: socket.cc,v 1.3 2003/01/18 19:52:38 marcari Exp $
 *
 * Authors: Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <list>                                                                 
#include <vector>  
#include <iostream>                                                             
#include <fstream>                                                              
#include <string>                                                               
                  
#include "host.h"
#include "vlcs.h"
#include "logger.h"
#include "config-common.h"
#include "socket.h"


C_Socket::C_Socket(C_Logger &logger) : i_handle(0), logger(logger)
{
}

C_Socket::~C_Socket()
{
    close( i_handle );
}

template<typename sockaddr_inx>
void C_Socket::_Reply(const sockaddr_inx sa_client, const std::string message )
{
    time_t tm;
    time(&tm);

    /* TODO: remove this ugly kludge */
    logger << C_Logger::INFO << C_Config::RemoveLF( asctime( localtime( &tm ))) << " : "
            << Ntop( ) << " => " << message << C_Logger::END;

    for( ; ; )
    {
        if( sendto( i_handle, message.c_str(), message.length() + 1, 0,
                    (struct sockaddr *)&sa_client, sizeof(sa_client) ) < 0 )
        {
            if( errno == EINTR )
            {
                continue;
            }
            //logger->Alert( "Cannot sendto (%s)\n", strerror(errno) );
        }
        break;
    }    
}

template<typename sockaddr_inx>
int C_Socket::_Receive( sockaddr_inx &sa_client, std::string &buffer )
{
    static char p_buffer[1024];
    socklen_t i_dummy = sizeof( sa_client );
    
    int i_len = recvfrom( i_handle, p_buffer, sizeof(p_buffer), 0,
                        (struct sockaddr *)&sa_client, &i_dummy);
    p_buffer[i_len] = '\0';
    buffer = p_buffer;

    return i_len;
}


/*
 * C_IPv4Socket class implementation
 */

C_IPv4Socket::C_IPv4Socket(C_Logger &logger,
        int port, std::string bind_addr) throw (int) : C_Socket(logger)
{
    int i_opt;
    struct sockaddr_in sa_server;
    struct in_addr s_bind_addr;

    this->logger = logger;

    
    i_handle = socket( AF_INET, SOCK_DGRAM, 0 );

    if( i_handle < 0 )
    {
        logger << C_Logger::ALERT << "Cannot open socket (" << strerror(errno) 
                << "), exiting" << C_Logger::END;
        throw( -1 );
    }

    if( setsockopt( i_handle, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof(i_opt) )
          < 0 )
    {
        logger << C_Logger::ALERT << "Cannot setsockopt (" << strerror(errno) 
                << "), exiting" << C_Logger::END;
        throw( -1 );
    }

    
    sa_server.sin_family = AF_INET;

    if ( !bind_addr.empty() )
    {
        inet_aton(bind_addr.c_str(), &s_bind_addr);
        sa_server.sin_addr.s_addr = s_bind_addr.s_addr;
    } 
    else 
    {
        sa_server.sin_addr.s_addr = INADDR_ANY;
    }
    sa_server.sin_port = ntohs( port );
    if( bind( i_handle, (struct sockaddr *)&sa_server, sizeof(sa_server) ) < 0 )
    {
        logger << C_Logger::ALERT << "Cannot bind (" << strerror(errno) << "), exiting"
                << C_Logger::END;
        throw( -1 );
    }
}

int C_IPv4Socket::Receive( std::string &buffer )
{
    return _Receive( m_sa_client, buffer );
}

void C_IPv4Socket::Reply( std::string message )
{
    _Reply( m_sa_client, message );
}

const std::string C_IPv4Socket::Ntop( void )
{
    char str[INET_ADDRSTRLEN];
    return std::string(inet_ntop(AF_INET, &(m_sa_client.sin_addr), 
                                 str, sizeof(str)));
}
    
#ifdef IPV6_SUPPORT
/*
 * C_IPv6Socket class implementation
 */

C_IPv6Socket::C_IPv6Socket(C_Logger &logger, 
        int port, std::string bind_addr ) throw (int) : C_Socket(logger)
{
    int i_opt;
    struct sockaddr_in6 sa_server; //XXX
    struct in6_addr s_bind_addr;   //XXX

    this->logger = logger;
    
    
    i_handle = socket( AF_INET6, SOCK_DGRAM, 0 );

    if( i_handle < 0 )
    {
        logger << C_Logger::ALERT << "Cannot open socket (" << strerror(errno) 
                << "), exiting" << C_Logger::END;
        throw( -1 );
    }

    if( setsockopt( i_handle, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof(i_opt) )
          < 0 )
    {
        logger << C_Logger::ALERT << "Cannot setsockopt (" << strerror(errno) 
                << "), exiting" << C_Logger::END;
        throw( -1 );
    }

    
    sa_server.sin6_family = AF_INET6; //XXX
 
    if ( !bind_addr.empty() )
    {
        inet_pton(AF_INET6, bind_addr.c_str(), &s_bind_addr);    //XXX
        memcpy(sa_server.sin6_addr.s6_addr, s_bind_addr.s6_addr, //XXX
                sizeof(s_bind_addr.s6_addr));
    } 
    else 
    {
        sa_server.sin6_addr = in6addr_any;  //XXX
    }
    sa_server.sin6_port = ntohs( port ); //XXX
    if( bind( i_handle, (struct sockaddr *)&sa_server, sizeof(sa_server) ) < 0 )
    {
        logger << C_Logger::ALERT << "Cannot bind (" << strerror(errno) << "), exiting"
                << C_Logger::END;
        throw( -1 );
    }
}

int C_IPv6Socket::Receive( std::string &buffer )
{
    return _Receive( m_sa_client, buffer );
}


void C_IPv6Socket::Reply( std::string message )
{
    _Reply( m_sa_client, message );
}
const std::string C_IPv6Socket::Ntop( void )
{
    char str[INET6_ADDRSTRLEN];
    return std::string(inet_ntop(AF_INET6, &(m_sa_client.sin6_addr), 
                                 str, sizeof(str)));
}

#endif  // IPV6
