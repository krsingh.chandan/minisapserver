/*****************************************************************************
 * vlcs.h: VideoLAN Channel Server configuration
 *****************************************************************************
 * Copyright (C) 2001 VideoLAN
 * $Id: vlcs.h,v 1.2 2002/12/20 15:53:59 marcari Exp $
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Marc Ariberti <marcari@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

/* precise the bind address, useful when using IP aliasing 
 * do not define this symbol if you want to bind to INADDR_ANY or in6addr_any*/
/* #define BIND_ADDRESS "192.168.12.42" */

/* Uncomment this line to use IPv6 with vlcs by default */
/* #define IPV6 */

/* comment this line to not compile IPV6 support into vlcs */
#define IPV6_SUPPORT

#define VLCS_VERSION 13
#define VLCS_PORT 6010
#define CHANNEL_CONF_XML "vlcs.conf"
#define CHANNEL_CONF_TXT "vlcs-old.conf"

#define NO_COLORS       0
#define LINUX_COLORS    1

/* undefine this symbol to remove STDOUT logging */
#define LOG_STDOUT      LINUX_COLORS

/* undefine this symbol if you do not want to log to a file */
#define LOG_FILE "vlcs.log"

/* uncomment this symbol if you want to log to syslog */
/* #define SYSLOG_NAME "VLCS" */
#define SYSLOG_FACILITY LOG_DAEMON

int main( int argc, char ** argv );
void Close( int i_ret );

#undef DEBUG
