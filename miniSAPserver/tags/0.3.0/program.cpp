/*****************************************************************************
 * program.cpp : SAP Program class
 ****************************************************************************
 * Copyright (C) 1998-2002 VideoLAN
 * $Id$
 *
 * Authors: Damien Lucas <nitrox@videolan.org>
 *          Philippe Van Hecke <philippe.vanhecke@belnet.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/

#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
using namespace std;

#include "program.h"

Program::Program() : b_rtp(false), b_has_pl_group(false)
{
    /* Set default Values */
    address="";
    port="";
    permanent = true;
    program_ipversion = 4;
    program_ttl = "15";
    machine = "localhost";
    user = "VideoLAN";
    site = "http://www.videolan.org";
}
Program::~Program() {return;}

string Program::GetName(void) {return name;}
string Program::GetUser(void) {return user;}
string Program::GetMachine(void) {return machine;}
string Program::GetSite(void) {return site;}
string Program::GetAddress(void){return address;}
string Program::GetPort(void){return port;}
string Program::GetTTL(void){return program_ttl;}
string Program::GetPlGroup(void){return pl_group;}

int Program::GetIPVersion(void){return program_ipversion;};

bool   Program::IsPermanent(void){return permanent;}
bool   Program::IsRTP(void){return b_rtp;}
bool   Program::HasPlGroup(void){return b_has_pl_group;}

void Program::SetName(char* n){name=n;}
void Program::SetUser(char* u){user=u;}
void Program::SetMachine(char* m){machine=m;}
void Program::SetSite(char* s){site=s;}
void Program::SetAddress(char* a){address=a;}
void Program::SetPlGroup(char *h){pl_group=h;}
void Program::SetRTP(bool b){b_rtp = b;}
void Program::SetHasPlGroup(bool b){b_has_pl_group = b ;}
void Program::SetPort(char* p)
{
    int i_port=atoi(p);
    char psz_port[12];
    sprintf(psz_port,"%i",i_port);
    port +=psz_port;

}

void Program::SetTTL(char *p){program_ttl=p;}
void Program::SetIPVersion(char *p){program_ipversion=atoi(p);}

Program::Program(string n, string u, string m, string s, string a,string p)
    : program_ipversion(4), b_has_pl_group (false)
{
    name=n;
    user=u;
    machine=m;
    site=s;
    address=a;
    port=p;
    permanent=true;
}
