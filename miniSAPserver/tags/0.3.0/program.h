/*****************************************************************************
 * program.h : SAP programs classes definition
 ****************************************************************************
 * Copyright (C) 1998-2002 VideoLAN
 * $Id$ 
 *
 * Authors: Damien Lucas <nitrox@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 *****************************************************************************/


class Program
{
  public:
    Program(string, string, string, string, string,string);
    Program();
    ~Program();

    /*  Functions to get the values */
    string GetName();
    string GetUser();
    string GetMachine();
    string GetSite();
    string GetAddress();
    string GetPort();
    string GetTTL();
    string GetPlGroup();
    int GetIPVersion();

    /* Functions to set the values */
    void SetName(char*);
    void SetUser(char*);
    void SetMachine(char*);
    void SetSite(char*);
    void SetAddress(char*);
    void SetPort(char *);
    void SetTTL(char *);
    void SetPlGroup(char *);
    void SetHasPlGroup(bool);
    void SetIPVersion(char *);
    void SetRTP(bool);

    bool IsPermanent();
    bool IsRTP();
    bool HasPlGroup();

  private:
    string name;
    string user;
    string machine;
    string site;
    string address;
    string port;
    string program_ttl;
    string pl_group;
    int program_ipversion;
    bool permanent;
    bool b_rtp;
    bool b_has_pl_group;
    uint32_t start_time;
    uint32_t stop_time;
    /* TODO support for periodical programs */
};
